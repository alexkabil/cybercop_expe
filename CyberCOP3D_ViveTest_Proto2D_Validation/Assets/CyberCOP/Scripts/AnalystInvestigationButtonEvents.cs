﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalystInvestigationButtonEvents : MonoBehaviour {
    private int _idAsset=-1;
    private int _state = -1;
    private int _alerttype = -1;
    public GameObject AnalystButton;
    private AssetInfoList Scenario;
    public TestUIEvents AnalysInvestigationAction;
    bool samebutton = false;

	// Use this for initialization
	void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
        //AnalystButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(ButtonAction); faudra corriger les appels partout..
    }

    public void ButtonAction()
    {
        if (!samebutton)
        {
            AnalysInvestigationAction.Raise(_idAsset, _state, _alerttype, Scenario.Currentuser.id);
            samebutton = true;
        }
    }

    public void InitButtonState(int i, int j, int k, int l)
    {
        if (_idAsset == -1)//condition initiale
        {
            _idAsset = i;
            _state = j;
            _alerttype = k;
            samebutton = false;
            return;
        }
        if(_idAsset==i && _state==j && _alerttype == k)
        {
            samebutton = true;
            return;
        }
        _idAsset = i;
        _state = j;
        _alerttype = k;
        samebutton = false;

    }



    public void Highlight(bool b)
    {
        if (Scenario.hasAssistance)
        {
            if (b)
            {
                AnalystButton.transform.GetChild(1).GetComponent<ParticleSystem>().Play();
            }
            else
            {
                AnalystButton.transform.GetChild(1).GetComponent<ParticleSystem>().Stop();
            }
        }
        
    }

    public void BlockInvestigationButton(bool b)
    {
        AnalystButton.GetComponent<UnityEngine.UI.Button>().interactable = !b;
        Highlight(!b);
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
