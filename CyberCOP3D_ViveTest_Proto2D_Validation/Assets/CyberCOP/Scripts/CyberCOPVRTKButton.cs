﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class CyberCOPVRTKButton : MonoBehaviour {
    public VRTK_ControllerEvents Gamepad;
    private bool isActive = false;
    //a virer si le tableau d'actions fonctionne
    public SystemActions ButtonActionActivation;
    public SystemActions ButtonActionDeactivation;

    public SystemActions[] ButtonActions;
    private int _buttonPermutation = 0;
    public delegate void OnButtonPressedUpdate(SystemActions action);
    public static event OnButtonPressedUpdate ButtonPressedDelegate;
    private void OnEnable()
    {
        if (Gamepad != null)
        {
            Gamepad.ButtonOnePressed += DoButtonOnePressed;
            Gamepad.ButtonOneReleased += DoButtonOneReleased;
            //controllerEvents.ButtonOneTouchStart += DoButtonOneTouchStart;
            //controllerEvents.ButtonOneTouchEnd += DoButtonOneTouchEnd;

            Gamepad.ButtonTwoPressed += DoButtonTwoPressed;
            Gamepad.ButtonTwoReleased += DoButtonTwoReleased;
            //controllerEvents.ButtonTwoTouchStart += DoButtonTwoTouchStart;
            //controllerEvents.ButtonTwoTouchEnd += DoButtonTwoTouchEnd;
        }
    }

    private void OnDisable()
    {
        if (Gamepad != null)
        {
            Gamepad.ButtonOnePressed -= DoButtonOnePressed;
            Gamepad.ButtonOneReleased -= DoButtonOneReleased;
            //controllerEvents.ButtonOneTouchStart -= DoButtonOneTouchStart;
            //controllerEvents.ButtonOneTouchEnd -= DoButtonOneTouchEnd;

            Gamepad.ButtonTwoPressed -= DoButtonTwoPressed;
            Gamepad.ButtonTwoReleased -= DoButtonTwoReleased;
            //controllerEvents.ButtonTwoTouchStart -= DoButtonTwoTouchStart;
            //controllerEvents.ButtonTwoTouchEnd -= DoButtonTwoTouchEnd;
        }
    }
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}


    private void DoButtonOnePressed(object sender, ControllerInteractionEventArgs e)
    {
        /*
        isUIActive = !isUIActive;
        if (isUIActive)
        {
            MenuEventMove.Raise(true);
        }
        else
        {
            MenuEventMove.Raise(false);
        }
        */
    }

    private void DoButtonOneReleased(object sender, ControllerInteractionEventArgs e)
    {
        
    }


    private void DoButtonTwoPressed(object sender, ControllerInteractionEventArgs e)
    {
        /*
        isActive = !isActive;
        SystemActions action = isActive ? ButtonActionActivation : ButtonActionDeactivation;
        ButtonPressedDelegate(action);
        */
        //systeme de permutation de vues ou d'actions en fonction de la taille de la liste d'actions
        ButtonPressedDelegate(ButtonActions[_buttonPermutation]);
        _buttonPermutation= (_buttonPermutation+1)%ButtonActions.Length;
        //a tester..
    }

    private void DoButtonTwoReleased(object sender, ControllerInteractionEventArgs e)
    {
        
    }

    
}
