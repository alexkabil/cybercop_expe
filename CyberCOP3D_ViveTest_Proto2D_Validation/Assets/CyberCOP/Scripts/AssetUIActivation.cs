﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetUIActivation : MonoBehaviour
{
    public GameObject UIAsset;
    private int UIControl = 0;
    private bool activation = false;
    private Transform userHead;
    public int id;
    private Color _initColor;
    // Use this for initialization
	void Start ()
    {
        _initColor = GetComponent<Renderer>().material.color;
        UIAsset.SetActive(false);

	}
	public void UiActivation(bool b)
    {
        UIAsset.SetActive(b);
    }

    public void UIActivation()
    {
        if (UIControl != 0)
        {
            return;
        }
        if (Camera.main.transform != null)
        {
            userHead = Camera.main.transform;
        }
        
        activation = true;
        UIAsset.SetActive(true);
    }
    public void UIDeactivation()
    {
        if (UIControl != 0)
        {
            return;
        }
        activation = false; 
        
        UIAsset.SetActive(false);
    }
    public void UIStay()
    {
        if (UIControl == 0)
        {
            
            UIControl =1;
            return;
        }
        if (UIControl == 1)
        {
            UIControl = 0;
            return;
        }
        

    }

    public void SetColor(bool b)
    {
        GetComponent<Renderer>().material.color = b ? Color.green : _initColor;
    }
    public void SetButtonAction()
    {
        activation = !activation;
        if (activation)
        {
            if (Camera.main.transform != null)
            {
                userHead = Camera.main.transform;
            }
            SetColor(true);
            transform.GetComponentInParent<GraphVisPosition>().PositionAssets(id);
        }
        UIAsset.SetActive(activation);
    }

    // Update is called once per frame
    void Update ()
    {
        if (activation && userHead!=null)
        {
            UIAsset.transform.LookAt(2 * UIAsset.transform.position - userHead.position);
        }
	}
}
