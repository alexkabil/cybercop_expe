﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetCoordAnalystSelection : MonoBehaviour
{
    private AssetInfoList Scenario;
    public Transform Assets;

    public InformationButtonAssetEvents InformationButtonAssetEvents;

    public RequiredActions ScenarioAction = RequiredActions.ASSETSELECTION;
    // Use this for initialization
	void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }

    public void SetNextStepScenario()
    {
        if (Scenario.GetCurrentAttack().GetCurrentAction() == ScenarioAction)
        {
            Scenario.GetCurrentAttack().NextScenarioAction();
        }
    }

    public void AnalystSelectInvestigatedAsset(int i, int j, int k, int l)
    {
        if (Scenario.hasAssistance)
        {
            Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightObject(false, k);
            if (k == 0)
            {
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightPhysInfoButton(true);
            }
            if (k == 1)
            {
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightCyberInfoButton(true);
            }
        }
    }

    public void SetEventAlertState(int i, int j, int k, int l)
    {
        InformationButtonAssetEvents.SetInformationAlert(i, j, k, l);
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
