﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetsSelection : MonoBehaviour
{
    public TestUIEvents AssetSelectionEvent;
    private int _assetId=0;
    private int _alertType=0;
    private int _state = 0;
    private AssetInfoList Scenario;
	// Use this for initialization
	void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }
    
    public void SetInvestigationNeeds(int i, int j, int k, int l)
    {
        _assetId = i;
        _state = j;
        _alertType = k;
    }

	public void AssetSelected(int i, int j, int k, int l)
    {
        if (i != _assetId)
        {
            Debug.Log("Pas le bon asset,");
            return;
        }
        else
        {
            AssetSelectionEvent.Raise(i, j, k, l);
        }

        /*
        if (Scenario.Assets[i - 1].State.isAlert == false)
        {
            Debug.Log("Il est pas en alerte");
            return;
        }
        if (i == _assetId)
        {

            if (Scenario.Assets[i - 1].State.isSelectedCoord)
            {
                AssetSelectionEvent.Raise(i, j, k, l);
            }
            return;
        }
        */
    }
	// Update is called once per frame
	void Update ()
    {
		
	}
}
