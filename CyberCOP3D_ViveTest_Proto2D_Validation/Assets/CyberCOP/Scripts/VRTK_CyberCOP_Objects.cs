﻿
using UnityEngine;
using VRTK;
using VRTK.Highlighters;

public class VRTK_CyberCOP_Objects : MonoBehaviour
{
    public VRTK_DestinationMarker pointer;
    public Color hoverColor = Color.cyan;
    public Color selectColor = Color.yellow;
    public Color otherColor = Color.grey; //laisser ça??
    public int InteractionLayer = 14;
    private AssetInfoList Scenario;

    //public delegate void ON3DAssetClickDelegate(CyberCopEventsInformation e);
    //public static event ON3DAssetClickDelegate Asset3DDelegate;
    public void Start()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }

    public void SetInteractionLayer(int i)
    {
        InteractionLayer = i;
    }

    protected virtual void OnEnable()
    {
        pointer = (pointer == null ? GetComponent<VRTK_DestinationMarker>() : pointer);

        if (pointer != null)
        {
            pointer.DestinationMarkerEnter += DestinationMarkerEnter;
            pointer.DestinationMarkerHover += DestinationMarkerHover;
            pointer.DestinationMarkerExit += DestinationMarkerExit;
            pointer.DestinationMarkerSet += DestinationMarkerSet;
        }
        else
        {
            VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTKExample_PointerObjectHighlighterActivator", "VRTK_DestinationMarker", "the Controller Alias"));
        }
    }

    protected virtual void OnDisable()
    {
        if (pointer != null)
        {
            pointer.DestinationMarkerEnter -= DestinationMarkerEnter;
            pointer.DestinationMarkerHover -= DestinationMarkerHover;
            pointer.DestinationMarkerExit -= DestinationMarkerExit;
            pointer.DestinationMarkerSet -= DestinationMarkerSet;
        }
    }

    protected virtual void DestinationMarkerEnter(object sender, DestinationMarkerEventArgs e)
    {
        if (e.target.gameObject.layer == InteractionLayer || e.target.gameObject.layer==13)//revoir c'est quoi 13?Dataviz..
        {
            //rajouter event pour hover?
            if (e.target.GetComponent<CyberCOP3DInteractiveObject>() != null)
            {
                CyberCOP3DInteractiveObject asset = e.target.GetComponent<CyberCOP3DInteractiveObject>();
                if (asset.AssetControl == 0)
                {
                    ToggleHighlight(e.target, hoverColor);
                }
                return;
            }
            /*
            else if (Scenario.isTuto) //on prend en compte différemment les objets tutos?
            {
                ToggleHighlight(e.target, Color.green);
            }
            */
            //affichage ui???
            /*
            if (e.target.GetComponent<CyberCOP3DAssets>() != null)
            {
                e.target.GetComponent<CyberCOP3DAssets>().SetUserInteraction(Scenario.Currentuser.id, Scenario.Currentuser.CurrentTicketID, true);
                //juste quand on appuie on fait apparaitre ui??
            }
            */
        }
    }

    private void DestinationMarkerHover(object sender, DestinationMarkerEventArgs e)
    {
    }

    protected virtual void DestinationMarkerExit(object sender, DestinationMarkerEventArgs e)//mettre user immersive..
    {
        if (e.target.gameObject.layer == InteractionLayer || e.target.gameObject.layer == 13)
        {
            if (e.target.GetComponent<CyberCOP3DInteractiveObject>() != null)
            {
                CyberCOP3DInteractiveObject asset = e.target.GetComponent<CyberCOP3DInteractiveObject>();
                if (asset.AssetControl == 0)
                {
                    ToggleHighlight(e.target, Color.clear);
                }
            }
            /*
            if (e.target.GetComponent<CyberCOP3DAssets>() != null)
            {
                e.target.GetComponent<CyberCOP3DAssets>().SetUserInteraction(Scenario.Currentuser.id, Scenario.Currentuser.CurrentTicketID, false);
                //juste quand on appuie on fait apparaitre ui??
            }
            */
        }
    }

    protected virtual void DestinationMarkerSet(object sender, DestinationMarkerEventArgs e)
    {
        if (e.target.gameObject.layer == InteractionLayer || e.target.gameObject.layer == 13)//pourquoi 13 deja ??
        {

            if (e.target.GetComponent<CyberCOP3DInteractiveObject>() != null)
            {
                CyberCOP3DInteractiveObject asset = e.target.GetComponent<CyberCOP3DInteractiveObject>();
                //mettre un script similaire sur les objets de la vue alerte
                asset.SetButtonAction();

                return;
            }
            if (e.target.GetComponent<AssetUIActivation>() != null)
            {
                e.target.GetComponent<AssetUIActivation>().SetButtonAction();
                return;
            }
            else
            {
                ToggleHighlight(e.target, Color.red);
                if (e.target.gameObject.name == "3DTuto")
                {
                    GameObject.Find("TutorialManager").GetComponent<CyberCopTutorialManager>().AssetClikIT();
                    return;
                }
                else if (e.target.gameObject.name == "CybTuto")
                {
                    GameObject.Find("TutorialManager").GetComponent<CyberCopTutorialManager>().AssetClikCyber();
                    return;
                }
                else if (e.target.gameObject.name == "AlertTuto")
                {
                    GameObject.Find("TutorialManager").GetComponent<CyberCopTutorialManager>().AssetClikAlert();
                    return;
                }
            }
        }
    }

    protected virtual void ToggleHighlight(Transform target, Color color)
    {
        VRTK_BaseHighlighter highligher = (target != null ? target.GetComponentInChildren<VRTK_BaseHighlighter>() : null);
        if (highligher != null)
        {
            highligher.Initialise();
            if (color != Color.clear)
            {
                highligher.Highlight(color);
            }
            else
            {
                highligher.Unhighlight();
            }
        }
    }

    private void ActivateObject(Transform t, bool b)
    {

    /*if (t.GetComponent<AssetUIActivation>() != null)
    {
        t.SendMessage(s);
    }*/
        if (t.GetComponent<CyberCOP3DInteractiveObject>() != null)
        {
        }
    }    

    protected virtual void DebugLogger(uint index, string action, Transform target, RaycastHit raycastHit, float distance, Vector3 tipPosition)
    {
        string targetName = (target ? target.name : "<NO VALID TARGET>");
        string colliderName = (raycastHit.collider ? raycastHit.collider.name : "<NO VALID COLLIDER>");
        VRTK_Logger.Info("Controller on index '" + index + "' is " + action + " at a distance of " + distance + " on object named [" + targetName + "] on the collider named [" + colliderName + "] - the pointer tip position is/was: " + tipPosition);
    }
}