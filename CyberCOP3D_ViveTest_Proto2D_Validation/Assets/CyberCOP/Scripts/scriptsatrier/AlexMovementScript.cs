﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlexMovementScript : MonoBehaviour {
    public float speed = 5.0f;
    public float rotationspeed = 100.0f;
    public bool isMovement = false;
    public Camera camAction;
    private int _layerinteraction = 14;
	// Use this for initialization
	void Start ()
    {
		camAction = transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Camera>();
	}
    private void OnEnable() 
    {
        ScenarioActionsListener.CyberPhysicalViewDelegate += ChangeLayerInteraction;    
    }
    private void OnDisable() 
    {
        ScenarioActionsListener.CyberPhysicalViewDelegate -= ChangeLayerInteraction;
    }

    public void ChangeLayerInteraction(int i)
    {
        switch (i)
        {
            case 0:
                _layerinteraction = 14;
                break;
            case 1:
                _layerinteraction = 10;
                break;
            case 2:
                _layerinteraction = 16;
                break;
            default:
                _layerinteraction = 14;
                break;
        }
    }
    private GameObject RaycastObject(Vector2 screenPos)
    {
        RaycastHit info;
        if (Physics.Raycast(camAction.ScreenPointToRay(screenPos), out info, 20))
        {
            return info.collider.gameObject;
        }

        return null;
    }

    public void ClickOnObjects()
    {
        if(Input.GetMouseButtonUp(0))
        {
            GameObject selectedObject = RaycastObject(Input.mousePosition);
            if (selectedObject!=null && (selectedObject.layer == _layerinteraction || selectedObject.layer == 13))
            {
                if(selectedObject.GetComponent<CyberCOP3DInteractiveObject>()!=null)
                {
                    selectedObject.GetComponent<CyberCOP3DInteractiveObject>().SetButtonAction();
                }
                else
                {
                    if (selectedObject.name == "CybTuto")
                    {
                        GameObject.Find("TutorialManager").GetComponent<CyberCopTutorialManager>().AssetClikCyber();
                        return;
                    }
                    if (selectedObject.name == "3DTuto")
                    {
                        GameObject.Find("TutorialManager").GetComponent<CyberCopTutorialManager>().AssetClikIT();
                        return;
                    }
                    if (selectedObject.GetComponent<AssetUIActivation>() != null)
                    {
                        selectedObject.GetComponent<AssetUIActivation>().SetButtonAction();
                    }
                }
            }
        }

    }

	public void MovementManagement()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float strafe = Input.GetAxis("Horizontal") * speed;
        float rotation = Input.GetAxis("Mouse X") * rotationspeed;
        float yaw = -Input.GetAxis("Mouse Y") * rotationspeed;
        translation *= Time.deltaTime;
        strafe *= Time.deltaTime;
        rotation *= Time.deltaTime;
        yaw *= Time.deltaTime;
        transform.Translate(strafe, 0, translation);
        if(Input.GetMouseButton(1))
        {
            transform.Rotate(0, rotation, 0);
            transform.GetChild(0).GetChild(1).Rotate(yaw,0,0);
        }
    }
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            isMovement = !isMovement;
        }
        ClickOnObjects();
        if (isMovement)
        {
            MovementManagement();
            //que en mvt?
        }
	}
}
