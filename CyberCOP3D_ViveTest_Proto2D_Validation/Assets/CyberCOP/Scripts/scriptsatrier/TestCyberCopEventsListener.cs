﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCyberCopEventsListener : MonoBehaviour {
    public CyberCopEvent even;
	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void GetEvent(CyberCopEventsInformation i, bool b)
    {
        Debug.Log(i.Asset + " " + i.View + " " + i.Action + " " + i.User + " " + i.TicketID + " ");
    }

	// Update is called once per frame
	void Update ()
    {

    }
}
