﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberCopCoordPanelCamera : MonoBehaviour {
    public Transform[] positionsCam;//les mettre en avance?
                                    // Use this for initialization
    public Transform ParentTransform;

    public Animator CoordAnimator;

    public IEnumerator UICameraPlace()
    {
        bool init = true;
        float timer = 0;
        while (init)
        {
            timer += Time.deltaTime;
            if(timer>5.0f || GameObject.FindGameObjectWithTag("MainCamera") != null)
            {
                init = false;
                yield return null;
            }
            yield return null;
        }
        if (GameObject.FindGameObjectWithTag("MainCamera") != null)
        {
            ParentTransform.parent = GameObject.FindGameObjectWithTag("MainCamera").transform;
            ParentTransform.localPosition = Vector3.zero;
            ParentTransform.localRotation = Quaternion.identity;
        }
        yield return null;
    }

    public void SetAnimationUI(bool b)
    {
        if (CoordAnimator != null)
        {
            CoordAnimator.SetBool("IsWideScreen",b);
        }
    }

    public void SetCamPos(int i)
    {
        if (i > positionsCam.Length)
        {
            return;
        }
        if (GameObject.FindGameObjectWithTag("MainCamera") != null)
        {
            Camera.main.transform.position = positionsCam[i].position;
            Camera.main.transform.rotation = positionsCam[i].rotation;
        }
    }
}
