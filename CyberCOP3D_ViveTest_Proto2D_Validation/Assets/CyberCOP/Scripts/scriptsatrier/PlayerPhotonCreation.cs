﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;

public class PlayerPhotonCreation : PunBehaviour {
    private Transform _camera;
    private List<float[]> _positions;
    private int _ownId = -1;
    public bool isLogPos = true;
	// Use this for initialization
	IEnumerator Start ()
    {
        bool test = true;
        while (test)
        {
            if (GameObject.FindGameObjectWithTag("MainCamera")!=null)
            {
                test = false;
                yield return null;
            }
            yield return null;
        }
        _camera = Camera.main.transform;
        if (GetComponent<PhotonView>().isMine)
        {
            GetComponent<PhotonView>().owner.TagObject = gameObject;
            if (isLogPos)
            {
                StartCoroutine(LogPosition());
            }
        }

        yield return null;
	}
    private void Awake()
    {
        if (GetComponent<PhotonView>().isMine)
        {
            GetComponent<Renderer>().enabled = false;
            gameObject.name = "Me";
            transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            //GetComponent<Renderer>().material.color = Color.blue;
            transform.GetChild(1).GetComponent<Renderer>().material.color = Color.blue;
            _ownId = GetComponent<PhotonView>().ownerId;
        }
        else
        {
            GetComponent<Renderer>().material.color = Color.white;
            transform.GetChild(1).GetComponent<Renderer>().material.color = Color.white;
        }
    }
    private void OnDisable()
    {
        Debug.Log("parti "+GetComponent<PhotonView>().ownerId);
        if(_ownId== GetComponent<PhotonView>().ownerId)
        {
            if (isLogPos)
            {
                CyberCopStaticLog.WritePositions(_positions);
            }

        }

    }

    public IEnumerator LogPosition()
    {
        bool init = true;
        //CyberCopStaticLog.InitWriterPos(); //on met ça dans le manager now
        _positions = new List<float[]>();
        while (init)
        {
            float[] positions = new float[] { Time.time, transform.position.x, transform.position.y, transform.position.z,transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z };
            _positions.Add(positions);
            yield return new WaitForSeconds(1.0f);
        }
        yield return null;
    }

    // Update is called once per frame
    void Update ()
    {
        if (GetComponent<PhotonView>().isMine && _camera != null)
        {
            float CameraYrot = _camera.rotation.eulerAngles.y;
            float cameraXrot = _camera.rotation.eulerAngles.x;
            transform.position = _camera.position + new Vector3(0, -0.3f, -0.25f);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, CameraYrot, transform.eulerAngles.z);
            transform.GetChild(0).eulerAngles = new Vector3(cameraXrot, transform.GetChild(0).eulerAngles.y, transform.GetChild(0).eulerAngles.z);
        }
    }
}
