﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CyberCopTutorialManager : MonoBehaviour {
    public bool isTuto = false;
    public GameObject Panel1;
    public GameObject Panel2;
    public GameObject Panel3;
    public ParticleSystem ParticleMain;
    public ParticleSystem ParticleBurst;
    public ParticleSystem ParticleDoor;
    public ParticleSystem ParticleDoor1;
    public GameObject Panel4;
    public GameObject Panel5;
    public GameObject Panel6;
    public GameObject Panel7;
    public GameObject Panel8;
    public GameObject CyberPanel;
    public GameObject ITPanel;
    public GameObject AlertPanel;

    public GameObject Door;
    public MenuUiPosition HandUI;
    public GameObject Asset;
    public Transform AlertButtons;
    public Text AlertText;
    public Button[] ITButtons;
    public Button[] CyberButtons;
    public Transform AlertCaracterization;
    public ParticleSystem AlertParticles;
    public bool isGraph = false;

    private bool _it = true;
    private bool _cyber = true;
    private bool _alert = true;
    private int _vueCount = 0;
    private int _alertCount = 0;
    private bool _isInvestIT = false;
    private bool _isInvestCyber = false;
    private bool _alertIT = false;
    private bool _alertCyber = false;
    private bool _ITinfo = false;
    private bool _CYBERinfo = false;
    // Use this for initialization
    void Start ()
    {
        if (isTuto)
        {
            InitTutorial();
        }
    }
	
    public void InitTutorial()
    {
        Panel2.SetActive(false);
        Panel3.SetActive(false);
        Panel4.SetActive(false);
        Panel5.SetActive(false);
        Panel6.SetActive(false);
        Panel7.SetActive(false);
        Panel8.SetActive(false);
        CyberPanel.SetActive(false);
        ITPanel.SetActive(false);
        AlertPanel.SetActive(false);
        //HandUI.SetUIInitTuto();
        Panel1.GetComponent<AudioSource>().Play();
        Panel1.transform.Find("Text").GetComponent<Text>().text = "Bonjour et Bienvenue\n\n" +
            "Vous êtes dans l'espace tutoriel, où pas à pas vous allez apprendre à vous déplacer et à interagir.\n\n" +
            "Appuyez sur 'Suivant' Pour Continuer.\n\n" +
            "Il suffit de poser les pouces sur les 'joypads' pour faire apparaître des rayons! La manette de droite permet de sélectionner les objets tandis que la manette de gauche permet de se déplacer";
        Panel1.transform.Find("Text (1)").GetComponent<Text>().text = "Utilisez le 'joypad' de la manette de droite pour afficher le pointeur et la gachette pour valider";
    }
    //premier bouton de validation
    public void FirstButton()
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "BEGIN BUTTON");
        Asset.SetActive(false);
        Panel1.SetActive(false);
        Panel2.SetActive(true);
        Panel2.GetComponent<AudioSource>().Play();
        
        Panel2.transform.Find("Text").GetComponent<Text>().text = "Les déplacements se font par téléportation ou via le joypad!\n\n" +
            "Pour faire pivoter le point de vue, appuyez sur la seconde gachette et pivotez le 'joypad' de la manette gauche.\n" +
            "Appuyez sur le 'joypad' pour vous déplacer\n" +
            "Appuyez sur la gachette pour valider la téléportation.\n\n" +
            "Téléportez vous vers l'écran suivant!";
        Panel2.transform.Find("Text (1)").GetComponent<Text>().text = "Utilisez le 'joypad' de la manette de gauche pour afficher le pointeur et la gachette pour se téleporter.\n" +
            "Bougez la manette pour choisir le lieu de téléportation.\n\n" +
            "Appuyez sur le 'Joypad' et sur la gachette sous le majeur de la manette de gauche pour avancer ou pivoter.";
        Panel3.SetActive(true);
        Panel3.GetComponent<AudioSource>().Play();
        /*
        Panel3.transformFind("Text").GetComponent<Text>().text = "Bravo!\n" +
            "Appuyez sur suivant pour passer à la suite";
        Panel3.transform.Find("Text (1)").GetComponent<Text>().text = "Cliquez sur suivant avec la manette de droite";
        */
        ParticleMain.Play();
        //ParticleBurst.Play();

    }
    public void SecondButton()//fait pour les tutos dans scene...
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "NAVIGATION OK");
        Panel2.SetActive(false);
        Panel3.SetActive(false);
        Panel4.SetActive(true);
        Panel4.GetComponent<AudioSource>().Play();
        Panel4.transform.Find("Text").GetComponent<Text>().text = "Vous avez à votre disposition trois vues :\n La vue alerte contient des informations sur les incidents. " +
            "\n La vue IT et la vue cyber contiennent des informations sur différents aspects des assets.\n Activez le bouton 'Vue activée' de chaque vue pour passer à la suite.";
        Panel4.transform.Find("Text (1)").GetComponent<Text>().text = "Appuyez sur le bouton de la manette de gauche pour basculer de vues";
        ParticleMain.Stop();
        ITPanel.SetActive(true);
        AlertPanel.SetActive(true);
        CyberPanel.SetActive(true);
        //ParticleBurst.Stop();
        //HandUI.SetUITuto();
    }
    /*
    public void SecondButton()
    {
        Panel2.SetActive(false);
        Panel3.SetActive(false);
        Panel4.SetActive(true);
        Panel4.GetComponent<AudioSource>().Play();
        Panel4.transform.Find("Text").GetComponent<Text>().text = "Vous avez à votre disposition une interface contextuelle liée à votre manette de droite.\n\n" +
            "Vous pouvez sélectionner les éléments de votre interface utilisateur en dirigeant le curseur via votre tête et en appuyant sur le 'joypad' de droite pour valider.";
        Panel4.transform.Find("Text (1)").GetComponent<Text>().text = "Appuyez sur le bouton de la manette de droite pour faire apparaitre l'interface";
        ParticleMain.Stop();
        //ParticleBurst.Stop();
        HandUI.SetUITuto();
    }
    */

    public void ViewsCounts()
    {
        _vueCount++;
        if (_vueCount == 3)
        {
            ThirdButton();//ou alors afficher un truc qui dit de passer à la suite? truc bravo passage à la suite?
        }
    }
    public void AssetsCounts()
    {
        _alertCount++;
        if (_alertCount == 2)
        {
            FifthButton();//ou alors afficher un truc qui dit de passer à la suite? truc bravo passage à la suite?
        }
    }


    public void ThirdButton()
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "CHANGE VIEWS OK");
        //HandUI.CloseUITuto();
        Panel4.SetActive(false);
        Panel5.SetActive(true);
        Panel5.GetComponent<AudioSource>().Play();
        Panel5.transform.Find("Text").GetComponent<Text>().text = "Bravo, vous avez appris à changer de vues.\n Appuyez sur le bouton pour passer à la suite.";
        Panel5.transform.Find("Text (1)").GetComponent<Text>().text = "Utilisez le 'joypad' de la manette de droite pour afficher le pointeur et la gachette pour valider";
    }

    public void FourthButton()
    {
        Panel5.SetActive(false);
        Panel6.SetActive(true);
        Panel6.GetComponent<AudioSource>().Play();
        Panel6.transform.Find("Text").GetComponent<Text>().text = "Pour connaître l'état du système, vous devez basculer en vue ALERTE.\n Les particules entourant un asset indiquent qu'il y a un ou des problèmes.";
        Panel6.transform.Find("Text (1)").GetComponent<Text>().text = "Passez le pointeur de la manette droite sur l'asset et appuyez sur la gachette pour sélectionner l'asset.";

        Asset.SetActive(true);
        Asset.transform.GetChild(0).gameObject.SetActive(true);
        Asset.transform.GetChild(1).gameObject.SetActive(true);
        Asset.transform.GetChild(2).gameObject.SetActive(true);
        Asset.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
        Asset.transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
        Asset.transform.GetChild(2).GetChild(0).gameObject.SetActive(false);
        AlertParticles.Play();
        //Asset.transform.GetChild(2).GetComponent<ParticleSystem>().Play();

        //Asset.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
    }

    public void AlertButtonsColors()//voir si on integre ça dans la fonction
    {
        AlertButtons.GetChild(1).GetComponent<Image>().color = _isInvestIT ? Color.blue : Color.white;
        AlertButtons.GetChild(0).GetComponent<Image>().color = _isInvestCyber ? Color.blue : Color.white;

    }

    public void SelectITAlert()
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "SELECT IT ALERT");
        _alertIT = !_alertIT;
        
        if (_alertIT)//selection alerte
        {
            _isInvestIT = true;
            _isInvestCyber = false;
            AlertText.text = "Vous avez selectionné l'alerte Entropie.\n Vous pouvez maintenant analyser l'asset dans la vue 'IT'.";
            //maj texte explications
            if (_ITinfo)
            {
                AlertText.text = "Vous avez analysé l'asset dans la vue 'IT'.\n Vous pouvez maintenant Caractériser l'alerte.";
                AlertCaracterization.GetChild(0).GetComponent<Button>().interactable = true;
                AlertCaracterization.GetChild(1).GetComponent<Button>().interactable = true;
                AlertCaracterization.GetComponent<Text>().text = "Une fois une analyse effectuée dans la bonne vue, vous pouvez soit rejetter l'alerte, soit l'escalader, ainsi que donner une cause probable de l'alerte";

                //maj boutons
            }
            return;
        }
        _isInvestIT = false;
        AlertCaracterization.GetComponent<Text>().text = "";

        AlertText.text = "Les boutons Alerte Entropie et Alerte Réseau vous permettent de récupérer des informations sur les problèmes et d'investiguer dans les différentes vues";
        //maj texte
        if (_ITinfo)
        {
            //maj boutons
        }

    }
    public void SelectCyberAlert()
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "CYBER ALERT SELECTION");
        _alertCyber = !_alertCyber;
        
        if (_alertCyber)//selection alerte
        {
            _isInvestCyber = true;
            _isInvestIT = false;
            AlertText.text = "Vous avez selectionné l'alerte Réseau.\n Vous pouvez maintenant analyser l'asset dans la vue 'Cyber'.";
            // maj boutons alertes

            //maj texte explications
            if (_CYBERinfo)
            {
                AlertText.text = "Vous avez analysé l'asset dans la vue 'Cyber'.\n Vous pouvez maintenant Caractériser l'alerte.";
                AlertCaracterization.GetComponent<Text>().text = "Une fois une analyse effectuée dans la bonne vue, vous pouvez soit rejetter l'alerte, soit l'escalader, ainsi que donner une cause probable de l'alerte";
                AlertCaracterization.GetChild(0).GetComponent<Button>().interactable = true;
                AlertCaracterization.GetChild(1).GetComponent<Button>().interactable = true;
                //maj boutons
            }
            return;
        }
        _isInvestCyber = false;
        AlertCaracterization.GetComponent<Text>().text = "";

        AlertText.text = "Les boutons Alerte Entropie et Alerte Réseau vous permettent de récupérer des informations sur les problèmes et d'investiguer dans les différentes vues";

        //maj texte
        if (_CYBERinfo)
        {
            //maj boutons
        }
    }


    public void FifthButton()
    {
        AlertParticles.Stop();
        Panel6.SetActive(false);
        Panel7.SetActive(true);
        Asset.SetActive(false);
        Panel7.GetComponent<AudioSource>().Play();
        Panel7.transform.Find("Text").GetComponent<Text>().text = "Bravo, Vous avez terminé le tutoriel!\n\n" +
            "Vous avez appris à vous déplacer, à sélectionner les objets et à activer les menus d'information!\n Appuyez sur suivant pour passer à l'expérimentation";
        Panel7.transform.Find("Text (1)").GetComponent<Text>().text = "";
        //rajouter le bouton suivant?

    }

    public void LastButton() //start expe
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "END BUTTON");
        CyberCopStaticLog.CloseTutoWriter();
        //StartCoroutine(LoadAsyncSceneExpe());
    }

    IEnumerator LoadAsyncSceneExpe()
    {
        AsyncOperation asyncLoad = isGraph? SceneManager.LoadSceneAsync("CyberCOPEventsVR_cybercopEvents_testHolisticapp_ExpeGraph") : SceneManager.LoadSceneAsync("CyberCOPEventsVR_cybercopEvents_testHolisticapp_ExpeNav");
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }


    public void AssetClikIT()
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "ASSET IT CLIK");
        Asset.transform.GetChild(0).GetChild(0).gameObject.SetActive(_it);
        //maj boutons?
        if (_it)
        {
            if (!_ITinfo)
            {
                foreach (Button b in ITButtons)
                {
                    b.interactable = _isInvestIT;
                }
            }
            else
            {
                foreach (Button b in ITButtons)
                {
                    b.interactable = false;
                }
            }
        }
        _it = !_it;
        
    }
    public void AssetClikCyber()
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "ASSET CYBER CLIK");
        Asset.transform.GetChild(1).GetChild(0).gameObject.SetActive(_cyber);
        if (_cyber)
        {
            if (!_CYBERinfo)
            {
                foreach (Button b in CyberButtons)
                {
                    b.interactable = _isInvestCyber;
                }
            }
            else
            {
                foreach (Button b in CyberButtons)
                {
                    b.interactable = false;
                }
            }
        }
        _cyber = !_cyber;
    }
    public void AssetClikAlert()
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "ASSET ALERT CLIK");
        Asset.transform.GetChild(2).GetChild(0).gameObject.SetActive(_alert);
        _alert = !_alert;
    }

    public void ITAnalysis()
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "IT ANALYSIS");
        _ITinfo = true;
        AlertText.text = "Vous avez analysé l'asset dans la vue 'IT'.\n Vous pouvez maintenant Caractériser l'alerte.";
        AlertCaracterization.GetComponent<Text>().text = "Une fois une analyse effectuée dans la bonne vue, vous pouvez soit rejetter l'alerte, soit l'escalader, ainsi que donner une cause probable de l'alerte";
        AlertCaracterization.GetChild(0).GetComponent<Button>().interactable = true;
        AlertCaracterization.GetChild(1).GetComponent<Button>().interactable = true;
        //maj boutons alerte
    }

    public void CyberAnalysis()
    {
        CyberCopStaticLog.WriteTutoAction(Time.time, "CYBER ANALYSIS");
        _CYBERinfo = true;
        AlertText.text = "Vous avez analysé l'asset dans la vue 'Cyber'.\n Vous pouvez maintenant Caractériser l'alerte.";
        AlertCaracterization.GetComponent<Text>().text = "Une fois une analyse effectuée dans la bonne vue, vous pouvez soit rejetter l'alerte, soit l'escalader, ainsi que donner une cause probable de l'alerte";
        AlertCaracterization.GetChild(0).GetComponent<Button>().interactable = true;
        AlertCaracterization.GetChild(1).GetComponent<Button>().interactable = true;
        //maj boutons alerte
    }

    public void DismissAlert()//bouton dismiss
    {
        AssetsCounts();
        if (_isInvestCyber)
        {
            CyberCopStaticLog.WriteTutoAction(Time.time, "DISMISS CYBER");
            AlertButtons.GetChild(0).GetComponent<Button>().interactable = false;
            AlertCaracterization.GetChild(0).GetComponent<Button>().interactable = false;
            AlertCaracterization.GetChild(1).GetComponent<Button>().interactable = false;
            AlertText.text = "Vous avez caractérisé l'alerte cyber.\n Caractérisez l'alerte entropie si ce n'est fait.";
            AlertCaracterization.GetComponent<Text>().text = "Alerte Rejetée";

        }
        if (_isInvestIT)
        {
            CyberCopStaticLog.WriteTutoAction(Time.time, "DISMISS IT");
            AlertButtons.GetChild(1).GetComponent<Button>().interactable = false;
            AlertCaracterization.GetChild(0).GetComponent<Button>().interactable = false;
            AlertCaracterization.GetChild(1).GetComponent<Button>().interactable = false;
            AlertText.text = "Vous avez caractérisé l'alerte it.\n Caractérisez l'alerte réseau si ce n'est fait.";
            AlertCaracterization.GetComponent<Text>().text = "Alerte Rejetée";
        }
    }

    public void EscalateAlert()
    {
        AssetsCounts();
        if (_isInvestCyber)
        {
            CyberCopStaticLog.WriteTutoAction(Time.time, "ESCALATE CYBER");
            AlertButtons.GetChild(0).GetComponent<Button>().interactable = false;
            AlertCaracterization.GetChild(0).GetComponent<Button>().interactable = false;
            AlertCaracterization.GetChild(1).GetComponent<Button>().interactable = false;
            AlertText.text = "Vous avez caractérisé l'alerte cyber.\n Caractérisez l'alerte entropie si ce n'est fait.";
            AlertCaracterization.GetComponent<Text>().text = "Alerte Escaladée";

        }
        if (_isInvestIT)
        {
            CyberCopStaticLog.WriteTutoAction(Time.time, "ESCALATE IT");
            AlertButtons.GetChild(1).GetComponent<Button>().interactable = false;
            AlertCaracterization.GetChild(0).GetComponent<Button>().interactable = false;
            AlertCaracterization.GetChild(1).GetComponent<Button>().interactable = false;
            AlertText.text = "Vous avez caractérisé l'alerte it.\n Caractérisez l'alerte réseau si ce n'est fait.";
            AlertCaracterization.GetComponent<Text>().text = "Alerte Escaladée";
        }
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
