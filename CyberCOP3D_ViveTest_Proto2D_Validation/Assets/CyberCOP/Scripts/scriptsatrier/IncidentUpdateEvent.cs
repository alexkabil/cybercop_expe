﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncidentUpdateEvent : MonoBehaviour {

	public CyberCopEvent IncidentEvent;


	// Use this for initialization
	void Start () 
	{
		
	}

    private void OnEnable()
    {
        AlertUpdateEventListener.AlertEscalationDelegate += AlertEscalation;
    }

    private void OnDisable()
    {
        AlertUpdateEventListener.AlertEscalationDelegate -= AlertEscalation;
    }

    public void IncidentUpdateState(CyberCopEventsInformation e)
	{
		IncidentEvent.Raise(e,true);
	}

    public void AlertEscalation(CyberCopEventsInformation e)
    {
        IncidentEvent.Raise(new CyberCopEventsInformation(e.Asset, e.View, (int)IncidentUpdateActions.INCIDENTCREATION, e.User, e.TicketID), true);
    }

	// Update is called once per frame
	void Update () 
	{
		
	}
}
