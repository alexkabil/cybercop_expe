﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ButtonAssetDelegateAction : MonoBehaviour,IPointerClickHandler {
    public delegate void OnButtonClickDelegate(CyberCopEventsInformation e);
    public static event OnButtonClickDelegate ButtonDelegate;
    public int butID=0;
    public int isKinetic = 0;
    private int _action =0;
    private int _user = 1;// en fonction du current user id?
	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void SetUserStatus(int user)
    {
        _user = user;
    }

	// Update is called once per frame
	void Update ()
    {
		
	}

    public void HighlightAsset(bool b) { }
    public void Selection(bool b)
    {
        GetComponent<ButtonHighlighter>().Highlight(b); //changer ca pour mettre directement sur ce script?
    }
    public void SelectionOther(bool b)
    {
        GetComponent<ButtonHighlighter>().OtherHighlight(b);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
       // Debug.Log(eventData.selectedObject.name);
        _action++;
        int act = _action % 2 == 0 ? (int)RequiredActions.ASSETUNSELECTION : (int)RequiredActions.ASSETSELECTION; //l'ui est genéraliste...

        ButtonDelegate(new CyberCopEventsInformation(butID,isKinetic,act,_user,0));//assetselection avec un numéro de ticket ??? envoi geré par user?
        //throw new System.NotImplementedException();
    }
}
