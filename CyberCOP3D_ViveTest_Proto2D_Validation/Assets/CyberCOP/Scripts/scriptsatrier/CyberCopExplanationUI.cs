﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberCopExplanationUI : MonoBehaviour {
    public RectTransform panel1;
    private Vector2 delta;
    private Vector3 scale;

	// Use this for initialization
	void Start ()
    {
        delta = transform.GetChild(0).GetComponent<RectTransform>().sizeDelta;
        scale = transform.GetChild(0).GetComponent<RectTransform>().localScale;
        StartCoroutine(PlaceExplanationUI());
    }

    public IEnumerator PlaceExplanationUI()
    {
        bool init = true;
        float timer = 0;
        while (init)
        {
            timer += Time.deltaTime;
            if (GameObject.FindGameObjectWithTag("MainCamera") != null)
            {
                transform.SetParent(GameObject.FindGameObjectWithTag("MainCamera").transform);
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                Vector2 scalability = new Vector2(Camera.main.scaledPixelWidth/ transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x, Camera.main.scaledPixelHeight / transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y);
                transform.GetChild(0).GetComponent<RectTransform>().localScale = new Vector2(scale.x*scalability.x, scale.y * scalability.y);
                init = false;
                yield return null;
            }
            if (timer > 5.0f)
            {
                init = false;
                yield return null;
            }
            yield return null;
        }
        yield return null;
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
