﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Text;

public class NetworkEventManager : MonoBehaviour 
{
	static UdpClient server;
	Thread thread;
	string message="";
	string clientaddress="";
	static UdpClient client;
	private bool _stop = false;


	private void OnEnable() 
	{
		thread = new Thread(new ThreadStart(ThreadReceiver));
		thread.Start();
	}
	private void OnDisable() 
	{
		if(client!=null)
		{
			client.Close();
		}
		server.Close();
		thread.Abort();
		_stop=true;
	}


	public void DiscoverMsg()
	{
		UdpClient broadC = new UdpClient();
		IPEndPoint ipBroad = new IPEndPoint(IPAddress.Broadcast,12345);
		broadC.EnableBroadcast = true;
		byte[] msg = Encoding.ASCII.GetBytes("IP");
		broadC.Send(msg,msg.Length,ipBroad);
		broadC.EnableBroadcast=false;
		broadC.Close();
	}
	// Use this for initialization
	void Start () 
	{
		
	}

	private void ThreadReceiver()
	{
		server = new UdpClient(12345);
		while(true)
		{
			IPEndPoint ServEndPoint = new IPEndPoint(IPAddress.Any,0);
			byte[] receive = server.Receive(ref ServEndPoint);
			message = ServEndPoint.Address.ToString()+";"+Encoding.ASCII.GetString(receive);
		}
	}

	public void Send(string s)
	{
		byte[] msg = Encoding.ASCII.GetBytes(s);
		client.Send(msg,msg.Length);
	}

	void CheckMessages()
	{
		if(message.Contains("IP"))
		{
			foreach(IPAddress ip in Dns.GetHostAddresses(Dns.GetHostName()))
			{
				if(ip.Equals(IPAddress.Parse(message.Split(';')[0])))
				{
					Debug.Log("C'est moi!");
					message="";
					return;
				}
			}
			if(clientaddress=="")
			{
				clientaddress=message.Split(';')[0];
				Debug.Log("On a l'addresse du client et c'est : "+clientaddress);
                KnownClient(clientaddress);
                Send("IP");
				message ="";
			}
            else
            {
                Debug.Log("ping pong");
                message = "";
            }
			return;
		}
        if (message.Contains("Test"))
        {
            Debug.Log("J'ai recu un message de " + message.Split(';')[0]);
            message = "";
            return;
        }
	}
    private void KnownClient(string address)
    {
        client = new UdpClient(address, 12345);
    }
    // Update is called once per frame
    void Update () 
	{
		CheckMessages();
		if(Input.GetKeyDown(KeyCode.M))
		{
			DiscoverMsg();
		}
        if (Input.GetKeyDown(KeyCode.N))
        {
            Send("Test");
        }
	}
}
