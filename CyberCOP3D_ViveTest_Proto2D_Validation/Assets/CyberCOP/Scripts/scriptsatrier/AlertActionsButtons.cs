﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
//boutons quand on sélectionne une alerte sur la vue coord.
public class AlertActionsButtons : MonoBehaviour, IPointerClickHandler {

    public RequiredActions ButtonAction;
    private CyberCopEventsInformation contextualEvent;
    private bool _isActive = true;
    private int _view;

    public delegate void OnButtonClickDelegate(CyberCopEventsInformation e);
    public static event OnButtonClickDelegate ButtonDelegate;

    // Use this for initialization
    void Start ()
    {
		
	}
	
    public void SetButtonState(CyberCopEventsInformation e, List<RequiredActions> actionsdone)
    {
        contextualEvent = e;
        _view = e.View;
        GetComponent<UnityEngine.UI.Button>().interactable = false;
        GetComponent<UnityEngine.UI.Image>().color = Color.white;
        if (ButtonAction==RequiredActions.ASK_ANALYST_CYBERINVESTIGATION || ButtonAction == RequiredActions.ASK_ANALYST_KINETICINVESTIGATION)
        {
            _view = ButtonAction == RequiredActions.ASK_ANALYST_CYBERINVESTIGATION ? 0 : 1;
            if (actionsdone.Contains(RequiredActions.ASK_ANALYST_KINETICINVESTIGATION) || actionsdone.Contains(RequiredActions.ASK_ANALYST_CYBERINVESTIGATION))
            {
                GetComponent<UnityEngine.UI.Button>().interactable = false;
                GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f, 0.4f, 0.8f, 0.8f);
            }
            else
            {
                GetComponent<UnityEngine.UI.Button>().interactable = true;
            }
        }
        else if (ButtonAction == RequiredActions.REPORTREAD_COORD)
        {
            if (actionsdone.Contains(RequiredActions.REPORTSEND_ANALYST) && !actionsdone.Contains(RequiredActions.REPORTREAD_COORD))
            {
                GetComponent<UnityEngine.UI.Button>().interactable = true;
            }
            else
            {
                if (actionsdone.Contains(RequiredActions.REPORTREAD_COORD))
                {
                    GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f, 0.4f, 0.8f, 0.8f);
                }
                GetComponent<UnityEngine.UI.Button>().interactable = false;
            }
        }
        else if(ButtonAction==RequiredActions.ALERTINCIDENT || ButtonAction == RequiredActions.ALERTDISCARD)
        {
            if (actionsdone.Contains(RequiredActions.REPORTREAD_COORD))
            {
                if (actionsdone.Contains(RequiredActions.ALERTDISCARD) || actionsdone.Contains(RequiredActions.ALERTINCIDENT))
                {
                    GetComponent<UnityEngine.UI.Button>().interactable = false;
                    GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f, 0.4f, 0.8f, 0.8f);
                }
                else
                {
                    GetComponent<UnityEngine.UI.Button>().interactable = true;
                }
            }
        }
        else
        {
            GetComponent<UnityEngine.UI.Button>().interactable = false;
        }
        
        //mettre marqueur si action déja faite?
    }
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void HighlightButtonScenario(CyberCopEventsInformation e)
    {
        if (e.Action == (int)ButtonAction)
        {
            GetComponent<UnityEngine.UI.Image>().color = Color.green;
        }

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!GetComponent<UnityEngine.UI.Button>().IsInteractable())
        {
            return;
        }
        //GetComponent<UnityEngine.UI.Image>().color = Color.blue;
        _isActive = !_isActive;//garder en mémoire que l'action a été faite?? faire le lien avec la liste des actions??
        CyberCopEventsInformation click = new CyberCopEventsInformation(contextualEvent.Asset, _view, (int)ButtonAction, contextualEvent.User, contextualEvent.TicketID);
        ButtonDelegate(click); //on ne peut appuyer que si c'est la nextaction? ou alors on passe toujours le buttonAction?
        //throw new System.NotImplementedException();
    }
}
