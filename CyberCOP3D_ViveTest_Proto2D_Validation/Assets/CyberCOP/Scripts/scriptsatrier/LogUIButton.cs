﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogUIButton : MonoBehaviour
{
    public Transform LogButtonParent;
    public GameObject LogPrefab;
	// Use this for initialization
	void Start ()
    {
		
	}

    public void CreateLog(string s)
    {

            var button = GameObject.Instantiate(LogPrefab, LogButtonParent);

            button.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = s;
            button.transform.SetAsFirstSibling();

            LogButtonParent.GetComponent<RectTransform>().sizeDelta += new Vector2(0, button.GetComponent<RectTransform>().sizeDelta.y);

    }



    // Update is called once per frame
    void Update ()
    {
		
	}
}
