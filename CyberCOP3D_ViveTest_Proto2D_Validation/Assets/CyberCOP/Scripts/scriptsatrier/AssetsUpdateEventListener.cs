﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AssetsUpdateEventListener : MonoBehaviour {
    private AssetInfoList Scenario;
    public Transform CyberAssetsFather;
    public Transform KineticAssetsFather;

    public CyberCOP3DInteractiveObject[] Kinetic3DAssets;
    public CyberCOP3DInteractiveObject[] Cyber3DAssets;

    private CyberCOP3DInteractiveObject[] _assets;
    private ButtonAssetDelegateAction[] _2Dassets;
    public Transform Asset2DButtonsParent;
    public UnityEngine.UI.Text AssetNumberText;
    public UnityEngine.UI.Text AssetTextExplain;
    private bool _asset2DActivation = false;

    public delegate void OnTicketStateUpdate(CyberCopEventsInformation e);
    public static event OnTicketStateUpdate TicketUpdateDelegate;

    public delegate void OnAssetUIUpdate(CyberCopEventsInformation e);
    public static event OnAssetUIUpdate UIUpdateDelegate;

    public delegate void OnAlertStateUpdate(CyberCopEventsInformation e);
    public static event OnAlertStateUpdate AlertUpdateDelegate;

    private CyberCopEventsInformation previousAssetUpdate = new CyberCopEventsInformation(999,999,999,999,999);


    //delegates dégueux, à voir si on change..
    private void OnEnable()
    {
        AlertUpdateEventListener.AssetUIUpdateDelegate += AssetUIModification;
    }
    private void OnDisable()
    {
        AlertUpdateEventListener.AssetUIUpdateDelegate -= AssetUIModification;
    }



    // Use this for initialization
    void Start ()
    {
        //ajouter tous les assets qui ont le meme script dans un tableau avec findgameobjectoftype?
        _assets = FindObjectsOfType<CyberCOP3DInteractiveObject>();
        _2Dassets = FindObjectsOfType<ButtonAssetDelegateAction>();

        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }
	
    public void AssetUpdateState(CyberCopEventsInformation e, bool b)
    {
        if(previousAssetUpdate.Action==e.Action && previousAssetUpdate.Asset==e.Asset && previousAssetUpdate.TicketID == e.TicketID && previousAssetUpdate.User == e.User && previousAssetUpdate.View == e.View)
        {
            Debug.Log("meme event asset");
            return;
        }
        //Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e); //on vire ca d'ici, on met des required actions quand ca concerne les assets dans le listener user (ou le truc à delegates)
        previousAssetUpdate = e;
        //UIUpdateDelegate(e);//changer ptet pour filtrer un peu plus??mettre apres action non?
        switch (e.Action)
        {
            //filtrage avec l'enum des assets ici wesh
            case (int)AssetUpdateActions.ASSETSELECTION://prise en compte de la sélection d'asset en vue alerte
                AssetSelectionType(e, true);
                HighlightAssetAfterTicketValidation(e, false);
                AssetUIModification(e);
                break;
            case (int)AssetUpdateActions.ASSETUNSELECTION:
                AssetSelectionType(e, false);
                AssetUIModification(e);
                break;
            case (int)AssetUpdateActions.ALERT:
                AssetAlert(e);
                break;
            case (int)AssetUpdateActions.METRICRAISE:
                ValueRaise(e);
                break;
            case (int)AssetUpdateActions.ADDMALICIOUSPROCESSES:
                AddProcesses(e, true);
                break;
            case (int)AssetUpdateActions.ADDLEGITPROCESSES:
                AddProcesses(e, false);
                break;
            case (int)AssetUpdateActions.ASSETINFORMATION:
                AssetUIModification(e);
                //Debug.Log("assetinfodemandees...");
                break;
            case (int)AssetUpdateActions.ASSETANALYSIS:
                AssetUIModification(e);
                Analysis(e);
                //Debug.Log("assetinfodemandees...");//voir ce qu'on met ici ?? changement sur l'asset??
                break;
            case (int)AssetUpdateActions.HIGHLIGHTASSET:
                HighlightAssetAfterTicketValidation(e, true);
                break;
            case (int)AssetUpdateActions.UNHIGHLIGHTASSET:
                HighlightAssetAfterTicketValidation(e, false);
                break;
            case (int)AssetUpdateActions.REPORTASSET:
                AssetUIModification(e);
                break;
            default:
                break;
        }
        //UIUpdateDelegate(e);//changer ptet pour filtrer un peu plus??mettre apres action non?
        //UI3DUpdate(e);
    }
    //action déclenchée que par et pour moi ! donc pas de soucis de ticket et tout..
    public void HighlightAssetAfterTicketValidation(CyberCopEventsInformation e, bool b)
    {
        if (Scenario.Currentuser.isImmersive)
        {
            //commenté pour les expés!
            /*
            var asset = (from item in _assets where item.assetId == e.Asset && item.view == e.View select item).FirstOrDefault();
            if (asset != null)
            {
                asset.HighlightAsset(b);
            }
            */
        }
        else
        {
            //rajouter trucs highlight 2D
        }
    }

    public void AddProcesses(CyberCopEventsInformation e, bool b)
    {
        if (Scenario.Currentuser.isImmersive)
        {
            var asset = (from item in _assets where item.GetAssetInfoID() == e.Asset && item.view == e.View select item).FirstOrDefault();
            if (asset != null)
            {
                asset.SetProcesses(b);
            }
        }
    }

    public void Analysis(CyberCopEventsInformation e)
    {
        if (Scenario.Currentuser.isImmersive)
        {
            var asset = (from item in _assets where item.GetAssetInfoID() == e.Asset && item.view == e.View select item).FirstOrDefault();
            if (asset != null)
            {
                asset.SetAnalysisText();
            }
        }
    }

    public void AssetUIModification(CyberCopEventsInformation e)
    {
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        var actionsAssetUser = (from item in Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset where item.View == e.View &&item.User==e.User /*&& item.TicketID==e.TicketID*/ select (RequiredActions)item.Action).ToList();
        //virer le ticket pour tjs voir les infos une fois qu'on les a ??
        //check l'état interne de l'asset tt simplement aussi? ...
        //tout passer en lastselectedasset???
        //remplacer ca par actions sur ticket???
        //on a que les actions faites par le user qui modif la ui..
        RequiredActions actiontodo = RequiredActions.NONE;
        if (e.TicketID != -1)//on check si l'user investigue un ticket??
        {
            if (Scenario.GetAttack(e.TicketID).assetConcerned == e.Asset && e.View==user.ticketView/*Scenario.GetAttack(e.TicketID).cyberPhysical == e.View*/)
            {
                actiontodo = Scenario.GetAttack(e.TicketID).GetCurrentAction();
                //actionsAssetUser = Scenario.Attacks[e.TicketID].ActionsPerformed;
                //rajout ici pour que ca concerne les tickets, à voir...
            }
        }
        if (!Scenario.Currentuser.isImmersive)
        {
            //en cours..
            //prévoir si qqun d'autre sélectionne l'asset?
            if (user != Scenario.Currentuser)
            {
                //si c'est moi qui fait qqch, l'affichage est là normalement, mais mettre la garde
                return;
            }
            Asset2DModification(e);
            /*
            var asset = (from item in _2Dassets where item.butID== e.Asset && item.isKinetic== e.View select item).FirstOrDefault();
            if (asset != null)
            {
                //maj etat asset et des uis.. des que qqun fait un truc..
                //Asset2DModification(e);
            }
            */
        }
        else
        {
            var asset = (from item in _assets where item.GetAssetInfoID() == e.Asset && item.view == e.View select item).FirstOrDefault();
            if (asset != null)
            {
                asset.UIState(e, actiontodo, actionsAssetUser);
            }
        }
    }

    public void Asset2DModification(CyberCopEventsInformation e)
    {
        if (!_asset2DActivation)
        {
            return;
        }
        UserStereotypeSO.AssetSelected lastAsset = Scenario.Currentuser.LastAssetSelected();
        CyberCopEventsInformation lastAssetevent = new CyberCopEventsInformation(lastAsset.id, lastAsset.view, e.Action, e.User, e.TicketID);
        AssetNumberText.text = "Asset " + Scenario.Currentuser.LastAssetSelected().id + " vue " + Scenario.Currentuser.LastAssetSelected().view;
        var actionsAssetUser = (from item in Scenario.Assets[lastAssetevent.Asset - 1].State.ActionsDoneOnAsset where item.View == lastAssetevent.View && item.User == lastAssetevent.User && item.TicketID == lastAssetevent.TicketID select (RequiredActions)item.Action).ToList();

        RequiredActions actiontodo = RequiredActions.NONE;
        if (lastAssetevent.TicketID != -1)//on check si l'user investigue un ticket??
        {
            if (Scenario.GetAttack(e.TicketID).assetConcerned == lastAssetevent.Asset && lastAssetevent.View == Scenario.Currentuser.ticketView )
            {
                actiontodo = Scenario.GetAttack(lastAssetevent.TicketID).GetCurrentAction();
                //actionsAssetUser = Scenario.Attacks[e.TicketID].ActionsPerformed;
                //rajout ici pour que ca concerne les tickets, à voir...
            }
        }
        Set2DInfoText(lastAssetevent, actionsAssetUser);
        foreach (Transform tr in Asset2DButtonsParent)
        {
            if (tr.GetComponent<AssetActionsButtons>() != null)
            {
                //action sur chaque bouton indépendament (activation et tout)
                AssetActionsButtons buttonaction = tr.GetComponent<AssetActionsButtons>();
                buttonaction.SetButtonState(lastAssetevent);
                buttonaction.ActivateActions(Scenario.Currentuser.ticketView == lastAssetevent.View && actiontodo != RequiredActions.NONE, actionsAssetUser);
                buttonaction.ActionAlreadyDone(actionsAssetUser);//passage dans un premier temps des actions faites et tout
            }
                //gérer etat des boutons en fonction ticket avancement et etat  asset...
        }
        //maj des boutons en fonction du ticket et tout..
    }
    public void Set2DInfoText(CyberCopEventsInformation e, List<RequiredActions> actionsdoneuser)
    {
        AssetInformation _assetInfo = Scenario.Assets[e.Asset - 1];
        //revoir l'affichage en fonction de ce qui a été fait et de l'action passée..
        AssetTextExplain.text = e.View == 0 ? _assetInfo.InitCyberInfo() : _assetInfo.InitPhysicInfo();
        if (actionsdoneuser.Contains(RequiredActions.ASSETINFORMATION))
        {
            AssetTextExplain.text = e.View == 0 ? _assetInfo.CyberInfo() : _assetInfo.PhysInfo();
            if (actionsdoneuser.Contains(RequiredActions.ASSETANALYSIS))
            {
                AssetTextExplain.text = e.View == 0 ? _assetInfo.AllCyberInfo() : _assetInfo.AllPhysInfo();//a voir si on laisse...
            }
        }
    }
    public void Asset2DActivation()
    {
        if(Scenario.Currentuser.GetSelectedAssets().Count == 0)
        {
            _asset2DActivation = false;
            foreach (Transform tr in Asset2DButtonsParent)
            {
                tr.gameObject.SetActive(false);
                //gérer etat des boutons en fonction ticket avancement et etat  asset...
            }
            AssetNumberText.text = "Plus d'asset selectionne";
            AssetTextExplain.text = "";
            return;
        }
        if (Scenario.Currentuser.GetSelectedAssets().Count == 1)
        {
            _asset2DActivation = true;
            foreach (Transform tr in Asset2DButtonsParent)
            {
                tr.gameObject.SetActive(true);
                //gérer etat des boutons en fonction ticket avancement et etat  asset...
            }
            AssetNumberText.text = "Asset Selectionne";
        }
    }

    public void ValueRaise(CyberCopEventsInformation e)
    {
        if (Scenario.Currentuser.isImmersive)
        {
            var asset = e.View == 0 ? (from element in Cyber3DAssets where element.GetAssetInfoID() == e.Asset select element).FirstOrDefault() : (from element in Kinetic3DAssets where element.GetAssetInfoID() == e.Asset select element).FirstOrDefault();
            if (asset != null)
            {
                asset.SetHighValue();//maj objet et apparition ui, que pour moi.
            }
        }
    }

    public void AssetAlert(CyberCopEventsInformation e)
    {
        if (e.User == 100)//system?
        {
            if (e.View == 0) //cyber
            {
                var asset = (from element in CyberAssetsFather.GetComponentsInChildren<ButtonAssetDelegateAction>() where element.butID == e.Asset select element).First();
                if (asset != null)
                {
                    //changement assets.[e.asset-1] différent de assetconcerned truc comme ca
                    if(Scenario.Assets[e.Asset - 1].State.cyberState == AssetStatesInformation.assetTypeStates.NORMAL)
                    {
                        Scenario.Assets[e.Asset - 1].State.cyberState = AssetStatesInformation.assetTypeStates.ALERT;
                        asset.GetComponent<ButtonHighlighter>().AlertText(true);
                    }
                }
                AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTCREATION, e.User, e.TicketID));
                return;
            }
            if (e.View == 1) //kinetic
            {
                var asset = (from element in KineticAssetsFather.GetComponentsInChildren<ButtonAssetDelegateAction>() where element.butID == e.Asset select element).First();
                if (asset != null)
                {
                    if (Scenario.Assets[e.Asset - 1].State.kineticState == AssetStatesInformation.assetTypeStates.NORMAL)
                    {
                        Scenario.Assets[e.Asset - 1].State.kineticState = AssetStatesInformation.assetTypeStates.ALERT;
                        asset.GetComponent<ButtonHighlighter>().AlertText(true);
                    }
                }
                AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTCREATION, e.User, e.TicketID));
                return;
            }
            return;
        }
        else
        {
            return;
        }
    }

    public void AssetSelectionType(CyberCopEventsInformation e, bool b)
    {
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        //ici faire maj ui  et non pas dans uiupdate?..
        if (user==Scenario.Currentuser) //own user? avec id?
        {
            //rajouter filtrage en fonction du role? faire une fonction pour 3D ou non.
            if(!Scenario.Currentuser.isImmersive)
            {
                var asset = (from item in _2Dassets where item.isKinetic == e.View && item.butID == e.Asset select item).FirstOrDefault();
                if (asset != null)
                {
                    asset.Selection(b);
                    Asset2DActivation();
                }

               
            }
            else //immersive view
            {
                //var asset = (from element in _assets where element.assetId == e.Asset && element.view == e.View select element).FirstOrDefault();
                var asset = (from element in _assets where element.GetAssetInfoID() == e.Asset && element.view == e.View select element).FirstOrDefault();
                if (asset != null)
                {
                    asset.SetUserInteraction(e, b);//maj objet et apparition ui, que pour moi.
                }
            }

        }
        else
        {
            //separation 2D 3D
            var otheruser = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
            if(!Scenario.Currentuser.isImmersive)
            {
                //changement du truc 2D..
                var asset = (from item in _2Dassets where item.isKinetic == e.View && item.butID == e.Asset select item).FirstOrDefault();
                if (asset != null)
                {
                    asset.SelectionOther(b);//maj couleur mais pas affichage ui..
                }
            }
            else//immersive
            {
                var asset = (from element in _assets where element.GetAssetInfoID() == e.Asset && element.view == e.View select element).FirstOrDefault();
                if (asset != null)
                {
                    asset.OtherUserSelection(e, b,otheruser.isImmersive);//maj objet et apparition ui, que pour moi.
                }
            }
        }
    }

    public void ReportAsset(CyberCopEventsInformation e)
    {
        //modifier la teneur du rapport??
    }
    // Update is called once per frame
    void Update ()
    {
		
	}
}
