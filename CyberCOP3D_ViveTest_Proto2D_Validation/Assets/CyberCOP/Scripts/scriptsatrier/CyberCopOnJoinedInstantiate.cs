﻿using UnityEngine;
using System.Collections;

public class CyberCopOnJoinedInstantiate : MonoBehaviour
{
    public Transform SpawnPosition;
    public float PositionOffset = 2.0f;
    public GameObject PrefabsToInstantiate;   // set in inspector
    private AssetInfoList Scenario;

    public void OnJoinedRoom()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
        if (this.PrefabsToInstantiate != null)
        {
            Vector3 spawnPos = Vector3.up;
            if (this.SpawnPosition != null)
            {
                spawnPos = this.SpawnPosition.position;
            }

            Vector3 random = Random.insideUnitSphere;
            random.y = 0;
            random = random.normalized;
            Vector3 itempos = spawnPos + this.PositionOffset * random;

            PhotonNetwork.Instantiate(PrefabsToInstantiate.name, itempos, Quaternion.identity, 0);
            PhotonNetwork.Instantiate(Scenario.Currentuser.name, itempos, Quaternion.identity, 0);
        }
    }
}
