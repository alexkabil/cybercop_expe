﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberCopGraphExpe : MonoBehaviour {
    private CyberCOP3DInteractiveObject _assets;
    public Transform Assets;
	// Use this for initialization
	void Start ()
    {
		for(int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).position = Assets.GetChild(i).position;
        }
        SetLinks();
	}
	public void SetLinks()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            if (i < 5)
            {
                transform.GetChild(i).GetComponent<LineRenderer>().SetPosition(1, transform.GetChild(12).localPosition- transform.GetChild(i).localPosition);
            }
            else if (i < 9)
            {
                transform.GetChild(i).GetComponent<LineRenderer>().SetPosition(1, transform.GetChild(11).localPosition - transform.GetChild(i).localPosition);
            }
            else
            {
                transform.GetChild(i).GetComponent<LineRenderer>().SetPosition(1, transform.GetChild(transform.childCount-1).localPosition - transform.GetChild(i).localPosition);
            }
        }
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
