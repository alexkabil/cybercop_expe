﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioSceneManager : MonoBehaviour {
    public AssetInfoList Scene_Scenario;
    public UserStereotypeSO scene_CurrentUser;
    public GameObject TutorialManager;
    public bool isTuto;

    private void OnEnable()
    {
        Scene_Scenario.InitState();
        Scene_Scenario.Currentuser = scene_CurrentUser;//pour adapter le curentuser..
        TutorialManager.SetActive(isTuto);
        string s = scene_CurrentUser.Name;
        s += isTuto ? "_"+(0.ToString()) : "_" + Scene_Scenario.scenarioNumber.ToString();
        if (isTuto)
        {
            CyberCopStaticLog.InitTutoWriter(s);
        }
        else
        {
            CyberCopStaticLog.InitWriter(s);
            CyberCopStaticLog.InitWriterPos(s);
        }
        
        //ajout init de log avec bons parametres..
    }
    private void OnDisable()
    {
        Scene_Scenario.EndState();
        /*
        if (!isTuto)
        {
            CyberCopStaticLog.CloseWriter();
            CyberCopStaticLog.CloseWriterPos();
        }
        */
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
