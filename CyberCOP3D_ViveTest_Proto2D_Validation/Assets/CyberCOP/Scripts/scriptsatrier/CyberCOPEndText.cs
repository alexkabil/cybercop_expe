﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberCOPEndText : MonoBehaviour {

    // Use this for initialization
    public IEnumerator Start()
    {
        bool init = true;
        float timer = 0;
        while (init)
        {
            timer += Time.deltaTime;
            if (GameObject.FindGameObjectWithTag("MainCamera") != null)
            {
                transform.parent = GameObject.FindGameObjectWithTag("MainCamera").transform;
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.identity;
                init = false;
                yield return null;
            }
            if (timer > 5.0f)
            {
                Debug.Log("camera non trouvee");
                init = false;
                yield return null;
            }

            yield return null;
        }
        yield return null;
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
