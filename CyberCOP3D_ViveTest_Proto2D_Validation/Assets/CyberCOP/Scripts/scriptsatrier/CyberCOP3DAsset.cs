﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CyberCOP3DAsset : MonoBehaviour {
    private Transform PhysicalUI;
    private Transform CyberUI;
    private Button PhysInformationButton;
    private Button PhysIncidentButton;
    private Button CyberInformationButton;
    private Button CyberIncidentButton;
    //ca ou alors direct une action sur les boutons??
    private Text AssetText;//lien juste sur canvas??
    private Text CyberText;


    // Use this for initialization
    void Start ()
    {
        PhysicalUI = transform.GetChild(0).GetChild(0).GetChild(1);
        CyberUI = transform.GetChild(1).GetChild(0).GetChild(0);

        PhysInformationButton = PhysicalUI.GetChild(2).GetComponent<Button>();
        //PhysInformationButton.onClick.AddListener(GetAssetPhysInfo);

        PhysIncidentButton = PhysicalUI.GetChild(3).GetComponent<Button>();
        //PhysIncidentButton.onClick.AddListener(GetPhysIncidentButton);

        CyberInformationButton = CyberUI.GetChild(2).GetComponent<Button>();
        //CyberInformationButton.onClick.AddListener(GetAssetCyberInfo);

        CyberIncidentButton = CyberUI.GetChild(3).GetComponent<Button>();
       // CyberIncidentButton.onClick.AddListener(GetCyberIncidentButton);

        AssetText = PhysicalUI.GetChild(1).GetComponent<Text>();
        CyberText = CyberUI.GetChild(1).GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
