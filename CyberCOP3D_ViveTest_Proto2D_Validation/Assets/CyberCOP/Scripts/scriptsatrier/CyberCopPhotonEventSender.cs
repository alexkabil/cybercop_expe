﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberCopPhotonEventSender : Photon.PunBehaviour
{
    public CyberCopEvent userEvent;
    private List<float[]> savedActionswTs;
    private bool _init = true;
    public bool isLog = true;

    private void OnEnable()
    {
        savedActionswTs = new List<float[]>();
    }
    private void OnDisable()
    {
        if (isLog)
        {
            CyberCopStaticLog.WriteActions(savedActionswTs);
        }

        savedActionswTs.Clear();
    }


    public void SendEvent(CyberCopEventsInformation e)
    {
        int[] data = new int[] {e.Asset,e.View,e.Action,e.User,e.TicketID };
        photonView.RPC("PhotonCyberCopEvent", PhotonTargets.All, data);
    }

    public void SendActions()
    {
        if (savedActionswTs.Count > 0)
        {
            List<float[]> actions = new List<float[]>();
            for (int i = 0; i < savedActionswTs.Count; i++)
            {
                actions.Add(new float[] { savedActionswTs[i][0], savedActionswTs[i][1], savedActionswTs[i][2], savedActionswTs[i][3], savedActionswTs[i][4],savedActionswTs[i][5]});
            }
            photonView.RPC("PhotonRecoverActions", PhotonTargets.All, actions.ToArray());
        }
    }

	[PunRPC]
    public void PhotonCyberCopEvent(int[] data)
    {
        CyberCopEventsInformation e = new CyberCopEventsInformation(data[0], data[1], data[2], data[3], data[4]);
        //*Debug.Log("Event envoyé et recu: " + e.Asset + " " + e.View + " " + e.Action + " " + e.User + " " + e.TicketID);
        savedActionswTs.Add(new float[] { e.Asset, e.View, e.Action, e.User, e.TicketID, Time.time});
        userEvent.Raise(e, true);
    }

    [PunRPC]
    public void PhotonRecoverActions(float[][]c)
    {
        if (!_init || savedActionswTs.Count != 0)
        {
            return;
        }
        _init = false;
        List<float[]> actions = new List<float[]>(c);
        foreach(float[] act in actions)
        {
            CyberCopEventsInformation e = new CyberCopEventsInformation((int)act[0], (int)act[1], (int)act[2], (int)act[3], (int)act[4]);
            userEvent.Raise(e, false);//pour le log ? !! pas mal ouais
            savedActionswTs.Add(act);
        }
        
        /*
        for(int i = 0; i < obj.Count; i++)
        {
            CyberCopEventsInformation e = new CyberCopEventsInformation(obj[i][0], obj[i][1], obj[i][2], obj[i][3], obj[i][4]);
            Debug.Log("actions recues!!");
            userEvent.Raise(e, true);
        }*/
    }

}
