﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CyberCopMapAssetActions : MonoBehaviour {
    public Color alertITColor = Color.yellow;
    public Color alertCYBERColor;

    public Color selectColor = Color.blue;//pour l'instant on laisse comme ca ou on change en fonction des actions??

    private Image _ITasset;
    private Image _Cyberasset;
    private Image _selectionBorder;
    private Image _analysisImage;
    public int assetIndex;
    private int alert = 0;
	// Use this for initialization
	void Start ()
    {
        _ITasset = transform.GetChild(0).GetComponent<Image>();
        _Cyberasset = transform.GetChild(1).GetComponent<Image>();
        _selectionBorder = transform.GetChild(3).GetComponent<Image>();
        _analysisImage = transform.GetChild(2).GetComponent<Image>();
	}
	
    public void AssetOnAlert(int[] info)
    {
        if (info[0] == assetIndex)
        {
            alert++;
            if (alert == 1)
            {
                _Cyberasset.color = info[1] == 0 ? alertCYBERColor : alertITColor;
                _ITasset.color = info[1] == 0 ? alertCYBERColor : alertITColor;
            }
            else
            {
                if (info[1] == 0)
                {
                    _Cyberasset.color = alertCYBERColor;
                }
                if (info[1] == 1)
                {
                    _ITasset.color = alertITColor;
                }
            }
            if (info[1] == 0)
            {
                _Cyberasset.color = alertCYBERColor;
            }
            if (info[1] == 1)
            {
                _ITasset.color = alertITColor;
            }
        }
    }

    public void SelectOneOnMap(int i)
    {
        _selectionBorder.enabled = i == assetIndex ? true : false;
        if (i == assetIndex)
        {
            _selectionBorder.color = selectColor;
        }
    }

    public void SelectOnMap(int[] infoSelect)
    {
        if (infoSelect[0] == assetIndex)
        {
            _selectionBorder.enabled = infoSelect[1] == 1 ? true : false;
            _selectionBorder.color = selectColor;
        }
    }

    public void AcceptOnMap(int[] infoSelect)
    {
        if (infoSelect[0] == assetIndex)
        {
            _analysisImage.color = Color.blue;
            _analysisImage.enabled = infoSelect[1] == 1 ? true : false;
        }
    }

    //redondant vu qu'on broadcast??
    public void UnselectOneOnMap(int i)
    {
        if (i == assetIndex)
        {
            _selectionBorder.enabled = false;
        }
    }

    public void TicketingAsset(int[] info)
    {
        if (info[0] == assetIndex)
        {
            _analysisImage.enabled = true;
            _analysisImage.color = info[1] == 0? new Color(1, 0.2f, 0.8f, 1.0f): new Color(1, 0.65f, 0, 1.0f);
            _selectionBorder.color = info[1] == 0 ? new Color(1, 0.2f, 0.8f, 1.0f) : new Color(1, 0.65f, 0, 1.0f);
            //GetComponent<Image>().color = Color.white;
        }
    }

    public void AnalysisAsset(int i)
    {
        if (i == assetIndex)
        {
            _analysisImage.color = Color.blue;
            _selectionBorder.color = Color.blue;
        }
    }

    public void ReportAsset(int i)
    {
        if (i == assetIndex)
        {
            if (_analysisImage.enabled)
            {
                _analysisImage.color = Color.green;//new Color(1, 0.65f, 0, 1.0f);
            }
        }
    }
	// Update is called once per frame
	void Update ()
    {
		
	}
}
