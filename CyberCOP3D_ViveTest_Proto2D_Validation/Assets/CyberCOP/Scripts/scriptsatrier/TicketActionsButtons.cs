﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
//boutons quand on sélectionne une alerte sur la vue coord.
public class TicketActionsButtons : MonoBehaviour, IPointerClickHandler
{

    public RequiredActions ButtonAction;
    private CyberCopEventsInformation ticketEvent;
    private bool _isActive = true;
    public bool isAcceptationButton;
    public delegate void OnButtonClickDelegate(CyberCopEventsInformation e);
    public static event OnButtonClickDelegate ButtonDelegate;
    public int ticketid;
    public int buttonCreator;
    private bool _needclose = false;
    private bool _isAccepted = false;
    public bool isClosed = false;

    // Use this for initialization
    void Start()
    {

    }
    public bool NeedClose()
    {
        return _needclose;
    }
    public bool isAccepted()
    {
        return _isAccepted;
       
    }
    public void AcceptTicket()
    {
        _isAccepted = true;
    }

    public void NeedCloseTicket(bool b)
    {
        _needclose = b;
    }
    public bool getActivity()
    {
        return _isActive;
    }
    public void ChangeActivity()
    {
        _isActive = true;
    }
    public void Highlight()
    {
        GetComponent<UnityEngine.UI.Image>().color = !_isActive ? new Color(0.1f, 0.8f, 0.4f, 0.6f) : new Color(0.8f, 0.1f, 0.4f, 0.6f);
    }

    public void Notification(bool b)
    {
        var high = gameObject.GetComponentsInChildren<UnityEngine.UI.Image>(); //le pere est pris en compte donc faut prendre le 2e
        if (high.Length > 1)
        {
            high[1].enabled = b;
        }
        //maj avec des particules?ou petit bidule sur le bouton..
    }

    public void ActivateButton(bool b)
    {
        GetComponent<UnityEngine.UI.Button>().interactable = b;
    }
    //testcommentairepourcommitinexistant
    public void SetButtonState(CyberCopEventsInformation e)
    {
        ticketEvent = e;
        ticketid = e.TicketID;
        if (isAcceptationButton)
        {
        }

        //GetComponent<UnityEngine.UI.Button>().interactable = (int)e.Action == (int)ButtonAction ? true : false; //faire du highlight plutot que de l'activation?
        
    }
    /*
    public void ActionAlreadyDone()
    {
        GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f, 0.4f, 0.8f, 0.5f);
    }
    */

    // Update is called once per frame
    void Update()
    {

    }
    //truc moche pour gérer différence entre bouton ticket qu'on sélectionne déselectionne et boutons de validation...
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!GetComponent<UnityEngine.UI.Button>().IsInteractable())
        {
            return;
        }
        if (!isAcceptationButton)
        {
            ButtonAction = _isActive ? RequiredActions.TICKETSELECTION : RequiredActions.TICKETUNSELECTION;
        }
        _isActive = !_isActive; // a voir si on met ca la?...
        //GetComponent<UnityEngine.UI.Image>().color = Color.blue;
        //garder en mémoire que l'action a été faite?? faire le lien avec la liste des actions??
        //mettre ca aussi dans highlight???
        //il faut transmettre le current user et non pas celui qui a crée le ticket...
        CyberCopEventsInformation click = new CyberCopEventsInformation(ticketEvent.Asset, ticketEvent.View, (int)ButtonAction, ticketEvent.User, ticketEvent.TicketID);
        ButtonDelegate(click); //on ne peut appuyer que si c'est la nextaction? ou alors on passe toujours le buttonAction?
        //throw new System.NotImplementedException();
    }
}
