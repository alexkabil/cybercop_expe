﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public static class CyberCopStaticLog
{
    private static StreamWriter _writer;
    private static StreamWriter _writerPos;
    private static StreamWriter _writerTuto;

    
    public static void InitTutoWriter(string s)
    {
        string user = s + "_" + System.DateTime.Now.Day + "_" + System.DateTime.Now.Month + "_" + System.DateTime.Now.Year + "__" + System.DateTime.Now.Hour + "_" + System.DateTime.Now.Minute + "_" + System.DateTime.Now.Second + "_";
        _writerTuto = new StreamWriter(Path.Combine(Application.persistentDataPath, user + "log.csv"), true);
        _writerTuto.WriteLine("Time;" + "Action;");
        _writerTuto.Flush();
    }

    public static void InitWriter(string s)
    {
        string user = s + "_" + System.DateTime.Now.Day + "_" + System.DateTime.Now.Month + "_" + System.DateTime.Now.Year + "__" + System.DateTime.Now.Hour + "_" + System.DateTime.Now.Minute + "_" + System.DateTime.Now.Second + "_";
        _writer = new StreamWriter(Path.Combine(Application.persistentDataPath, user + "log.csv"), true);
        _writer.WriteLine("Time;"+"Asset;"+"View;"+"Action;" + "User;" + "TicketID;");
        _writer.Flush();
    }
    public static void InitWriterPos(string s)
    {
        string user = s + "_" + System.DateTime.Now.Day + "_" + System.DateTime.Now.Month + "_" + System.DateTime.Now.Year + "__" + System.DateTime.Now.Hour + "_" + System.DateTime.Now.Minute + "_" + System.DateTime.Now.Second + "_";
        _writerPos = new StreamWriter(Path.Combine(Application.persistentDataPath, user + "Positionlog.csv"), true);
        _writerPos.WriteLine("Time;"/*+"user;"*/+"PosX;" + "PosY;" + "PosZ;" + "RotX;" + "RotX;" + "RotX;");
        _writerPos.Flush();
    }

    public static void WritePosition(List<float> positions)
    {
        string pos = "";
        foreach(float f in positions)
        {
            pos += f.ToString() + ";";
        }
        _writerPos.WriteLine(pos);
        _writerPos.Flush();
    }

    public static void WritePositions(List<float[]> positions)
    {
        if (_writerPos == null)
        {
            return;
        }
        foreach (float[] act in positions)
        {
            _writerPos.WriteLine(act[0] + ";" + act[1] + ";" + act[2] + ";" + act[3] + ";" + act[4] + ";" + act[5] + ";" + act[6]);
            _writerPos.Flush();
        }
        _writerPos.Close();
    }

    public static void CloseWriter()
    {
        if(_writer!=null)
        _writer.Close();
    }
    public static void CloseWriterPos()
    {
        if (_writerPos != null)
            _writerPos.Close();
    }

    public static void CloseTutoWriter()
    {
        if (_writerTuto != null)
        {
            _writerTuto.Close();
        }
    }

    public static void WriteTutoAction(float f, string s)
    {
        if (_writerTuto == null)
        {
            return;
        }
        _writerTuto.WriteLine(f.ToString() + ";" + s + ";");
        _writerTuto.Flush();
    }

    public static void WriteActions(List<float[]> actions)
    {
        if (_writer == null)
        {
            return;
        }
        foreach(float[] act in actions)
        {
            _writer.WriteLine(act[5] + ";" + act[0] + ";" + act[1] + ";" + System.Enum.GetName(typeof(RequiredActions), (int)act[2]) + ";" + act[3] + ";" + act[4]);
            _writer.Flush();
        }
        _writer.Close();
    }

}
