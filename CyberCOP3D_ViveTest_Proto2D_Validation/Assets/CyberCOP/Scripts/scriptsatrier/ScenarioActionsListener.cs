﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioActionsListener : MonoBehaviour {

    public delegate void OnAssetStateUpdate(CyberCopEventsInformation e);
    public static event OnAssetStateUpdate AssetUpdateDelegate;
    public delegate void OnViewUpdate(int i);
    public static event OnViewUpdate CyberPhysicalViewDelegate;
    public delegate void OnUIUpdate(bool b);
    public static event OnUIUpdate UIUpdateDelegate;
    private CyberCopEventsInformation previousSystemAction = new CyberCopEventsInformation(999, 999, 999, 999, 999);
    private AssetInfoList Scenario;
    
    // Use this for initialization
    void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }

    public void ScenarioActionsReceived(CyberCopEventsInformation e, bool b)
    {
        if (previousSystemAction.Action == e.Action && previousSystemAction.Asset == e.Asset && previousSystemAction.TicketID == e.TicketID && previousSystemAction.User == e.User && previousSystemAction.View == e.View)
        {
            Debug.Log("meme event systeme");
            return;
        }
        previousSystemAction = e;

        //filtrage par utilisateur d'abord? 0==me filtrage en fonction du scénario?? pas pour le scenario? 0 veut ptet dire (systeme?) user = 100?
        if (e.User == 100)
        {
            //filtrage par action?
            switch (e.Action)
            {
                case (int)SystemActions.BEGINATTACK:
                    //changement action user vers action asset 
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.METRICRAISE, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ALERT, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ADDMALICIOUSPROCESSES, e.User, e.TicketID));
                    break;
                case (int)SystemActions.BEGINFALSEATTACK:
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.METRICRAISE, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ALERT, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ADDLEGITPROCESSES, e.User, e.TicketID));
                    break;
                default:
                    break;
            }
        }
        if(e.User==Scenario.Currentuser.id)  //a voir ce qu'on fait..
        {
            switch (e.Action)
            {
                case (int)SystemActions.BEGINATTACK://inutile?
                    //changement action user vers action asset 
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.METRICRAISE, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ALERT, e.User, e.TicketID));
                    break;
                case (int)SystemActions.BEGINFALSEATTACK://inutile??
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.METRICRAISE, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ALERT, e.User, e.TicketID));
                    break;
                case (int)SystemActions.CYBERVIEW:
                    CyberPhysicalViewDelegate(1);
                    //mettre un déclencheur d'actions??
                    break;
                case (int)SystemActions.PHYSICALVIEW:
                    CyberPhysicalViewDelegate(0);
                    break;
                case (int)SystemActions.ALERTVIEW:
                    CyberPhysicalViewDelegate(2);
                    break;
                case (int)SystemActions.UIACTIVATION:
                    UIUpdateDelegate(true);
                    break;
                case (int)SystemActions.UIDEACTIVATION:
                    UIUpdateDelegate(false);
                    break;
                default:
                    break;
            }
        }
    }


    // Update is called once per frame
    void Update ()
    {
	}
}
