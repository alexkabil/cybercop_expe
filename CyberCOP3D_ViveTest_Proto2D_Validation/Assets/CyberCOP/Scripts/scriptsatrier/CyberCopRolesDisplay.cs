﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CyberCopRolesDisplay : MonoBehaviour {
    public Transform GridUsers;
    private AssetInfoList Scenario;
    private Transform _camera;
	// Use this for initialization
	IEnumerator Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
        bool test = true;
        float timer = 0;
        while (test)
        {
            timer += Time.deltaTime;
            if (GameObject.FindGameObjectWithTag("MainCamera") != null ||timer>6.0f)
            {
                test = false;
                yield return null;
                timer = 0;
            }
            yield return null;
        }
        if (GameObject.FindGameObjectWithTag("MainCamera") != null)
        {
            _camera = Camera.main.transform;
            transform.SetParent(_camera);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            Vector2 canvasSize = new Vector2(Camera.main.scaledPixelWidth, Camera.main.scaledPixelHeight);
            transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = canvasSize;
        }
        yield return null;
	}
	public void SetUser(string s, bool b)
    {
        if (GridUsers.Find(s))
        {
            var usr = (from item in Scenario.Users where item.name == s select item).FirstOrDefault();
            GridUsers.Find(s).GetComponent<Image>().enabled = b;
            GridUsers.Find(s).GetChild(0).GetComponent<Text>().text = b?usr.name+"  "+(Scenario.Currentuser.name==usr.name?"(me)":"(other)")+" "+usr.userRole+" "+(usr.isImmersive?"Immersed":"Non Immersed"):"";
        }
    }
	// Update is called once per frame
	void Update ()
    {
		
	}
}
