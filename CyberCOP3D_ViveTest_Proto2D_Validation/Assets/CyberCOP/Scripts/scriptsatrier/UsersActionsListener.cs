﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UsersActionsListener : MonoBehaviour {

    private AssetInfoList Scenario;

    public delegate void OnAssetStateUpdate(CyberCopEventsInformation e);
    public static event OnAssetStateUpdate AssetUpdateDelegate;

    public delegate void OnTicketStateUpdate(CyberCopEventsInformation e);
    public static event OnTicketStateUpdate TicketUpdateDelegate;
    public delegate void OnAlertStateUpdate(CyberCopEventsInformation e);
    public static event OnAlertStateUpdate AlertUpdateDelegate;

    private CyberCopEventsInformation previousUserAction = new CyberCopEventsInformation(999,999,999,999,999);

    // Use this for initialization
    void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }
	
    public void UsersActionsReceived(CyberCopEventsInformation e, bool b)
    {
        if (previousUserAction.Action ==e.Action && previousUserAction.Asset==e.Asset && previousUserAction.TicketID == e.TicketID && previousUserAction.User == e.User && previousUserAction.View == e.View)
        {
            //Debug.Log("meme event user");
            return;
        }
        previousUserAction = e;
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault(); //utile pour passe rparametre ?
        //filtrage par utilisateur d'abord? 0==me filtrage en fonction du scénario??
        if (e.User == Scenario.Currentuser.id)//utilisateur 
        {
                //pour filtrer en fonction du role de l'user ??
                /*
                var item = (from it in Scenario.Users where it.id == e.User select it).FirstOrDefault();
                if(item.userRole == Roles.ANALYST)
                {

                }
                */
            switch (e.Action)
            {
//faire ici les changements d'ui en fonction???
                case (int)RequiredActions.ASK_ANALYST_KINETICINVESTIGATION: //on transmet aussi l'action necessaire?
                    CoordProgress(user, e);
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETCREATION, e.User, e.TicketID));//soit on transmet ask soit on transmet direct l'action à faire..
                    break;
                case (int)RequiredActions.ASK_ANALYST_CYBERINVESTIGATION: //on transmet aussi l'action necessaire?
                    CoordProgress(user, e);
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETCREATION, e.User, e.TicketID));//soit on transmet ask soit on transmet direct l'action à faire..
                    break;
                case (int)RequiredActions.ASK_ANALYST_ANALYSIS: //on transmet aussi l'action necessaire?
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETCREATION, e.User, e.TicketID));//soit on transmet ask soit on transmet direct l'action à faire..
                    break;
                case (int)RequiredActions.TICKETSELECTION: //ici faut transmettre le ticket sur un bouton ou autre..
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETSELECTION, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.TICKETUNSELECTION: //ici faut transmettre le ticket sur un bouton ou autre..
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETUNSELECTION, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.TICKETACCEPTATION: //ici faut transmettre le ticket sur un bouton ou autre..
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETACCEPTATION, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.HIGHLIGHTASSET, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.TICKETCLOSING: //ici faut transmettre le ticket sur un bouton ou autre..
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETCLOSING, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ALERTSELECTION: //bug
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTSELECTION, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.UNHIGHLIGHTASSET, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ALERTUNSELECTION: //bug
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTUNSELECTION, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ASSETSELECTION_ANALYST:// on laisse ca comme ca pour l'instant
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETSELECTION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETUNSELECTION_ANALYST:
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETUNSELECTION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETSELECTION:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    GetComponent<AudioSource>().Play();
                    SelectedAsset(user, e);
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETSELECTION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETUNSELECTION:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    UnSelectedAsset(user, e);
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETUNSELECTION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETINFORMATION:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETINFORMATION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETANALYSIS:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETANALYSIS, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.REPORTSEND_ANALYST:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETREPORT, user.id, user.CurrentTicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.REPORTASSET, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ALERTINCIDENT:
                    CoordProgress(user, e);
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    //on commente pour l'expé...
                    //TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETESCALATION, user.id, user.CurrentTicketID));
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTESCALATION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ALERTDISCARD:
                    CoordProgress(user, e);
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    //on commente pour l'expé...
                    //TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETDISMISS, user.id, user.CurrentTicketID));
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTDISMISS, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.REPORTREAD_COORD:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    //AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.REPORTASSET, user.id, user.CurrentTicketID));//a voir pour recupérer infos asset?
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETREPORTING, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASK_AUTH:
                    AnalystAuthAsk(user, e);
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    //dans un premier temps, on accorde automatiquement? maj etat user?
                    break;
                default:
                    break;
            }
        }
        else
        {
            switch (e.Action)
            {
                case (int)RequiredActions.ALERTSELECTION: //bug
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTSELECTION, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ALERTUNSELECTION: //bug
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTUNSELECTION, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ASK_ANALYST_KINETICINVESTIGATION: //on transmet aussi l'action necessaire?
                    CoordProgress(user, e);
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETCREATION, e.User, e.TicketID));//soit on transmet ask soit on transmet direct l'action à faire..
                    break;
                case (int)RequiredActions.ASK_ANALYST_CYBERINVESTIGATION: //on transmet aussi l'action necessaire?
                    CoordProgress(user, e);
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETCREATION, e.User, e.TicketID));//soit on transmet ask soit on transmet direct l'action à faire..
                    Debug.Log("Reception de la requete coord AUTRE USER");
                    break;
                case (int)RequiredActions.TICKETSELECTION: //ici faut transmettre le ticket sur un bouton ou autre..
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETSELECTION, e.User, e.TicketID));
                    Debug.Log("TicketSelection autre user");
                    break;
                case (int)RequiredActions.TICKETACCEPTATION: //ici faut transmettre le ticket sur un bouton ou autre..
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETACCEPTATION, e.User, e.TicketID));
                    Debug.Log("ticket accept autre user");
                    break;
                case (int)RequiredActions.TICKETCLOSING: //ici faut transmettre le ticket sur un bouton ou autre..
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETCLOSING, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ASSETSELECTION:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    SelectedAsset(user, e);
                    Debug.Log("asset select autre user");
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETSELECTION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETUNSELECTION:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    UnSelectedAsset(user, e);
                    Debug.Log("asset unselect autre user");
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETUNSELECTION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETINFORMATION:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    Debug.Log("assetinfo autre user");
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETINFORMATION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETANALYSIS:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    Debug.Log("assetanalysis autre user");
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETANALYSIS, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.REPORTSEND_ANALYST:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    Debug.Log("reportsend autre user");
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.REPORTASSET, user.id, user.CurrentTicketID));
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETREPORT, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.REPORTREAD_COORD:
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETREPORTING, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ALERTINCIDENT:
                    CoordProgress(user, e);
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    //on commente pour l'expé...
                    //TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETESCALATION, user.id, user.CurrentTicketID));
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTESCALATION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ALERTDISCARD:
                    CoordProgress(user, e);
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    //on commente pour l'expé...
                    //TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETDISMISS, user.id, user.CurrentTicketID));
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTDISMISS, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASK_AUTH:
                    AnalystAuthAsk(user, e);
                    Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset.Add(e);
                    //dans un premier temps, on accorde automatiquement? maj etat user?
                    break;
                default:
                    break;
            }
        }
        ProgressTicket(user, e);
    }

    //checker en fonction des roles...Dans cette branche on va juste ne pas gerer les tickets..
    public void CoordProgress(UserStereotypeSO user, CyberCopEventsInformation e)//ne marche pas pour les tickets incidents?
    {
        CyberCOPAttack attack = Scenario.Attacks[e.TicketID];
        if(attack.assetConcerned==e.Asset)
        {
            if (!attack.RequiredActions.Contains((RequiredActions)e.Action))
            {
                attack.AddEvalAction(user.id, (RequiredActions)e.Action, false);
                return;
            }
            bool act = attack.cyberPhysical == e.View ? true : false;
            attack.AddEvalAction(user.id, (RequiredActions)e.Action, act);//rajouter le highlight?
        }
    }

    public void AnalystAuthAsk(UserStereotypeSO user, CyberCopEventsInformation e)//ne marche pas pour les tickets incidents?
    {
        if (!user.isInvestigatingTicket)
        {
            return;
        }
        CyberCOPAttack attack = Scenario.Attacks[e.TicketID];
        bool action = false;
        if (attack.assetConcerned == e.Asset && e.View ==user.ticketView)//bonasset
        {
            action = Scenario.Assets[e.Asset - 1].requireAuth;
        }
        user.hasAuth = true;
        attack.AddEvalAction(user.id, RequiredActions.ASK_AUTH, action);//rajouter le highlight?
    }

    public void ProgressTicket(UserStereotypeSO user, CyberCopEventsInformation e)
    {
        //ajout de l'action a la liste des actions?? ptet overkill
        user.AddActionUser(e);

        //fonction de progress de ticket ou d'alerte...
        //faire correspondre le ticket et la position des attaques dans le scenario
        //filtrage en fonction des roles, avec un role omnipotent pour test?
        //ajout sur les users de leur ticket courant? avec une structure?
        //if (user.CurrentTicketID != -1)
        //{
            //SystemProgression(user,e);//on va faire plus simple, limite on remet le controle de l'action ici?
            //voir si utiliser la lastaction bug..
        AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTPROGRESSION, e.User, e.Action));//pas besoin de passer l'action, on prend la derniere anyway
        //}
    }

    public void SelectedAsset(UserStereotypeSO user,CyberCopEventsInformation e)
    {
        user.AddAssetSelected(e);
    }
    public void UnSelectedAsset(UserStereotypeSO user, CyberCopEventsInformation e)
    {
        user.RemoveAssetSelected(e);
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
