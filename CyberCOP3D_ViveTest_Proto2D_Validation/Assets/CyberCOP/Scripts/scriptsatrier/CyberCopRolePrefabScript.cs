﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;

public class CyberCopRolePrefabScript : PunBehaviour
{

	// Use this for initialization
	void Start ()
    {
		
	}
    private void Awake()
    {
        string name = gameObject.name.Split('(')[0];
        if (GameObject.Find("CamControlCanvas") != null)
        {
            GameObject.Find("CamControlCanvas").GetComponent<CyberCopRolesDisplay>().SetUser(name, true);
        }

    }
    private void OnDisable()
    {
        /*
        string name = gameObject.name.Split('(')[0];
        if (GameObject.Find("CamControlCanvas") != null)
        {
            GameObject.Find("CamControlCanvas").GetComponent<CyberCopRolesDisplay>().SetUser(name, false);
        }
        */
    }
    // Update is called once per frame
    void Update ()
    {
		
	}
}
