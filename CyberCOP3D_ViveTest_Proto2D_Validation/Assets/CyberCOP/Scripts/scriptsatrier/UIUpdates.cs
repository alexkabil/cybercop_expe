﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;//a voir si on laisse, faudrait ptet mettre maj asset dans assetlistener..
public class UIUpdates : MonoBehaviour {
	private AssetInfoList Scenario;
	
	public Transform UIM2DMAP;
	public Transform UI3D;
    public Text DashboardText;
    public Transform ReportCoord;
	private int currentAsset=-1;
	private int currentTicket = -1;

    public Text AlertText;
    private string _actionsDoneOnAlert="";
    private string _alertCoordText = "";
    public Text AssetText;
    public Transform AlertUIButtonsParent;
    public Transform AssetUIButtonsParent;

    public Transform Ticket3DButtonParent;//pour modif dashboard?
    public Transform Ticket3DContextualText;

    private CyberCOP3DInteractiveObject[] InteractiveObjects;



	private void OnEnable() 
	{
        AlertUpdateEventListener.AlertUIUpdateDelegate += AlertUIModification;
        TicketUpdateEventListener.TicketUIUpdateDelegate += TicketUIModification; //test sur la progression du ticket pour l'alerte...

        //AlertUpdateEventListener.AssetUIUpdateDelegate += AssetUIModification;
		AssetsUpdateEventListener.UIUpdateDelegate+=AssetUIModification;

        AlertUpdateEventListener.MapUIUpdateDelegate += MapAlertUIModification;
        TicketUpdateEventListener.TicketMapUpdateDelegate += MapTicketUIModification;

    }
	private void OnDisable() 
	{
        AlertUpdateEventListener.AlertUIUpdateDelegate -= AlertUIModification;

        //AlertUpdateEventListener.AssetUIUpdateDelegate -= AssetUIModification;
        AssetsUpdateEventListener.UIUpdateDelegate-=AssetUIModification;

        TicketUpdateEventListener.TicketUIUpdateDelegate -= TicketUIModification; //test sur la progression du ticket pour l'alerte...

        AlertUpdateEventListener.MapUIUpdateDelegate -= MapAlertUIModification;
        TicketUpdateEventListener.TicketMapUpdateDelegate -= MapTicketUIModification;
    }
	
	// Use this for initialization
	void Start () 
	{
        InteractiveObjects = FindObjectsOfType<CyberCOP3DInteractiveObject>();
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;

        foreach (Transform tr in AlertUIButtonsParent)
        {
            tr.gameObject.SetActive(false);
        }
        foreach(Transform tr in AssetUIButtonsParent)
        {
            tr.gameObject.SetActive(false);
        }
	}
	//prévoir modif pour tickets...
    public void TicketUIModification(CyberCopEventsInformation e)
    {

        //réception quand envoi report pour l'instant, à prévoir si progress ticket?
        if (!Scenario.Currentuser.isImmersive)
        {
            if (Scenario.Currentuser.CurrentTicketID == e.TicketID) //si j'ai sélectionné cette alerte
            {
                //maj boutons du report envoyé, chgt texte..
                if (e.Action == (int)TicketUpdateActions.TICKETACCEPTATION)
                {
                    //AlertText.text = "Utilisateur " + e.User + " a selectionne le ticket";
                }
            }
            else
            {
                //notification sur ui 2D...
            }
        }
        else
        {
            //fait en amont dans ticketupdateeventlistener
        }

    }

    public void MapTicketUIModification(CyberCopEventsInformation e)
    {
        if (!Scenario.Currentuser.isImmersive)
        {
            if (e.Action == (int)TicketUpdateActions.TICKETCREATION)
            {
                UIM2DMAP.BroadcastMessage("TicketingAsset", new int[2] {e.Asset,e.View });
            }
            else if (e.Action == (int)TicketUpdateActions.TICKETACCEPTATION)
            {
                UIM2DMAP.BroadcastMessage("AnalysisAsset", e.Asset);
            }
            else if (e.Action == (int)TicketUpdateActions.TICKETREPORT)
            {
                UIM2DMAP.BroadcastMessage("ReportAsset", e.Asset);
            }
        }
    }

    public void MapAlertUIModification(CyberCopEventsInformation e)
    {
        if (!Scenario.Currentuser.isImmersive)
        {
            if (e.Action == (int)AlertUpdateActions.ALERTCREATION)
            {
                UIM2DMAP.BroadcastMessage("AssetOnAlert", new int[2] { e.Asset, e.View });
            }
            else if (e.Action == (int)AlertUpdateActions.ALERTSELECTION)
            {
                UIM2DMAP.BroadcastMessage("SelectOneOnMap", e.Asset);
            }
            else if (e.Action == (int)AlertUpdateActions.ALERTUNSELECTION)
            {
                UIM2DMAP.BroadcastMessage("UnselectOneOnMap", e.Asset);
            }
        }
    }

	public void AlertUIModification(CyberCopEventsInformation e)
	{
        if (!Scenario.Currentuser.isImmersive)
        {
            //faire un filtre sur e.user?
            if (e.TicketID == Scenario.Currentuser.CurrentTicketID) //un utilisateur a selectionné ce ticket et ui visible du coup par le current user
            {
                var attack = Scenario.GetAttack(e.TicketID);


                ReportCoord.gameObject.SetActive(false);

                if (e.User == Scenario.Currentuser.id) //si c'est moi qui modifie des trucs
                {
                    //c'est moi qui sélectionne?
                    //Nextaction à fournir aux boutons de l'ui..
                    CyberCopEventsInformation contextualAction = new CyberCopEventsInformation(attack.assetConcerned, attack.cyberPhysical, (int)attack.GetCurrentAction(), e.User, e.TicketID);
                    //appel à l'ui 2D

                    foreach (Transform tr in AlertUIButtonsParent)
                    {
                        tr.gameObject.SetActive(true);
                        if (tr.gameObject.GetComponent<AlertActionsButtons>() != null)
                        {
                            AlertActionsButtons alertbutton = tr.gameObject.GetComponent<AlertActionsButtons>();
                            alertbutton.SetButtonState(contextualAction, attack.ActionsPerformed);
                            if (Scenario.hasAssistance)
                            {
                                alertbutton.HighlightButtonScenario(contextualAction);
                            }

                        }
                        //Appel direct de la fonction, utiliser sendmessage à la place??
                    }
                }
                else //on fait modif ui aussi si qqun d'autre
                {
                    //c'est moi qui sélectionne?
                    //Nextaction à fournir aux boutons de l'ui..
                    CyberCopEventsInformation contextualAction = new CyberCopEventsInformation(attack.assetConcerned, attack.cyberPhysical, (int)attack.GetCurrentAction(), e.User, e.TicketID);
                    //si l'ui est activée, on affiche, sinon, notif!
                    if (Scenario.Currentuser.CurrentTicketID == e.TicketID)
                    {
                        foreach (Transform tr in AlertUIButtonsParent)
                        {
                            if (tr.gameObject.GetComponent<AlertActionsButtons>() != null)
                            {
                                AlertActionsButtons alertbutton = tr.gameObject.GetComponent<AlertActionsButtons>();
                                alertbutton.SetButtonState(contextualAction, attack.ActionsPerformed);
                                if (Scenario.hasAssistance)
                                {
                                    alertbutton.HighlightButtonScenario(contextualAction);
                                }
                            }
                            //Appel direct de la fonction, utiliser sendmessage à la place??
                        }
                    }
                }
                if (attack.ActionsPerformed.Count > 0)
                {
                    string actions = "";
                    string nextaction = "";
                    if(attack.ActionsPerformed.Contains(RequiredActions.ASK_ANALYST_CYBERINVESTIGATION) || attack.ActionsPerformed.Contains(RequiredActions.ASK_ANALYST_KINETICINVESTIGATION))
                    {
                        actions += "Demande d'investigation: done \n";
                        nextaction = "Un analyste doit accepter le ticket \n";
                    }
                    if (attack.ActionsPerformed.Contains(RequiredActions.TICKETACCEPTATION))
                    {
                        actions += "Acceptation Ticket: done \n";
                        nextaction = "Analyse de l'asset necessaire";
                    }
                    if (attack.ActionsPerformed.Contains(RequiredActions.ASSETANALYSIS))
                    {
                        actions += "Asset Analysis: done \n";
                        nextaction = "Un rapport doit etre transmis";
                    }
                    if (attack.ActionsPerformed.Contains(RequiredActions.REPORTSEND_ANALYST))
                    {
                        actions += "Rapport de ticket recu: done \n";
                        nextaction = "Lecture necessaire";
                    }
                    if (attack.ActionsPerformed.Contains(RequiredActions.REPORTREAD_COORD))
                    {
                        actions += "Lecture de rapport \n";
                        nextaction = "Action sur l'alerte nécessaire";
                        ReportCoord.gameObject.SetActive(true);
                        ReportCoord.GetChild(0).GetChild(0).GetComponent<Text>().text = "Rapport alerte " + e.TicketID + " asset " + e.Asset;
                        //string rapport = e.View == 0 ? Scenario.Assets[e.Asset - 1].AllCyberInfo() : Scenario.Assets[e.Asset - 1].AllPhysInfo();
                        ReportCoord.GetChild(1).GetComponent<Text>().text = e.View == 0 ? Scenario.Assets[e.Asset - 1].AllCyberInfo() : Scenario.Assets[e.Asset - 1].AllPhysInfo();
                        ReportCoord.GetChild(2).GetComponent<Text>().text = " Conseil : ";
                        ReportCoord.GetChild(2).GetComponent<Text>().text += Scenario.Attacks[e.TicketID].isFalsePositive ? "Rejetter l'alerte" : "Escalader l'alerte";
                        
                    }
                    if (attack.ActionsPerformed.Contains(RequiredActions.ALERTDISCARD))
                    {
                        actions += "Alerte Rejetee \n";
                        nextaction = "Le ticket peut être clos";
                    }
                    if (attack.ActionsPerformed.Contains(RequiredActions.ALERTINCIDENT))
                    {
                        actions += "Alerte Escaladee \n";
                        nextaction = "Le ticket peut être clos";
                    }
                    if (attack.ActionsPerformed.Contains(RequiredActions.TICKETCLOSING))
                    {
                        actions += "Ticket FERME \n";
                        nextaction = "";
                    }
                    AlertText.text = actions + nextaction;
                    _alertCoordText = AlertText.text;
                }
                

            }
            if (Scenario.Currentuser.CurrentTicketID == -1) //message de deactivation de l'alerte?
            {
                AlertText.text = "";
                ReportCoord.GetChild(1).GetComponent<Text>().text = "";
                //deactivation des boutons?
                ReportCoord.gameObject.SetActive(false);
                foreach(Transform tr in AlertUIButtonsParent)
                {
                    tr.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            //faire un filtre sur e.user?
            if (e.TicketID == Scenario.Currentuser.CurrentTicketID) //un utilisateur a selectionné ce ticket et ui visible du coup par le current user
            {
                var attack = Scenario.GetAttack(e.TicketID);
                if (e.User == Scenario.Currentuser.id) //si c'est moi qui modifie des trucs
                {
                    //c'est moi qui sélectionne?
                    //Nextaction à fournir aux boutons de l'ui..

                    //appel à l'ui 2D
                    
                    string alertState = "Alert ID : " + e.TicketID + "   user: " + e.User + " Asset: " + e.Asset + "  \n";
                    foreach (RequiredActions re in attack.ActionsPerformed)
                    {
                        alertState += " done :" + System.Enum.GetName(typeof(RequiredActions), re) + "\n";
                    }
                    //DashboardText.text = currentAction + alertState;
                }
                if (e.Action == (int)RequiredActions.REPORTSEND_ANALYST)
                {
                    //DashboardText.text = "Rapport envoyé";
                    //maj ui ticket ? (texte qui dit rapport envoyé) maj etat ticket tout court...
                }
            }
            if (Scenario.Currentuser.CurrentTicketID == -1) //message de deactivation de l'alerte?
            {
                //DashboardText.text = "";

            }
        }
	}


//comme pour les alertes, menu contextuel.. mais pas memes informations, ici e contient ce qu'il faut
//revoir, c'est pas ouf..
	public void AssetUIModification(CyberCopEventsInformation e)
	{
        if (!Scenario.Currentuser.isImmersive)
        {
            if(e.User==Scenario.Currentuser.id) //c'est moi qui agit..
            {
                if(e.Action==(int)AssetUpdateActions.ASSETUNSELECTION)
                {
                    if (Scenario.Currentuser.GetSelectedAssets().Count == 0)
                    {
                        AssetUIReset();
                        return;//voir si c'est utile.... 
                    }
                    else
                    {
                        Debug.Log("ya encore des trucs dedans...");
                        UserStereotypeSO.AssetSelected lastAsset = Scenario.Currentuser.LastAssetSelected(); 
                        CyberCopEventsInformation asset = new CyberCopEventsInformation(lastAsset.id,lastAsset.view,e.Action,e.User,e.TicketID);
                        AssetUISelection(asset);
                        return;
                    }
                }
                else
                {
                    AssetUISelection(e);
                    return;
                }
                /*
                if (e.Action == (int)AssetUpdateActions.ASSETSELECTION)
                {
                    AssetUISelection(e);
                }
                else //maj ui parce que c'est l'asset que je regarde??? c'est moi qui fait les modifs ici...
                {
                    if(e.Asset == Scenario.Currentuser.LastAssetSelected().id)
                    {
                        //si ya des modifs a l'ui alors que je regarde l'asset?
                        //modifs ui asset
                    }
                    return;
                }
                */
            }
            else//si c'est pas moi..
            {
               // Debug.Log("action par qqun d'autre...");
            }
        }
        else //en immersif
        {
            RequiredActions actiontodo = (RequiredActions)e.Action;//passer l'action quand même?

            if (e.TicketID != -1)
            {
                CyberCOPAttack attack = Scenario.GetAttack(e.TicketID);
                if (attack.assetConcerned == e.Asset && attack.cyberPhysical == e.View)
                {
                    actiontodo = attack.GetCurrentAction();
                }
            }
            //on passe pas e mais une info qui contient la nextaction (on vire le cas none?)
            CyberCopEventsInformation nextaction = new CyberCopEventsInformation(e.Asset, e.View, (int)actiontodo, e.User, e.TicketID);
            if (Scenario.Currentuser.isImmersive)
            {
                var asset = (from element in InteractiveObjects where element.GetAssetInfoID() == e.Asset && element.view == e.View select element).FirstOrDefault();
                if (asset != null)
                {
                    //asset.UIState(nextaction, actionsAsset);
                }
            }
            
        }
	}

    //public void Asset3DUIModification(CyberCopEventsInformation e, )

    public void AssetUIReset()
    {
        foreach (Transform tr in AssetUIButtonsParent)
        {
            tr.gameObject.SetActive(false);
            //gérer etat des boutons en fonction ticket avancement et etat  asset...
        }
        AssetText.text = "Plus d'asset selectionne";
    }

    public void AssetUISelection(CyberCopEventsInformation e) //maj boutons en fonction de l'état
    {
        RequiredActions nextaction = RequiredActions.NONE;
        var ActionsAssetOnTicket = (from item in Scenario.Assets[e.Asset - 1].State.ActionsDoneOnAsset where item.View == e.View select item).ToArray();
        foreach(CyberCopEventsInformation it in ActionsAssetOnTicket)
        {
            //Debug.Log(it.View+" "+it.Asset+" "+it.TicketID + " "+System.Enum.GetName(typeof(RequiredActions), it.Action));
        }
        if (e.TicketID != -1)
        {
            nextaction = Scenario.GetAttack(e.TicketID).GetCurrentAction();
            //comparaison sur l'asset et la view??
        }
        foreach (Transform tr in AssetUIButtonsParent)
        {

            tr.gameObject.SetActive(true);
            if (tr.GetComponent<AssetActionsButtons>() != null)
            {
                CyberCopEventsInformation ticketstate = new CyberCopEventsInformation(e.Asset, e.View, (int)nextaction, e.User, e.TicketID);
                //vérifier que c'est moi??
                AssetActionsButtons buttonaction = tr.GetComponent<AssetActionsButtons>();
                buttonaction.SetButtonState(ticketstate);
                buttonaction.ActionAlreadyDone(Scenario.Currentuser.id, ActionsAssetOnTicket);
                if (e.TicketID!=-1 && Scenario.GetAttack(e.TicketID).cyberPhysical == e.View && e.Asset==Scenario.GetAttack(e.TicketID).assetConcerned)//généraliser pour la vr aussi??
                {
                    buttonaction.HighlightButtonScenario(ticketstate);
                }
                //a voir si on laisse ca, toujours dispo sauf en fonction de l'état et du ticket??
                //gérer le currentState de l'asset (progression toujours, de normal, alert, investigated, analysed etc...) ou alors currentstate...
            }
            //gérer etat des boutons en fonction ticket avancement et etat  asset...
        }
        AssetStatesInformation assetState = Scenario.Assets[e.Asset - 1].State;
        switch (assetState.currentState(e.View))
        {
            case AssetStatesInformation.assetTypeStates.NORMAL:
                AssetText.text = "Asset dans un état normal \n";
                break;
            case AssetStatesInformation.assetTypeStates.ALERT:
                AssetText.text = "Asset en alerte \n";
                break;
            default:
                break;
        }
        AssetText.text+= "Asset selectionné "+e.Asset +" dans la vue " + e.View + " avec un ticket " + e.TicketID;   
    }

    public void Asset3DSelection(CyberCopEventsInformation e)
    {
        if (e.TicketID != -1)
        {

        }
    }

	// Update is called once per frame
	void Update () 
	{
		
	}
}
