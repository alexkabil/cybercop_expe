﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using System.Linq;

public class DashboardGraphs : MonoBehaviour {
    public GraphChart EntropyGraph;
    public GraphChart NetworkGraph;
    private AssetInfoList Scenario;
    private List<float> _entropiesValues;
    private List<float> _networkValues;
    private float _meanEntropy;
    private float _meanNetwork;
    private float _netindex = 0;
    private float _entropyindex = 0;
    private bool _drawGraph = true;
    // Use this for initialization
    void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
        _entropiesValues = (from item in Scenario.Assets select item.entropy.Value).ToList();
        _networkValues = (from item in Scenario.Assets select item.networkCharge.Value).ToList();
        _meanEntropy = _entropiesValues.Average();
        _meanNetwork = _networkValues.Average();
        if (Scenario.Currentuser.isImmersive)
        {
            //desactivation des graphes pour des questions de perfs
            DrawGraph();
            StartCoroutine(DrawGraphes());
        }
    }

    public void DrawGraph()
    {
        if (EntropyGraph != null)
        {
            //EntropyGraph.DataSource.StartBatch();
            EntropyGraph.DataSource.ClearCategory("Entropies");
            EntropyGraph.DataSource.AddPointToCategoryRealtime("Entropies", _entropyindex, 0);
            //EntropyGraph.DataSource.EndBatch();
        }
        if (NetworkGraph != null)
        {
            //NetworkGraph.DataSource.StartBatch();
            NetworkGraph.DataSource.ClearCategory("Networks");
            NetworkGraph.DataSource.AddPointToCategoryRealtime("Networks", _netindex, 0);
            //NetworkGraph.DataSource.EndBatch();
        }
    }

    public IEnumerator DrawGraphes()
    {
        while (_drawGraph)
        {
            if (EntropyGraph.gameObject.activeInHierarchy)
            {
                _entropiesValues = (from item in Scenario.Assets select item.entropy.Value).ToList();
                _meanEntropy = _entropiesValues.Average();
                _entropyindex++;
                //EntropyGraph.DataSource.StartBatch();
                EntropyGraph.DataSource.AddPointToCategoryRealtime("Entropies", _entropyindex, _meanEntropy);
                //EntropyGraph.DataSource.EndBatch();
                if (_entropyindex > 200)
                {
                    _entropyindex = 0;
                }
            }

            if (NetworkGraph.gameObject.activeInHierarchy)
            {
                _networkValues = (from item in Scenario.Assets select item.networkCharge.Value).ToList();
                _meanNetwork = _networkValues.Average();
                _netindex++;
                //NetworkGraph.DataSource.StartBatch();
                NetworkGraph.DataSource.AddPointToCategoryRealtime("Networks", _netindex, _meanNetwork);
                //NetworkGraph.DataSource.EndBatch();
                if (_netindex > 200)
                {
                    _netindex = 0;
                }
            }
            yield return new WaitForSeconds(1.0f);
        }
        yield return null;
    }

    // Update is called once per frame
    void Update ()
    {
    }
}
