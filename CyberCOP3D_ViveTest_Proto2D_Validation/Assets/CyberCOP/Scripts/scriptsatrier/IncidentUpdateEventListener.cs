﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class IncidentTest
{
    public int id;
    public int progress;
    public bool isSelected;
    public int owner;
    public int type;

    public IncidentTest(int id, int progress, int owner,int type, bool isSelected)
    {
        this.id = id;
        this.progress = progress;
        this.isSelected = isSelected;
        this.owner = owner;
        this.type = type;
    }

    public IncidentTest()
    {
        this.id = -1;
        this.progress = -1;
        this.isSelected = false;
        this.type = -1;
		this.owner = -1;
    }
}

public class IncidentUpdateEventListener : MonoBehaviour {

	private AssetInfoList Scenario;
	private List<IncidentTest> incidentList = new List<IncidentTest>();
	private CyberCopEventsInformation previousIncidentUpdate = new CyberCopEventsInformation(999, 999, 999, 999, 999);

    public GameObject IncidentPrefab;
    public Transform IncidentButtonParent;

    // Use this for initialization
    void Start () 
	{
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }
	
	public void IncidentUpdateState(CyberCopEventsInformation e, bool b)
	{
		previousIncidentUpdate = e;
		switch(e.Action)
		{
			case (int)IncidentUpdateActions.INCIDENTCREATION:
                IncidentCreation(e, false);
			break;
			case (int)IncidentUpdateActions.INCIDENTSELECTION:
			break;
			case (int)IncidentUpdateActions.INCIDENTUNSELECTION:
			break;
			default:
			break;
		}
	}

    public void IncidentCreation(CyberCopEventsInformation e, bool b)
    {
        if (!Scenario.Currentuser.isImmersive)
        {
            var button = GameObject.Instantiate(IncidentPrefab, IncidentButtonParent);
            CyberCopEventsInformation incident2Duser = new CyberCopEventsInformation(e.Asset, e.View, e.Action, Scenario.Currentuser.id, e.TicketID);
            button.GetComponent<ButtonIncidentDelegateAction>().SetButtonInitState(incident2Duser, b);
            button.transform.SetAsFirstSibling();
            IncidentButtonParent.GetComponent<RectTransform>().sizeDelta += new Vector2(0, button.GetComponent<RectTransform>().sizeDelta.y);
        }
    }


	// Update is called once per frame
	void Update () 
	{
		
	}
}
