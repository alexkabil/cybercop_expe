﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ButtonIncidentDelegateAction : MonoBehaviour, IPointerClickHandler
{
    public delegate void OnButtonClickDelegate(CyberCopEventsInformation e);
    public static event OnButtonClickDelegate ButtonDelegate;
    private int butID = 0;
    private int isKinetic = 0;
    private int _action = 0;
    public int ticketID = -1;
    private bool isSelected = false;
    private int _user = 1;// en fonction du current user id? ahouais c'est pour ça qu'on fait ca dans l'event...
                          // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public bool IsSelected()
    {
        isSelected = _action == 1 ? true : false;
        return isSelected;
    }
    public void ResetActionUnselection()
    {
        _action = 0;
    }
    public void Notification(bool b)
    {
        var high = gameObject.GetComponentsInChildren<UnityEngine.UI.Image>(); //le pere est pris en compte donc faut prendre le 2e
        if (high.Length > 1)
        {
            high[1].enabled = b;
        }
        //maj avec des particules?ou petit bidule sur le bouton..
    }

    public void SetButtonInitState(CyberCopEventsInformation e, bool b)
    {
        butID = e.Asset;
        isKinetic = e.View;
        ticketID = e.TicketID;
        _user = e.User;
        if (!b)
        {
            transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "Incident Asset " + butID + " : Escaladed";
            GetComponent<UnityEngine.UI.Button>().interactable = false;
            return;
        }
        string s = isKinetic == 0 ? "incidentNet?" : "incidentEntropy?";
        transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "Incident Asset " + butID + " " + s;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!GetComponent<UnityEngine.UI.Button>().interactable)
        {
            return;
        }
        //mettre un controle sur l'interaction?
        _action++;
        int act = _action % 2 == 0 ? (int)RequiredActions.ALERTUNSELECTION : (int)RequiredActions.ALERTSELECTION;
        ButtonDelegate(new CyberCopEventsInformation(butID, isKinetic, act, _user, ticketID));
    }
}
