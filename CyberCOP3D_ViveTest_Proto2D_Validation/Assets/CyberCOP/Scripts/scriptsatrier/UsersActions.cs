﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UsersActions : MonoBehaviour {
    public CyberCopEventUsers UserEvent;
    private AssetInfoList Scenario;
    public CyberCopPhotonEventSender eventSender;
	// Use this for initialization
	void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }
    private void OnEnable()//l'inconvénient c'est que quand on instancie les boutons en RT ca marche pas... ou alors laisser le delegate et lier les boutons avec
    {
        ScenarioActions.ScenarioEventsDelegate += ScenarioAction;

        CyberCOP3DInteractiveObject.Asset3DDelegate += Asset3DSelection;

        ButtonAssetDelegateAction.ButtonDelegate += AssetButtonSelection;
        ButtonAlertDelegateAction.ButtonDelegate += AlertButtonAction;

        Button3DAssetAction.Button3DDelegate += Asset3DAction;

        AlertActionsButtons.ButtonDelegate += AlertContextualButtonAction;
        AssetActionsButtons.ButtonDelegate+= AssetContextualButtonAction;
        TicketActionsButtons.ButtonDelegate += TicketContextualButtonAction;

        MenuUiPosition.UIHandAction += UIHandAction;

    }
    //juste filtrage en fonction de l'asset selection et de la vue? mise d'accord sur 1=kinetic et 0=cyber?
    public void AssetButtonSelection(CyberCopEventsInformation e)
    {
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault(); //gestion de l'user et du ticket
        //UserEvent.Raise(new CyberCopEventsInformation(e.Asset,e.View,e.Action,user.id,user.CurrentTicketID),true);
        eventSender.SendEvent(new CyberCopEventsInformation(e.Asset, e.View, e.Action, user.id, user.CurrentTicketID));
    }

    public void Asset3DSelection(CyberCopEventsInformation e)
    {
        eventSender.SendEvent(e);
        //UserEvent.Raise(e, true);
    }

    public void Asset3DAction(CyberCopEventsInformation e)
    {
        eventSender.SendEvent(e);
        //UserEvent.Raise(e, true);
    }

    public void AlertButtonAction(CyberCopEventsInformation e)
    {
        eventSender.SendEvent(e);
        //UserEvent.Raise(e, true);
    }

    public void AlertContextualButtonAction(CyberCopEventsInformation e) //envoi action coord à analyste (creation ticket)
    {
        eventSender.SendEvent(e);
        //UserEvent.Raise(e,true);
    }

    public void AssetContextualButtonAction(CyberCopEventsInformation e) //envoi action coord à analyste (creation ticket)
    {
        eventSender.SendEvent(e);
        //UserEvent.Raise(e,true);
        //idéalement mettre ici l'ajout aux actions asset?
    }
    public void TicketContextualButtonAction(CyberCopEventsInformation e) //envoi action coord à analyste (creation ticket)
    {
        eventSender.SendEvent(e);
        //on garde en mémoire e.user qui est l'emetteur du ticket et on transmet scenario.currentuser qui est celui qui a cliqué??
        //UserEvent.Raise(e, true);
    }

    public void ScenarioAction(CyberCopEventsInformation e)
    {
        eventSender.SendEvent(e);
        //UserEvent.Raise(e, true);
    }

    public void UIHandAction(CyberCopEventsInformation e)
    {
        eventSender.SendEvent(e);
    }

    private void OnDisable()
    {
        CyberCOP3DInteractiveObject.Asset3DDelegate -= Asset3DSelection;

        ButtonAssetDelegateAction.ButtonDelegate -= AssetButtonSelection;
        ButtonAlertDelegateAction.ButtonDelegate -= AlertButtonAction;

        AlertActionsButtons.ButtonDelegate -= AlertContextualButtonAction;
        AssetActionsButtons.ButtonDelegate-= AssetContextualButtonAction;
        TicketActionsButtons.ButtonDelegate -= TicketContextualButtonAction;

        Button3DAssetAction.Button3DDelegate -= Asset3DAction;

        ScenarioActions.ScenarioEventsDelegate -= ScenarioAction;

        MenuUiPosition.UIHandAction -= UIHandAction;
    }
    // Update is called once per frame
    void Update ()
    {
        /*
        if (Input.GetKeyDown(KeyCode.Q))
        {
            UserEvent.Raise(new CyberCopEventsInformation(7, 0, 1, 2, 2), true);
            UserEvent.Raise(new CyberCopEventsInformation(7, 0, 6, 2, 2), true);
        }
        */
	}
}
