﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberCOPVRPointer : MonoBehaviour {
    private Transform objectToFollow;
    private string ObjectName = "[VRTK][AUTOGEN][RightControllerScriptAlias][BasePointerRenderer_Origin_Smoothed]";
    public bool render = true;
	// Use this for initialization
	void Start ()
    {
        StartCoroutine(GetVRTKRightPointer());
	}
	public IEnumerator GetVRTKRightPointer()
    {
        float timer = 2.0f;
        yield return new WaitForSeconds(2.0f);
        bool init = true;
        while (init)
        {
            timer += Time.deltaTime;
            if (GameObject.Find(ObjectName) != null)
            {
                init = false;
                objectToFollow = GameObject.Find(ObjectName).transform;
                yield return null;
            }
            if (timer >= 5.0f)
            {
                init = false;
                yield return null;
            }
            yield return null;

        }

        yield return null;
    }

	// Update is called once per frame
	void Update ()
    {
        if (objectToFollow != null && render)
        {
            GetComponent<Renderer>().enabled =objectToFollow.GetChild(0).GetChild(0).gameObject.activeInHierarchy;
            transform.parent.localScale = new Vector3(transform.parent.localScale.x, transform.parent.localScale.y, -objectToFollow.GetChild(0).GetChild(0).localScale.z);
            transform.parent.position = new Vector3(objectToFollow.position.x, objectToFollow.position.y, objectToFollow.position.z);// objectToFollow.position.z- (objectToFollow.GetChild(0).GetChild(0).localScale.z)/2.0f);
            transform.parent.rotation = objectToFollow.rotation;
            //transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -transform.parent.localScale.z);
        }	
	}
}
