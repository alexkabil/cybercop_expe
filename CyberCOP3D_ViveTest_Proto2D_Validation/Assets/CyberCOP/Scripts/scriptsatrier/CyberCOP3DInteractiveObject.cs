﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using UnityEngine.UI;
using System.Linq;

public class CyberCOP3DInteractiveObject : MonoBehaviour {
    public int assetId;
    public int view;//kinetic=1?

    //rajouter graph et scenarioassets...
    private AssetInfoList Scenario;//dommage de rajouter ça mais bon..
    private AssetInformation _assetInfo;
    public GraphChart valueGraph;

    private bool _isGeneratingValue = true;

    private bool _isAssetSelected = false;

    //mettre différentes valeurs pour net et entropie?
    private float lowpart = -10.0f;
    private float highpart = 10.0f;

    public GameObject UIAsset;
    public Button3DAssetAction[] Buttons;
    public int AssetControl = 0;
    private bool activation = false;
    private Transform userHead;
    private Text _assetText;
    private Text _analysisText;
    private Text _assetName;
    private int _user;//user qui active ou deactive. Separer activation et deactivation?
    private int _ticket;
    private bool init = true;
    private bool _isHighlighted = false;
    private ParticleSystem highlighter;
    private List<int> _otherSelection;
    private bool _isSelectedByOther = false;
    private Transform _otherHighlighter;
    private bool _isHighValue = false;
    private bool _alertCyberAct = false;
    private bool _alertITAct = false;
    private int _alertcount = 0;
    private int _alertCyber = 0;
    private int _alertIT = 0;

    public delegate void ON3DAssetClickDelegate(CyberCopEventsInformation e);// peut être à utiliser ici plutot que dans VRTK_CyberCOP_Objets si ça foire...
    public static event ON3DAssetClickDelegate Asset3DDelegate;

    void Start()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
        if (view == 2)//vue alerte
        {
            UIAsset = transform.parent.GetChild(transform.GetSiblingIndex() - 1).gameObject;//ok
            _assetText = UIAsset.transform.GetChild(0).GetChild(1).GetComponent<Text>();
            _assetName = UIAsset.transform.GetChild(0).GetChild(0).GetComponent<Text>();
            _analysisText = UIAsset.transform.GetChild(0).GetChild(2).GetComponent<Text>();
            highlighter =transform.parent.parent.GetChild(transform.parent.GetSiblingIndex() - 1).GetComponent<ParticleSystem>();
            _assetInfo = Scenario.Assets[assetId - 1];//normalement ok..
            _otherSelection = new List<int>();

            if (transform.GetChild(0) != null)
            {
                _otherHighlighter = transform.GetChild(0);
            }
            init = false;
            return;
        }
        UIAsset = transform.parent.GetChild(transform.GetSiblingIndex() - 1).gameObject;//on va essayer ca...
        _assetText = UIAsset.transform.GetChild(1).GetChild(1).GetComponent<Text>();
        _analysisText = UIAsset.transform.GetChild(1).GetChild(5).GetComponent<Text>();
        _assetName = UIAsset.transform.GetChild(1).GetChild(0).GetComponent<Text>();
        highlighter =view==0? transform.parent.parent.GetChild(transform.parent.GetSiblingIndex() + 2).GetComponent<ParticleSystem>(): transform.parent.parent.GetChild(transform.parent.GetSiblingIndex() + 2).GetComponent<ParticleSystem>();
        _assetInfo = Scenario.Assets[assetId - 1];//normalement ok..
        _otherSelection = new List<int>();

        if (transform.GetChild(0) != null)
        {
            _otherHighlighter = transform.GetChild(0);
        }
        DrawGraph();
        StartCoroutine(GenerateValue());
        init = false;

        //ajout à la main??? fuckit
    }
    private void OnDisable()//appellée des qu'on change de vue..
    {
        if (view == 2)//vue alerte
        {
            return;
        }
        //_isGeneratingValue = false;
        StopCoroutine(GenerateValue());
    }
    private void OnEnable()
    {
        if (!init)
        {
            if (view != 2)//de quoi?
            {
                StartCoroutine(GenerateValue());
            }
            if (!activation)
            {
                GetComponent<VRTK_CyberHighlight>().Unhighlight();
            }
            else
            {
                GetComponent<VRTK_CyberHighlight>().Highlight(Color.green);
            }
        }
    }
    public void DrawGraph()
    {
        if (valueGraph != null)
        {
            switch (view)
            {
                case 1:
                    valueGraph.DataSource.StartBatch();
                    valueGraph.DataSource.ClearCategory("Entropy");
                    if (_assetInfo.entropy.prevValues != null)
                    {
                        foreach (var pair in _assetInfo.entropy.prevValues)
                        {
                            valueGraph.DataSource.AddPointToCategory("Entropy", (double)pair.Key, (double)pair.Value);
                        }
                    }
                    valueGraph.DataSource.EndBatch();
                    break;
                case 0:
                    valueGraph.DataSource.StartBatch();
                    valueGraph.DataSource.ClearCategory("Network");
                    if (_assetInfo.networkCharge.prevValues != null)
                    {
                        foreach (var pair in _assetInfo.networkCharge.prevValues)
                        {
                            valueGraph.DataSource.AddPointToCategory("Network", (double)pair.Key, (double)pair.Value);
                        }
                    }
                    valueGraph.DataSource.EndBatch();
                    break;
            }
        }
    }

    public IEnumerator GenerateValue()
    {
        FloatAsset valueChange = _assetInfo.entropy;
        string Category = "";
        switch (view)
        {
            case 0:
                valueChange = _assetInfo.networkCharge;
                Category = "Network";
                break;
            case 1:
                valueChange = _assetInfo.entropy;
                Category = "Entropy";
                break;
            default:
                //cas d'erreur?
                break;
        }
        while (_isGeneratingValue)
        {
            valueChange.SetValue(Random.Range(lowpart, highpart));

            if (_isAssetSelected)
            {
                //valueGraph.DataSource.StartBatch();
                valueGraph.DataSource.AddPointToCategoryRealtime(Category, (double)valueChange.index, (double)valueChange.Value);
                //valueGraph.DataSource.EndBatch();
            }
            yield return new WaitForSeconds(1.0f);
        }
        yield return null;
    }

    public void ActivateAlertButton(int view)//bouton alerte
    {
        UIAsset.transform.GetChild(0).GetChild(3).GetChild(view).gameObject.SetActive(true);
        _alertcount++;
        var colorpart = highlighter.main;
        if (_isHighlighted)//ou if _alertcount==2?
        {
            var emission = highlighter.emission;
            colorpart.startColor = Color.magenta;
            emission.rateOverTime= 1000.0f;
            return;
        }
        
        switch (view)
        {
            case 0://cyber?
                colorpart.startColor = Color.cyan;
                break;
            case 1://it?
                colorpart.startColor = Color.yellow;
                break;
            default:
                break;
        }
        HighlightAsset(true);
    }

    public void AlertSelectionButton(int i)
    {
        int alertid = -1;
        int action = (int)RequiredActions.ALERTSELECTION;
        switch (i)
        {
            case 0://cyber?
                _alertCyberAct = !_alertCyberAct;
                action = _alertCyberAct ? (int)RequiredActions.ALERTSELECTION : (int)RequiredActions.ALERTUNSELECTION;
                break;
            case 1://it?
                _alertITAct = !_alertITAct;
                action = _alertITAct ? (int)RequiredActions.ALERTSELECTION : (int)RequiredActions.ALERTUNSELECTION;
                break;
            default:
                break;
        }
        for(int j = 0; j < Scenario.Attacks.Length; j++)
        {
            if(Scenario.Attacks[j].assetConcerned== GetAssetInfoID() && Scenario.Attacks[j].cyberPhysical== i)
            {
                alertid = j;
                break;
            }
        }
        CyberCopEventsInformation alertSel = new CyberCopEventsInformation(GetAssetInfoID(), i, action, Scenario.Currentuser.id,alertid);
        Asset3DDelegate(alertSel);
    }

    public int GetAssetInfoID()
    {
        return _assetInfo.State.id;
    }

    public void SelectionAlert(bool b, int view)
    {
        switch (view)
        {
            case 0://cyber?
                _alertCyberAct = b;
                break;
            case 1://it?
                _alertITAct = b;
                break;
            default:
                break;
        }
        _analysisText.gameObject.SetActive(b);
        UIAsset.transform.Find("AlertObjPanel").GetChild(3).GetChild(view).gameObject.GetComponent<Image>().color = b ? Color.blue : Color.white;
    }

    public void AlertActionButtonsActivation(bool b)
    {
        foreach (Button but in _analysisText.transform.GetComponentsInChildren<Button>())
        {
            but.interactable = b;
        }
    }

    public void UpdateStateAlertText(int view, string s)
    {
        switch (view)
        {
            case 0://cyber?
                if (_alertCyberAct)
                {
                    AlertStateUpdate(view,s);
                }
                break;
            case 1://it?
                if (_alertITAct)
                {
                    AlertStateUpdate(view,s);
                }
                break;
            default:
                break;
        }
    }

    public void AlertStateUpdate(int view, string s)
    {
        _analysisText.text = "Alerte " +(view==0?"Cyber":"Entropie");
        _analysisText.text += ("\n\n"+s);
    }


    //pour l'expé, action par le bouton, 0=dismiss
    public void ActionAlertDiscardEscalate(int action)//discard ou escalate
    {
        int actiondone = action==0?(int)RequiredActions.ALERTDISCARD: (int)RequiredActions.ALERTINCIDENT;
        CyberCopEventsInformation actionAlert = new CyberCopEventsInformation(GetAssetInfoID(), Scenario.GetAttack(Scenario.Currentuser.CurrentTicketID).cyberPhysical,actiondone,Scenario.Currentuser.id, Scenario.Currentuser.CurrentTicketID);
        Asset3DDelegate(actionAlert);
        //maj couleur highlight ou asset?
        _alertcount--;
        if (_alertcount == 1) //s'il reste une alerte
        {
            var emission = highlighter.emission;
            var colorpart = highlighter.main;
            int viewatk = Scenario.GetAttack(Scenario.Currentuser.CurrentTicketID).cyberPhysical;//alerte reglee, il en reste une à l'opposé
            colorpart.startColor = viewatk==1? Color.cyan:Color.yellow;
            emission.rateOverTime = 500.0f;
        }
        if (_alertcount == 0)
        {
            gameObject.GetComponent<Renderer>().material.color = action == 0 ? Color.green : Color.red;
            HighlightAsset(false);
        }
        
        //afficher bouton cause?
        _analysisText.transform.GetChild(2).gameObject.SetActive(true);
        _analysisText.transform.GetChild(2).GetChild(2).gameObject.SetActive(true);
        _analysisText.transform.GetChild(2).GetChild(action).gameObject.SetActive(true);
    }


    public void OtherUserSelection(CyberCopEventsInformation e, bool b, bool immersive)
    {
        Color actioncolor = b ? Color.green : Color.red;
        if (_otherSelection.Contains(e.User))
        {
            if (!b)
            {
                _otherSelection.Remove(e.User);
                if (_otherSelection.Count == 0)
                {
                    _isSelectedByOther = false;
                    SetHighlightOther(b, immersive);
                }
                else
                {
                    GetComponent<VRTK_CyberHighlight>().Highlight(actioncolor);
                }
            }
        }
        else
        {
            if (b)
            {
                if (_otherSelection.Count == 0)
                {
                    _isSelectedByOther = true;
                    GetComponent<VRTK_CyberHighlight>().Highlight(actioncolor);
                    SetHighlightOther(b, immersive);
                }
                _otherSelection.Add(e.User);
                
            }
        }
        GetComponent<VRTK_CyberHighlight>().Highlight(actioncolor);
    }

    public void SetHighlightOther(bool activation,bool immersion)
    {
        Debug.Log("coloration asset");
        if (immersion)
        {
            GetComponent<Renderer>().material.color = activation ? Color.green : Color.white;
        }
        else
        {
            _otherHighlighter.GetComponent<Renderer>().material.color = activation? new Color(0, 1, 0, 1.0f): new Color(1,1,1,0);
        }
    }

    public void SetUserInteraction(CyberCopEventsInformation e, bool b)
    {
        _user = e.User;
        _ticket = e.TicketID;
        Color actioncolor = b ? Color.green : Color.red;
        GetComponent<VRTK_CyberHighlight>().Highlight(actioncolor);

        _isAssetSelected = b;
        if (GameObject.FindGameObjectWithTag("MainCamera").transform != null && b)
        {
            userHead = Camera.main.transform;//changer pour faire camera user?
            if (view != 2)
            {
                DrawGraph();//tester..
            }
        }
        activation = b;
        AssetControl = b ? 1 : 0;
        UIAsset.SetActive(b);
        //mettre un delegate des qu'on selectionne et deselectionne asset??? et choppé par le bon asset? fuckit
    }

    public void HighlightAsset(bool b)
    {
        if (b)
        {
            highlighter.Play();
            _isHighlighted = true;
            return;
        }
        if (!b)
        {
            highlighter.Stop();
            _isHighlighted = false;
            return;
        }
    }

    //appel dès que maj ui?? //copier ca pour ui 2D
    public void UIState(CyberCopEventsInformation e,RequiredActions actiontodo, List<RequiredActions> actionsdoneuser)
    {
        if (!UIAsset.activeSelf) //a voir si on peut maj quand même?
        {
            return;
        }
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        foreach (Button3DAssetAction but in Buttons)
        {
            but.SetButtonState(e,user.ticketView==view && actiontodo != RequiredActions.NONE, actionsdoneuser);
            if (Scenario.hasAssistance && e.View==view && e.Asset == GetAssetInfoID())
            {
                if (actiontodo != RequiredActions.NONE)
                {
                    but.HighlightButtonScenario(actiontodo);
                }
            }
        }
        SetInfoText(e, actionsdoneuser);
        ActionToDo(actiontodo);//en fonctiond de l'action, faire des trucs cools
    }
    
    public void ActionToDo(RequiredActions action)
    {

    }

    public void SetButtonAction()
    {
        RequiredActions action = activation ? RequiredActions.ASSETUNSELECTION : RequiredActions.ASSETSELECTION;
        CyberCopEventsInformation assetaction = new CyberCopEventsInformation(GetAssetInfoID(), view, (int)action, Scenario.Currentuser.id, Scenario.Currentuser.CurrentTicketID);
        Asset3DDelegate(assetaction);
    }

    public void SetAnalysisText()
    {
        StartCoroutine(AnalysisText());
    }

    public IEnumerator AnalysisText()
    {
        int timer = 0;
        bool init = true;
        _analysisText.text = view == 0 ? "Analyse réseau" : "Analyse entropie";
        while (init)
        {
            timer += 1;
            _analysisText.text += ".";
            if (timer >= 8)
            {
                init = false;
                timer = 0;
                yield return null;
            }
            yield return new WaitForSeconds(0.2f);
        }
        if (!_isHighValue)
        {
            _analysisText.text = view == 0 ? "Rien à signaler concernant le réseau" : "Rien à signaler concernant l'entropie";
        }
        else
        {
            float randVal = Random.Range(0, 50.0f);
            float valAnomaly = view == 0 ? (_alertCyber==1?randVal: randVal+50.0f) : (_alertIT == 1 ? randVal : randVal + 50.0f)/*_alertIT * randVal*/;
            _analysisText.text = view == 0 ? "Probabilité anomalie réseau: " : "Probabilité anomalie entropie: ";
            _analysisText.text += valAnomaly.ToString() + " %";
        }

        yield return null;
    }

    public void SetInfoText(CyberCopEventsInformation e,List<RequiredActions> actionsdoneuser)
    {
        //revoir l'affichage en fonction de ce qui a été fait et de l'action passée..
        //maj pour gérer les alertes??
        if (view != 2)
        {
            _assetText.text = view == 0 ? _assetInfo.InitCyberInfo() : _assetInfo.InitPhysicInfo();
            if (actionsdoneuser.Contains(RequiredActions.ASSETINFORMATION))
            {
                _assetText.text = view == 0 ? _assetInfo.CyberInfo() : _assetInfo.PhysInfo();
                if (actionsdoneuser.Contains(RequiredActions.ASSETANALYSIS))
                {
                    _assetText.text = view == 0 ? _assetInfo.AllCyberInfo() : _assetInfo.AllPhysInfo();//a voir si on laisse...
                }
            }
            return;
        }
        else
        {
            _assetText.text = _assetInfo.InitAlertInfo();
            _assetName.text = _assetInfo.name;
            return;
        }
    }

    public void SetHighValue()
    {
        lowpart = 40.0f;
        highpart = 100.0f;
        _isHighValue = true;
    }
    //true = malicieux
    public void SetProcesses(bool b)//laisser comme ca ou mettre unmalicious et en fonction des vues aussi?
    {
        if (b)
        {
            if (view == 1)
            {
                _assetInfo.processes.Add("WannaCrypt0r.exe");
                _assetInfo.processes.Add("C2Command.exe");
                _alertIT = 2;
                return;
            }
            else
            {
                _assetInfo.processes.Add("Etern4lBlu3");
                _assetInfo.processes.Add("Doubl3Puls4r");
                _alertCyber = 2;
                return;
            }
        }
        else
        {
            if (view == 1)
            {
                _assetInfo.processes.Add("BackupController.exe");
                _assetInfo.processes.Add("FileEncrypter.exe");
                _alertIT = 1;
                return;
            }
            else
            {
                _assetInfo.processes.Add("DataTransfer.exe");
                _assetInfo.processes.Add("Updater.exe");
                _alertCyber = 1;
                return;
            }
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        if (activation && userHead != null)
        {
            UIAsset.transform.LookAt(2 * UIAsset.transform.position - userHead.position);
        }
    }
}
