﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioActions : MonoBehaviour { //faire le management du scenario ici ou dans un autre script??
    //tout doit référencer ce scenario et non pas directement assetinfolist...
    private ScenarioSceneManager SceneManager;
    public CyberCopEventSystem SystemEvents;
    public delegate void LaunchScenarioDelegate(CyberCopEventsInformation e);
    public static event LaunchScenarioDelegate ScenarioEventsDelegate;
    //public UserStereotypeSO currentUser;
    public bool isDesktop = false;
    public Transform SpawnPoint;
    public CyberCopCoordPanelCamera CamPanel;
    public bool isCamPanelNeeded = false;
    public GameObject[] NonImmersiveUIS;
    /*faire delegate pour déclenchement des events ici??*/
    private void OnEnable()
    {
        //Scenario.InitState();
        //Scenario.Currentuser = currentUser;//pour adapter le curentuser..
        SceneManager = GameObject.FindObjectOfType<ScenarioSceneManager>();
        CyberCOPVRTKButton.ButtonPressedDelegate += GetGamepadEvents;
        //SetSpawnPoint();
        //SetSpawnPointEXPE();
    }
    private void OnDisable()
    {
        //Scenario.EndState();
        CyberCOPVRTKButton.ButtonPressedDelegate -= GetGamepadEvents;
    }

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(StartViewScenarioAndStuff());
	}
	public IEnumerator StartViewScenarioAndStuff()
    {
        if (!SceneManager.Scene_Scenario.Currentuser.isImmersive)
        {
            if (isCamPanelNeeded)
            {
                StartCoroutine(CamPanel.UICameraPlace());//changement d'ui, plus besoin
            }
            var assetbuttons = FindObjectsOfType<ButtonAssetDelegateAction>();
            foreach (ButtonAssetDelegateAction button in assetbuttons)
            {
                button.SetUserStatus(SceneManager.Scene_Scenario.Currentuser.id);
            }

        }
        if (isDesktop)
        {
            var foundObjects = FindObjectsOfType<VRTK.VRTK_UICanvas>();
            foreach (VRTK.VRTK_UICanvas canvas in foundObjects)
            {
                canvas.enabled = false;
            }
        }
        var uionAssets = FindObjectsOfType<CyberCOP3DInteractiveObject>();
        foreach (CyberCOP3DInteractiveObject asset in uionAssets)
        {
            asset.UIAsset.SetActive(false);//s'assurer que le lien est fait!!
        }
        yield return new WaitForSeconds(1.0f);
        if (SceneManager.Scene_Scenario.Currentuser.isImmersive)
        {
            GetGamepadEvents(SystemActions.UIDEACTIVATION);//a voir si on généralise pour TOUTES les uis??
        }
        SetCameraUI(SceneManager.Scene_Scenario.Currentuser.userRole);//pour ui
        GetGamepadEvents(SystemActions.ALERTVIEW);
        yield return new WaitForSeconds(1.0f);
        for (int i = 0; i < SceneManager.Scene_Scenario.Attacks.Length; i++)
        {
            LaunchAttack(i);
        }
        //CyberCopStaticLog.InitWriter(SceneManager.Scene_Scenario.Currentuser.Name);//mettre une condition pour le log? on le change d'endroit...

        //ScenarioEventsDelegate(new CyberCopEventsInformation(4, 0, 1, 2, 0));
        yield return new WaitForSeconds(0.5f);

        //ScenarioEventsDelegate(new CyberCopEventsInformation(4, 1, 7, 2, 0));
        yield return new WaitForSeconds(0.5f);

        //ScenarioEventsDelegate(new CyberCopEventsInformation(5, 1, 1, 2, 1));
        yield return new WaitForSeconds(0.5f);

        //ScenarioEventsDelegate(new CyberCopEventsInformation(5, 1, 7, 2, 1));

        yield return null;
    }
    public void GetGamepadEvents(SystemActions action)
    {
        CyberCopEventsInformation actionsystem = new CyberCopEventsInformation(0, 0, (int)action, SceneManager.Scene_Scenario.Currentuser.id, 0);

        if (action==SystemActions.ALERTVIEW || action==SystemActions.PHYSICALVIEW || action == SystemActions.CYBERVIEW)
        {
            CyberCopEventsInformation changeviews = new CyberCopEventsInformation(0, 0, (int)RequiredActions.CHANGEVIEW, SceneManager.Scene_Scenario.Currentuser.id, SceneManager.Scene_Scenario.Currentuser.CurrentTicketID);
            ScenarioEventsDelegate(changeviews);
        }
        SystemEvents.Raise(actionsystem, true);
    }

    public void GetCyberView()
    {
        CyberCopEventsInformation actionsystem = new CyberCopEventsInformation(0, 0, (int)SystemActions.CYBERVIEW, SceneManager.Scene_Scenario.Currentuser.id, 0);

        SystemEvents.Raise(actionsystem, true);
    }
    public void GetITView()
    {
        CyberCopEventsInformation actionsystem = new CyberCopEventsInformation(0, 0, (int)SystemActions.PHYSICALVIEW, SceneManager.Scene_Scenario.Currentuser.id, 0);

        SystemEvents.Raise(actionsystem, true);
    }

    public void LaunchAttack(int i)
    {
        int atk = SceneManager.Scene_Scenario.GetAttack(i).isFalsePositive ? (int)SystemActions.BEGINFALSEATTACK : (int)SystemActions.BEGINATTACK;
        CyberCopEventsInformation cyber = new CyberCopEventsInformation(SceneManager.Scene_Scenario.GetAttack(i).assetConcerned, SceneManager.Scene_Scenario.GetAttack(i).cyberPhysical, atk, 100, i);
        SystemEvents.Raise(cyber, true);
    }

    public void SetCameraUI(Roles role)
    {
        if (!SceneManager.Scene_Scenario.Currentuser.isImmersive)
        {
            switch (role)
            {
                case Roles.ANALYST:
                    NonImmersiveUIS[0].GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
                    NonImmersiveUIS[1].GetComponent<Canvas>().enabled = false;
                    break;
                case Roles.COORDINATOR:
                    NonImmersiveUIS[1].GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
                    NonImmersiveUIS[0].GetComponent<Canvas>().enabled = false;
                    break;
                default:
                    break;
            }
           
        }
    } 

    public void SetSpawnPoint()//changer les uis pour la 2D??
    {
        if (!SceneManager.Scene_Scenario.Currentuser.isImmersive)
        {
            SpawnPoint.position = new Vector3(-11.82f, 0f, -6.27f);
            SpawnPoint.eulerAngles = new Vector3(0, -90.0f, 0);
        }
        else
        {
            SpawnPoint.position = new Vector3(-0.16f, 0.25f, 0.43f);
            SpawnPoint.eulerAngles = new Vector3(0, 0, 0);
        }
    }
    public void SetSpawnPointEXPE()//changer les uis pour la 2D??
    {
        if (!SceneManager.Scene_Scenario.Currentuser.isImmersive)
        {
            SpawnPoint.position = new Vector3(-11.82f, 0f, -6.27f);
            SpawnPoint.eulerAngles = new Vector3(0, -90.0f, 0);
        }
        else
        {
            SpawnPoint.position = new Vector3(-0.16f, 0.25f, 0.43f);
            SpawnPoint.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if(Input.GetKeyDown(KeyCode.O) && isDesktop)
        {
            GetGamepadEvents(SystemActions.PHYSICALVIEW);
        }
        if(Input.GetKeyDown(KeyCode.I) && isDesktop)
        {
            GetGamepadEvents(SystemActions.CYBERVIEW);
        }
        if (Input.GetKeyDown(KeyCode.P) && isDesktop)
        {
            GetGamepadEvents(SystemActions.ALERTVIEW);
        }

        //tester positionnement ui hand
        if (Input.GetKeyDown(KeyCode.K) && isDesktop)
        {
            GetGamepadEvents(SystemActions.UIACTIVATION);
        }
        if (Input.GetKeyDown(KeyCode.L) && isDesktop)
        {
            GetGamepadEvents(SystemActions.UIDEACTIVATION);
        }

        if (Input.GetKeyDown(KeyCode.Keypad1) && isDesktop)
        {
            //ScenarioEventsDelegate(new CyberCopEventsInformation(7, 0, 8, 1, 2));
            ScenarioEventsDelegate(new CyberCopEventsInformation(5, 1, 8, 1, 1));
        }
        if (Input.GetKeyDown(KeyCode.Keypad2) && isDesktop)
        {
            //ScenarioEventsDelegate(new CyberCopEventsInformation(7, 0, 10, 1, 2));
            ScenarioEventsDelegate(new CyberCopEventsInformation(5, 1, 10, 1, 1));
        }
        if (Input.GetKeyDown(KeyCode.Keypad3) && isDesktop)
        {
            //ScenarioEventsDelegate(new CyberCopEventsInformation(7, 0, 15, 1, 2));
            ScenarioEventsDelegate(new CyberCopEventsInformation(5, 1, 15, 1, 1));
        }
        if (Input.GetKeyDown(KeyCode.Keypad4) && isDesktop)
        {
            //ScenarioEventsDelegate(new CyberCopEventsInformation(7, 0, 21, 1, 2));
            ScenarioEventsDelegate(new CyberCopEventsInformation(5, 1, 22, 1, 1));
        }
        if (Input.GetKeyDown(KeyCode.Keypad5) && isDesktop)
        {
            //ScenarioEventsDelegate(new CyberCopEventsInformation(7, 0, 22, 1, 2));
            ScenarioEventsDelegate(new CyberCopEventsInformation(5, 1, 25, 1, 1));
        }
        if (Input.GetKeyDown(KeyCode.Keypad6) && isDesktop)
        {
            //ScenarioEventsDelegate(new CyberCopEventsInformation(7, 0, 25, 1, 2));
            //ScenarioEventsDelegate(new CyberCopEventsInformation(4, 1, 25, 3, 1));
        }
        if (Input.GetKeyDown(KeyCode.Keypad7) && isDesktop)
        {
            //ScenarioEventsDelegate(new CyberCopEventsInformation(7, 0, 26, 2, 2));
        }
        if (Input.GetKeyDown(KeyCode.Keypad8) && isDesktop)
        {
            //ScenarioEventsDelegate(new CyberCopEventsInformation(4, 1, 22, 2, 1));
        }

    }
}
