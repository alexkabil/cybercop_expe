﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class TicketTest
{
    public int id;
    public int progress;
    public bool isSelected;
    public int owner;
    public int type;
    public List<RequiredActions> Actions;


    public RequiredActions CurrentTicketAction()
    {
        if(Actions.Count>0 && progress < Actions.Count)
        {
            return Actions[progress];
        }
        return RequiredActions.NONE;
    }
    public TicketTest(int id, int progress, int owner,int type, bool isSelected)
    {
        this.id = id;
        this.progress = progress;
        this.isSelected = isSelected;
        this.owner = owner; //voir pour gérer l'owner?
        this.type = type;
        Actions = new List<RequiredActions>();
    }

    public TicketTest()
    {
        this.id = -1;
        this.progress = -1;
        this.isSelected = false;
        this.type = -1;
        Actions = new List<RequiredActions>();
        Actions.Add(RequiredActions.NONE);
    }
}
//pourquoi on s'emmerde avec des tickets et tout sachant que pour l'analyste c'est comme si il selectionnait l'alerte et faisait une action...
public class TicketUpdateEventListener : MonoBehaviour {
    private AssetInfoList Scenario;

    private CyberCopEventsInformation previousTicketUpdate = new CyberCopEventsInformation(999, 999, 999, 999, 999);

    private TicketTest currentTicket = new TicketTest();
    private List<TicketTest> TicketList = new List<TicketTest>();

    public delegate void OnTicketUIUpdate(CyberCopEventsInformation e); //test delegate ui
    public static event OnTicketUIUpdate TicketUIUpdateDelegate;

    public delegate void OnTicketMapUpdate(CyberCopEventsInformation e); //test delegate ui
    public static event OnTicketMapUpdate TicketMapUpdateDelegate;

    public GameObject TicketButtonPrefab;
    public Transform TicketButtonParent;
    public Transform TicketContextualText;
    public UnityEngine.UI.Text DashboardText;

    public GameObject Ticket3DButtonPrefab;
    public Transform Ticket3DButtonParent;
    public Transform Ticket3DContextualText;

    private string _ticketstring = "";
    private string _userticketstring = "";
    //juste pour gérer ui... fuck it
    private int currentticket = -2;
    // Use this for initialization
    void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
        TicketContextualText.gameObject.SetActive(false);
	}
	
    public void TicketUpdateState(CyberCopEventsInformation e, bool b)
    {
        //mettre une garde sur le doublon de tickets?
        /*
        if (previousTicketUpdate.Action == e.Action && previousTicketUpdate.Asset == e.Asset && previousTicketUpdate.TicketID == e.TicketID && previousTicketUpdate.User == e.User && previousTicketUpdate.View == e.View)
        {
            Debug.Log("meme event ticket");
            return;
        }
        */
        previousTicketUpdate = e;

        switch (e.Action)
        {
            case (int)TicketUpdateActions.TICKETCREATION:
                TicketCreation(e);
                DashboardTextDisplay(e, "Creation ticket ");
                break;
            case (int)TicketUpdateActions.TICKETSELECTION:
                TicketSelection(e,true);
                break;
            case (int)TicketUpdateActions.TICKETUNSELECTION:
                TicketSelection(e, false);
                break;
            case (int)TicketUpdateActions.TICKETACCEPTATION:
                TicketAcceptation(e);
                TicketNotification(e);
                DashboardTextDisplay(e, "Acceptation ticket ");
                break;
            case (int)TicketUpdateActions.TICKETCLOSING:
                TicketClosing(e);
                TicketNotification(e);
                DashboardTextDisplay(e, "Ticket ferme ");
                break;
            case (int)TicketUpdateActions.TICKETPROGRESSION:
                TicketNotification(e);
                //ajouter ici une fonction pour ecrire sur le tableau?
                break;
            case (int)TicketUpdateActions.TICKETREPORT:
                TicketNotification(e);
                DashboardTextDisplay(e, "Rapport Ticket Envoye ");
                break;
            case (int)TicketUpdateActions.TICKETREPORTING:
                TicketNotification(e);
                DashboardTextDisplay(e, "Rapport Ticket lu ");
                break;
            case (int)TicketUpdateActions.TICKETESCALATION:
                TicketEscalationDismiss(e);
                TicketNotification(e);
                break;
            case (int)TicketUpdateActions.TICKETDISMISS:
                TicketEscalationDismiss(e);
                TicketNotification(e);
                break;
            default:
                break;
        }
        TicketTextUpdate(e);

        TicketMapUpdateDelegate(e);
        TicketUIUpdateDelegate(e);

    }
    public void DashboardTextDisplay(CyberCopEventsInformation e, string s)
    {
        string text = s +e.TicketID+ " par utilisateur" + e.User+"\n";
        DashboardText.text += text;
    }

    //on met la creation d'objet ticket et sa validation de coté???
    public void TicketCreation(CyberCopEventsInformation e)
    {
        
        if (!Scenario.Currentuser.isImmersive) //si on est en 2D...
        {
            //buttonticket creation et maj 
            TicketButtonCreation(e);
        }
        else
        {
            var button = Instantiate(Ticket3DButtonPrefab, Ticket3DButtonParent);
            button.GetComponent<TicketActionsButtons>().SetButtonState(new CyberCopEventsInformation(e.Asset,e.View,e.Action,Scenario.Currentuser.id,e.TicketID));
            button.GetComponent<TicketActionsButtons>().buttonCreator = e.User;
            button.transform.SetAsFirstSibling();
            button.GetComponentInChildren<UnityEngine.UI.Text>().text = "Ticket " + e.TicketID + " on asset " + e.Asset;
            Ticket3DButtonParent.GetComponent<RectTransform>().sizeDelta += new Vector2(0, button.GetComponent<RectTransform>().sizeDelta.y);
        }

        //Debug.Log("objet ticket   cree bouton cree aussi");
    }

    public void TicketButtonCreation(CyberCopEventsInformation e)
    {/*rajouter conditions sur les ui? */
        var button = GameObject.Instantiate(TicketButtonPrefab, TicketButtonParent);
        button.GetComponent<TicketActionsButtons>().SetButtonState(new CyberCopEventsInformation(e.Asset, e.View, e.Action, Scenario.Currentuser.id, e.TicketID));
        button.GetComponent<TicketActionsButtons>().buttonCreator = e.User;
        button.transform.SetAsFirstSibling();
        button.GetComponentInChildren<UnityEngine.UI.Text>().text = "Ticket " + e.TicketID + " on asset " + e.Asset;
        TicketButtonParent.GetComponent<RectTransform>().sizeDelta += new Vector2(0, button.GetComponent<RectTransform>().sizeDelta.y);
    }

    public void TicketAcceptation(CyberCopEventsInformation e)
    {
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();

        user.isInvestigatingTicket = true;
        user.ticketView = e.View;//pour avoir des actions que liées à ça..
        user.CurrentTicketID = e.TicketID;
        var alert = user.isImmersive?(from item in Ticket3DButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault(): (from item in TicketButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
        if (alert != null)
        {
            alert.AcceptTicket();
        }
    }
    //a tester en 2D
    public void TicketSelection(CyberCopEventsInformation e, bool b)
    {
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        
        if (!Scenario.Currentuser.isImmersive)
        {
            //on va copier ce qu'on fait en immersif..
            /*
            var alert = (from item in TicketButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            if (Scenario.Currentuser.id == e.User) //si c'est moi
            {
                if (currentticket != -2 && currentticket != e.TicketID)
                {
                    var prevalert = (from item in TicketButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == currentticket select item).FirstOrDefault();
                    prevalert.GetComponent<TicketActionsButtons>().ChangeActivity();
                    prevalert.GetComponent<TicketActionsButtons>().Highlight();
                }
                currentticket = e.TicketID;

                alert.GetComponent<TicketActionsButtons>().Highlight();

                TicketContextualText.gameObject.SetActive(b);
                if (b)
                {
                    //ajouter un booléen pour l'affichage du texte et tout??
                    TicketContextualText.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "Ticket " + e.TicketID + " selectionné pour asset " + e.Asset + e.View + "\n";
                    if (user.CurrentTicketID == -1) //utilisateur sans ticket, activation des boutons...
                    {
                        TicketContextualText.gameObject.BroadcastMessage("SetButtonState", e);
                        TicketContextualText.GetChild(1).GetComponent<TicketActionsButtons>().ActivateButton(true);

                    }
                    else if (user.CurrentTicketID == e.TicketID)
                    {
                        TicketContextualText.GetChild(1).GetComponent<TicketActionsButtons>().ActivateButton(false);
                    }
                    else
                    {
                        TicketContextualText.GetChild(1).GetComponent<TicketActionsButtons>().ActivateButton(false);


                        //deactivation autres.. comme alertes
                    }
                }
                
            }
            */

            var alert = (from item in TicketButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            if (Scenario.Currentuser.id == e.User) //si c'est moi
            {
                if (currentticket != -2 && currentticket != e.TicketID)
                {
                    var prevalert = (from item in TicketButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == currentticket select item).FirstOrDefault();
                    prevalert.GetComponent<TicketActionsButtons>().ChangeActivity();
                    prevalert.GetComponent<TicketActionsButtons>().Highlight();//unhighlight en fait
                }
                currentticket = e.TicketID;
                //alert toujours egal a qqch quand on selectionne..
                alert.GetComponent<TicketActionsButtons>().Highlight();

                TicketContextualText.gameObject.SetActive(b);
                if (b)
                {
                    TicketContextualText.gameObject.BroadcastMessage("SetButtonState", e);
                    alert.GetComponent<TicketActionsButtons>().Notification(false);
                    TicketContextualText.GetChild(0).GetComponent<UnityEngine.UI.Text>().text += _userticketstring + _ticketstring;
                }
                else
                {
                    TicketContextualText.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "";
                }

            }
        }
        else
        {
            var alert = (from item in Ticket3DButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            if (Scenario.Currentuser.id == e.User) //si c'est moi
            {
                if (currentticket != -2 && currentticket != e.TicketID)
                {
                    var prevalert = (from item in Ticket3DButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == currentticket select item).FirstOrDefault();
                    prevalert.GetComponent<TicketActionsButtons>().ChangeActivity();
                    prevalert.GetComponent<TicketActionsButtons>().Highlight();//unhighlight en fait
                }
                currentticket = e.TicketID;
                //alert toujours egal a qqch quand on selectionne..
                alert.GetComponent<TicketActionsButtons>().Highlight();

                Ticket3DContextualText.gameObject.SetActive(b);
                if (b)
                {
                    Ticket3DContextualText.gameObject.BroadcastMessage("SetButtonState", e);
                    alert.GetComponent<TicketActionsButtons>().Notification(false);
                    Ticket3DContextualText.GetChild(0).GetComponent<UnityEngine.UI.Text>().text += _userticketstring + _ticketstring;
                }
                else
                {
                    Ticket3DContextualText.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "";
                }

            }
        }
    }
    //a tester en 2D
    public void TicketNotification(CyberCopEventsInformation e)
    {//quand le coordinateur lit le rapport, donc pas coord dans e.user?
        if (!Scenario.Currentuser.isImmersive)
        {
            //gerer pour la 2D..
            var alert = (from item in TicketButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            if (alert != null && alert.getActivity()) //ca veut dire que le bouton est actuellement celui choisi
            {
                alert.Notification(true);
                if (alert.GetComponent<AudioSource>() != null)
                {
                    alert.GetComponent<AudioSource>().Play();
                }
            }
        }
        else
        {
            //couper bouton ticket? ou maj ticket texte pour dire fini?
            var alert = (from item in Ticket3DButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            if (alert !=null &&alert.getActivity()) //ca veut dire que le bouton est actuellement celui choisi
            {
                alert.Notification(true);
                if (alert.GetComponent<AudioSource>() != null)
                {
                    alert.GetComponent<AudioSource>().Play();
                }
                

            }
        }
    }

    public void TicketTextUpdate(CyberCopEventsInformation e)
    {
        if (!Scenario.Currentuser.isImmersive)
        {
            //normalement ticket que en 3D..
            //faire pour la 2D pareil, si je suis dessus maj texte, sinon notif, donc préparation quand sélection
            var alert = (from item in TicketButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            var userwithticket = (from item in Scenario.Users where item.CurrentTicketID == e.TicketID && item.isInvestigatingTicket select item).FirstOrDefault();
            if (alert != null && !alert.GetComponent<TicketActionsButtons>().getActivity()) //ca veut dire que le bouton est actuellement celui choisi
            {
                if (alert.isAccepted())
                {
                    TicketContextualText.GetChild(1).GetComponent<TicketActionsButtons>().ActivateButton(false);
                }
                else
                {
                    if (!Scenario.Currentuser.isInvestigatingTicket)
                    {
                        TicketContextualText.GetChild(1).GetComponent<TicketActionsButtons>().ActivateButton(true);
                    }
                }
                if (alert.NeedClose() && Scenario.Currentuser.CurrentTicketID == e.TicketID && Scenario.Currentuser.isInvestigatingTicket)// mettre à jour que pour l'user qui est sur ce ticket?
                {
                    TicketContextualText.GetChild(2).GetComponent<TicketActionsButtons>().ActivateButton(true);
                }
                else
                {
                    TicketContextualText.GetChild(2).GetComponent<TicketActionsButtons>().ActivateButton(false);
                }
                if (Scenario.Currentuser.CurrentTicketID == e.TicketID)//si c'est mon ticket
                {
                    _userticketstring = "Je suis sur ce ticket\n";
                }
                else if (userwithticket != null)
                {
                    _userticketstring = "user " + userwithticket.id + " est sur ce ticket\n";
                }
                else
                {
                    if (!Scenario.Currentuser.isInvestigatingTicket)
                    {
                        _userticketstring = "Ticket non selectionne\n";
                    }
                    else
                    {
                        _userticketstring = "Je suis sur le ticket " + Scenario.Currentuser.CurrentTicketID + "\n";
                    }
                    if (alert.isClosed)
                    {
                        _userticketstring = "Ticket FERME\n"; ;
                    }
                }
                _ticketstring = TicketText(e);
                TicketContextualText.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = _userticketstring + _ticketstring;
            }
            else
            {
                _ticketstring = _userticketstring + TicketText(e);
            }
        }
        else
        {
            var alert = (from item in Ticket3DButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            var userwithticket = (from item in Scenario.Users where item.CurrentTicketID == e.TicketID && item.isInvestigatingTicket select item).FirstOrDefault();
            if (alert!=null &&!alert.GetComponent<TicketActionsButtons>().getActivity()) //ca veut dire que le bouton est actuellement celui choisi
            {
                if (alert.isAccepted())
                {
                    Ticket3DContextualText.GetChild(1).GetComponent<TicketActionsButtons>().ActivateButton(false);
                }
                else
                {
                    if (!Scenario.Currentuser.isInvestigatingTicket)
                    {
                        Ticket3DContextualText.GetChild(1).GetComponent<TicketActionsButtons>().ActivateButton(true);
                    }
                }
                if (alert.NeedClose() && Scenario.Currentuser.CurrentTicketID == e.TicketID && Scenario.Currentuser.isInvestigatingTicket)// mettre à jour que pour l'user qui est sur ce ticket?
                {
                    Ticket3DContextualText.GetChild(2).GetComponent<TicketActionsButtons>().ActivateButton(true);
                }
                else
                {
                    Ticket3DContextualText.GetChild(2).GetComponent<TicketActionsButtons>().ActivateButton(false);
                }
                if (Scenario.Currentuser.CurrentTicketID == e.TicketID)//si c'est mon ticket
                {
                    _userticketstring = "Je suis sur ce ticket\n";
                }
                else if (userwithticket != null)
                {
                    _userticketstring = "user "+userwithticket.id+" est sur ce ticket\n";
                }
                else
                {
                    if(!Scenario.Currentuser.isInvestigatingTicket)
                    {
                        _userticketstring = "Ticket non selectionne\n";
                    }
                    else
                    {
                        _userticketstring = "Je suis sur le ticket "+Scenario.Currentuser.CurrentTicketID +"\n";
                    }
                    if (alert.isClosed)
                    {
                        _userticketstring = "Ticket FERME\n"; ;
                    }
                }
                _ticketstring = TicketText(e);
                Ticket3DContextualText.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = _userticketstring+_ticketstring;
            }
            else
            {
                _ticketstring= _userticketstring+TicketText(e);
            }
        }
    }

    public string TicketText(CyberCopEventsInformation e)
    {
        string s = "";
        if (Scenario.Attacks[e.TicketID].ActionsPerformed.Contains(RequiredActions.ASK_ANALYST_CYBERINVESTIGATION) || Scenario.Attacks[e.TicketID].ActionsPerformed.Contains(RequiredActions.ASK_ANALYST_KINETICINVESTIGATION))
        {
            string view = e.View == 0 ? "Cyber " : "IT "; 
            s += view+"Ticket\n";
        }
        if (Scenario.Attacks[e.TicketID].ActionsPerformed.Contains(RequiredActions.TICKETACCEPTATION))
        {
            s += "Ticket Accepte\n";
        }
        if (Scenario.Attacks[e.TicketID].ActionsPerformed.Contains(RequiredActions.ASSETINFORMATION))
        {
            s += "AssetInvestigué\n";
        }
        if (Scenario.Attacks[e.TicketID].ActionsPerformed.Contains(RequiredActions.REPORTSEND_ANALYST))
        {
            s += "Rapport Envoyé\n";
        }
        if (Scenario.Attacks[e.TicketID].ActionsPerformed.Contains(RequiredActions.REPORTREAD_COORD))
        {
            s += "Rapport étudié, en attente de cloture de ticket\n";
        }
        if (Scenario.Attacks[e.TicketID].ActionsPerformed.Contains(RequiredActions.ALERTINCIDENT))
        {
            s += "Alerte escaladée, vous pouvez fermer le ticket\n";
        }
        if (Scenario.Attacks[e.TicketID].ActionsPerformed.Contains(RequiredActions.ALERTDISCARD))
        {
            s += "Alerte rejettée, vous pouvez fermer le ticket\n";

        }
        if (Scenario.Attacks[e.TicketID].ActionsPerformed.Contains(RequiredActions.TICKETCLOSING))
        {
            s += "Cloture ticket\n"; 
        }
        return s;
    }
    //a tester en 2D
    public void TicketEscalationDismiss(CyberCopEventsInformation e)
    {
        if (!Scenario.Currentuser.isImmersive)
        {
            var alert = (from item in TicketButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            alert.NeedCloseTicket(true);
        }
        else
        {
            var alert = (from item in Ticket3DButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            alert.NeedCloseTicket(true);
            Debug.Log(e.TicketID + "    alerte a fermer");
        }
    }
    //a tester en 2D
    public void TicketClosing(CyberCopEventsInformation e)
    {
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();

        user.isInvestigatingTicket = false;
        user.ticketView = -1;//raz du truc
        user.CurrentTicketID = -1;
        user.TicketsDone();
        if (!Scenario.Currentuser.isImmersive)
        {
            var alert = (from item in TicketButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            alert.NeedCloseTicket(false);
            alert.isClosed = true;
        }
        else
        {
            var alert = (from item in Ticket3DButtonParent.GetComponentsInChildren<TicketActionsButtons>() where item.ticketid == e.TicketID select item).FirstOrDefault();
            alert.NeedCloseTicket(false);
            
            alert.isClosed = true;
        }
        
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
