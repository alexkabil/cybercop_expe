﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
//ui pour tester les interactions avec les users...
public class CyberCopTestUIPanel : MonoBehaviour {
    public Dropdown ActionList;
    public Dropdown AssetList;
    public Dropdown ViewList;
    public Dropdown UserList;
    public Dropdown TicketList;
    public Transform TextsEvents;
    public Button RaiseButton;
    private AssetInfoList Scenario;
    public CyberCopEvent UserEvent;
    private int[] _actionsUI;
    private Text[] _actionsTexts;
	// Use this for initialization
	void Start ()
    {
        var test = System.Enum.GetValues(typeof(RequiredActions));
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
        ActionList.ClearOptions();
        AssetList.ClearOptions();
        ViewList.ClearOptions();
        UserList.ClearOptions();
        TicketList.ClearOptions();
        _actionsUI = new int[5] { 99, 99, 99, 99, 99 };
        _actionsTexts = new Text[5];
        for(int i = 0; i < _actionsTexts.Length; i++)
        {
            _actionsTexts[i] = TextsEvents.GetChild(i + 1).GetComponent<Text>();
        }


        foreach (AssetInformation asset in Scenario.Assets)
        {
            AssetList.options.Add(new Dropdown.OptionData(asset.State.id.ToString()));
        }
        foreach (var item in test)
        {
            ActionList.options.Add(new Dropdown.OptionData(item.ToString()));
        }
        for(int i = 1; i <= Scenario.Users.Length; i++)
        {
            UserList.options.Add(new Dropdown.OptionData("User " + i+" "+Scenario.Users[i-1].isImmersive));
        }

        TicketList.options.Add(new Dropdown.OptionData("Ticket "+"-1"));
        for(int i = 0; i < Scenario.Attacks.Length; i++)
        {
            TicketList.options.Add(new Dropdown.OptionData("Ticket " + i));
        }

        ViewList.options.Add(new Dropdown.OptionData("Cyber"));
        ViewList.options.Add(new Dropdown.OptionData("IT"));

        ActionList.value = 1;
        AssetList.value = 1;
        ViewList.value = 1;
        TicketList.value = 1;
        UserList.value = 1;

        ActionList.onValueChanged.AddListener(ActionValueChanged);
        AssetList.onValueChanged.AddListener(AssetValueChanged);
        ViewList.onValueChanged.AddListener(ViewValueChanged);
        UserList.onValueChanged.AddListener(UserValueChanged);
        TicketList.onValueChanged.AddListener(TicketValueChanged);
        RaiseButton.onClick.AddListener(RaiseButtonAction);

        ActionList.value = 0;
        AssetList.value = 0;
        ViewList.value = 0;
        TicketList.value = 0;
        UserList.value = 0;
    }

    private void ActionValueChanged(int i)
    {
        int action = (int)Enum.Parse(typeof(RequiredActions), ActionList.options[i].text,true);
        _actionsUI[2] = action;
        _actionsTexts[2].text = action.ToString();
    }
    private void AssetValueChanged(int i)
    {
        int asset = int.Parse(AssetList.options[i].text);
        _actionsUI[0] = asset;
        _actionsTexts[0].text = asset.ToString();
    }
    private void ViewValueChanged(int i)
    {
        _actionsUI[1] = i;
        _actionsTexts[1].text = i.ToString();
    }
    private void UserValueChanged(int i)
    {
        int user = int.Parse(UserList.options[i].text.Split(' ')[1]);
        _actionsUI[3] = user;
        _actionsTexts[3].text = user.ToString();
    }
    private void TicketValueChanged(int i)
    {
        int ticket = int.Parse(TicketList.options[i].text.Split(' ')[1]);
        _actionsUI[4] = ticket;
        _actionsTexts[4].text = ticket.ToString();
    }

    private void RaiseButtonAction()
    {
        CyberCopEventsInformation action = new CyberCopEventsInformation(_actionsUI[0], _actionsUI[1], _actionsUI[2], _actionsUI[3], _actionsUI[4]);
        UserEvent.Raise(action, true);
    }
    // Update is called once per frame
    void Update ()
    {
		
	}
}
