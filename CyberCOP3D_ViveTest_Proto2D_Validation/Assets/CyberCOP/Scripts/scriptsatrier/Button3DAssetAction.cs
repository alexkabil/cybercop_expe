﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Button3DAssetAction : MonoBehaviour,IPointerClickHandler {
    public RequiredActions ButtonAction;
    private CyberCopEventsInformation _action;
    public delegate void ON3DButtonClickDelegate(CyberCopEventsInformation e);
    public static event ON3DButtonClickDelegate Button3DDelegate;
    // Use this for initialization
    void Start ()
    {
	}
	

    public void SetButtonLabel()
    {
        string s = "";
        switch (ButtonAction)
        {
            case RequiredActions.ASSETANALYSIS:
                s = "Analyse";
                break;
            case RequiredActions.ASSETINFORMATION:
                s = "Information";
                break;
            case RequiredActions.REPORTSEND_ANALYST:
                s = "Rapport";
                break;
            default:
                s = "Button";
                break;
        }
        transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = s;
    }

    public void SetButtonState(CyberCopEventsInformation e,bool activation, List<RequiredActions> actions)
    {
        SetButtonLabel();
        ActivateActions(activation, actions);//changement pour l'activation des boutons que si on a un ticket
        GetComponent<UnityEngine.UI.Image>().color = Color.white;
        _action = new CyberCopEventsInformation(e.Asset,e.View,(int)ButtonAction,e.User,e.TicketID);
        ActionAlreadyDone(actions);
    }

    public void ActivateActions(bool b, List<RequiredActions> actionsdone)
    {
        if (b == false)
        {
            GetComponent<UnityEngine.UI.Button>().interactable = false;
            return;
        }
        else
        {
            GetComponent<UnityEngine.UI.Button>().interactable = true;
            if (actionsdone.Contains(ButtonAction))
            {
                GetComponent<UnityEngine.UI.Button>().interactable = false;
            }
            if(ButtonAction==RequiredActions.ASSETANALYSIS || ButtonAction== RequiredActions.ASSETINFORMATION)
            {
                if (actionsdone.Contains(RequiredActions.REPORTSEND_ANALYST))
                {
                    GetComponent<UnityEngine.UI.Button>().interactable = false;
                }
            }
            if (ButtonAction == RequiredActions.REPORTSEND_ANALYST)
            {
                if (actionsdone.Contains(RequiredActions.ASSETANALYSIS) || actionsdone.Contains(RequiredActions.ASSETINFORMATION))
                {
                    GetComponent<UnityEngine.UI.Button>().interactable = true;
                }
                else
                {
                    GetComponent<UnityEngine.UI.Button>().interactable = false;
                }
            }
        }
    }

    public void ActionAlreadyDone(List<RequiredActions> actionsdone)
    {
        if (actionsdone.Contains(ButtonAction))
        {
            GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f, 0.4f, 0.8f, 0.8f);
            GetComponent<UnityEngine.UI.Button>().interactable = false;
        }
    }

    public void HighlightButtonScenario(RequiredActions actiontodo)//action pour le scénario
    {
        if (actiontodo ==ButtonAction)//revoir
        {
            GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f, 0.8f, 0.2f, 0.8f);
        }
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!GetComponent<UnityEngine.UI.Button>().IsInteractable())
        {
            return;
        }
        Button3DDelegate(_action);
        //throw new System.NotImplementedException();
    }
}
