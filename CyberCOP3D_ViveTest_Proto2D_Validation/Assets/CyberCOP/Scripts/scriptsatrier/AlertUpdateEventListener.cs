﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AlertTest
{
    public int id;
    public int progress;
    public bool isSelected;
    public int owner;
    public int type;
    public List<RequiredActions> ActionsPerformed;

    public AlertTest(int id, int progress, int owner,int type, bool isSelected)
    {
        this.id = id;
        this.progress = progress;
        this.isSelected = isSelected;
        this.owner = owner;
        this.type = type;
        ActionsPerformed = new List<RequiredActions>();
    }

    public AlertTest()
    {
        this.id = -1;
        this.progress = -1;
        this.isSelected = false;
        this.type = -1;
		this.owner = -1;
        ActionsPerformed = new List<RequiredActions>();
    }

    public bool IsActionAlreadyPerformed(RequiredActions action)
    {
        if (ActionsPerformed.Contains(action))
        {
            return true;
        }
        return false;
    }

    public void AddAction(RequiredActions action)
    {
        if (!ActionsPerformed.Contains(action))
        {
            ActionsPerformed.Add(action);
        }
    }
}

public class AlertUpdateEventListener : MonoBehaviour {

	private AssetInfoList Scenario;
	/*Mettre ça dans un autre script? */
	public GameObject AlertPrefab;
	public Transform AlertButtonParent;
    public Transform DiscardButtonParent;
	public Transform AlertContextualUI;
    public UnityEngine.UI.Text alertText;

    private CyberCopEventsInformation previousAlertUpdate = new CyberCopEventsInformation(999, 999, 999, 999, 999);

    public delegate void OnAlertUIUpdate(CyberCopEventsInformation e); //test delegate ui
    public static event OnAlertUIUpdate AlertUIUpdateDelegate;

    public delegate void OnAssetUIUpdate(CyberCopEventsInformation e); //test delegate ui
    public static event OnAssetUIUpdate AssetUIUpdateDelegate;// rajouter maj pour l'ui asset qd modif alerte?
    public delegate void OnMapUIUpdate(CyberCopEventsInformation e); //test delegate ui
    public static event OnMapUIUpdate MapUIUpdateDelegate;// rajouter maj pour l'ui asset qd modif alerte?

    public delegate void OnTicketUpdate(CyberCopEventsInformation e); //test delegate ui
    public static event OnTicketUpdate TicketUpdateDelegate;// Bof pour les ticket que quand progress alerte..

    public delegate void OnAlertEscalation(CyberCopEventsInformation e); //test delegate ui
    public static event OnAlertEscalation AlertEscalationDelegate;// Bof pour les ticket que quand progress alerte.

    private AlertTest currentUserAlert = new AlertTest();
	private List<AlertTest> alertList = new List<AlertTest>();

    private CyberCOP3DInteractiveObject[] _alert3DObjects;

    private int _alerts = 0;
	// Use this for initialization
	void Start () 
	{
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
        //pour les expés...
        _alert3DObjects =  (from item in FindObjectsOfType<CyberCOP3DInteractiveObject>() where item.view==2 select item).ToArray();
    }
	
	public void AlertUpdateState(CyberCopEventsInformation e, bool b)
	{
		previousAlertUpdate =e;
		//filtrage en fonction de l'action demandée???
		var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
		switch(e.Action)
		{
			case (int)AlertUpdateActions.ALERTCREATION:
			AlertCreation(e);
			break;
			case (int)AlertUpdateActions.ALERTSELECTION:
			AlertSelection(e);
			break;
			case (int)AlertUpdateActions.ALERTUNSELECTION:
			AlertUnSelection(e);
			break;
			case(int)AlertUpdateActions.ALERTPROGRESSION:
			//AlertProgression(user.GetLastAction());//pour les expés
                AlertExpeProgression(user.GetLastAction());
			break;
            case (int)AlertUpdateActions.ALERTESCALATION:
            //AlertEscalation(e);
            AlertExpeCaracterisation(true, e);
            break;
            case (int)AlertUpdateActions.ALERTDISMISS:
           //AlertDismiss(e);
            AlertExpeCaracterisation(false, e);
            break;
            default:
			break;
		}
        MapUIUpdateDelegate(e);
	}

    public void AlertNotification(CyberCopEventsInformation e)
    {//quand le coordinateur lit le rapport, donc pas coord dans e.user?
        if (!Scenario.Currentuser.isImmersive)
        {
            var alertbutton = (from item in AlertButtonParent.GetComponentsInChildren<ButtonAlertDelegateAction>() where item.ticketID == e.TicketID select item).FirstOrDefault();
            if (alertbutton!=null)
            {
                if (!alertbutton.IsSelected())
                {
                    alertbutton.Notification(true);
                }
            }
            //gerer pour la 2D..
        }
        else
        {
        }
    }

    public void AlertCreation(CyberCopEventsInformation e)
	{
		alertList.Add(new AlertTest(e.TicketID,0,e.User ,0,false));//e.user=100
		//creer bouton ici? condition?
		if(!Scenario.Currentuser.isImmersive)
		{
			AlertButtonCreation(e);
		}
        else
        {
            foreach( CyberCOP3DInteractiveObject obj in _alert3DObjects)
            {
                if (obj.GetAssetInfoID() == e.Asset)//pas le bon truc finalement avec les scenarios...
                {
                    obj.ActivateAlertButton(e.View);
                    break;//voir si ça bloque pas
                }
            }
        }
		//Debug.Log("alerte cree");
	}
	public void AlertUnSelection(CyberCopEventsInformation e)
	{
        //raz du ticketid si jamais l'user n'a plus d'alerte selectionnee?
		var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        var alert = (from element in alertList where element.id == e.TicketID select element).FirstOrDefault();
        if (user.CurrentTicketID==e.TicketID)
		{
            if (!user.isInvestigatingTicket)
            {
                user.CurrentTicketID = -1;
                user.ticketView = -2;
            }
		}
        alert.isSelected = false;
        //AlertContextualUIUpdate(e, alert, false);
        alert.ActionsPerformed.Add(RequiredActions.ALERTUNSELECTION);//vérifier ca??

        if (!Scenario.Currentuser.isImmersive)
        {
            AlertButtonSelection(e.TicketID, false);
        }
        else
        {
            CyberCOPAttack alerte = Scenario.GetAttack(e.TicketID);
            Alert3DButtonSelection(alerte.assetConcerned, alerte.cyberPhysical, false);
        }
    }
	public void AlertSelection(CyberCopEventsInformation e)
	{
		var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        var alert = (from element in alertList where element.id == e.TicketID select element).FirstOrDefault();
		if(/* user.CurrentTicketID!=-1 && */user.CurrentTicketID != e.TicketID) //non, ce qui compte c'est la previously selected by user.
        {
           	var prevalert = (from element in alertList where element.id == user.CurrentTicketID select element).FirstOrDefault();
           	if(prevalert!=null)
			{
				prevalert.isSelected = false;
                prevalert.ActionsPerformed.Add(RequiredActions.ALERTUNSELECTION);
                if (!Scenario.Currentuser.isImmersive)
                {
                    AlertButtonSelection(prevalert.id, false);
                }
                else
                {
                    CyberCOPAttack alerte = Scenario.GetAttack(prevalert.id);
                    Alert3DButtonSelection(alerte.assetConcerned, alerte.cyberPhysical, false);
                }

                //AlertContextualUIUpdate(e, alert, false);
                //envoi event alert unselection???
            }
		}
		if(alert!=null)//l'alerte existe
		{
			//filtrage en fonction du role?
			alert.isSelected = true;
            user.ticketView = e.View;
            alert.progress = Scenario.GetAttack(e.TicketID).attackState;//voir si on fait une fonction pour ça
            if (!user.isInvestigatingTicket)
            {
                user.CurrentTicketID = e.TicketID;
            }
			 //ici ticketid veut dire alertid..
			//vérifier alerte précédente pour la déselectionner par ex?
			//Debug.Log("alerte selectionnee" +e.TicketID+" "+e.User);
			if(!Scenario.Currentuser.isImmersive)
			{
				AlertButtonSelection(alert.id,true);
            }
            else
            {
                CyberCOPAttack alerte = Scenario.GetAttack(alert.id);
                Alert3DButtonSelection(alerte.assetConcerned, alerte.cyberPhysical, true);
            }
		}
		if(user.id==Scenario.Currentuser.id) // a definir en fonction des roles pour le declenchement des event
		{
			currentUserAlert = alert;
			//mise à jour différente que si c'était le meme ticket que moi aussi??
		}
		else
		{
			//autre maj graphique sur la sélection et pas de changement de mon ui..
		}
	}

	public void AlertButtonCreation(CyberCopEventsInformation e)
	{/*rajouter conditions sur les ui? */
		var button = GameObject.Instantiate(AlertPrefab, AlertButtonParent);
        CyberCopEventsInformation alert2Duser = new CyberCopEventsInformation(e.Asset, e.View, e.Action, Scenario.Currentuser.id, e.TicketID);
	    button.GetComponent<ButtonAlertDelegateAction>().SetButtonInitState(alert2Duser,true);	
	    button.transform.SetAsFirstSibling();
	    AlertButtonParent.GetComponent<RectTransform>().sizeDelta+= new Vector2(0,button.GetComponent<RectTransform>().sizeDelta.y);
	}

    public void Alert3DButtonSelection(int asset, int view, bool b)
    {
        if (_alert3DObjects != null)
        {
            CyberCOP3DInteractiveObject obj = (from item in _alert3DObjects where item.GetAssetInfoID() == asset select item).FirstOrDefault();
            obj.SelectionAlert(b, view);
        }
    }

	public void AlertButtonSelection(int ticketid,bool b)
	{
		var alert =(from item in AlertButtonParent.GetComponentsInChildren<ButtonAlertDelegateAction>() where item.ticketID==ticketid select item).FirstOrDefault();
		if(alert!=null)
		{
			alert.GetComponent<ButtonHighlighter>().Highlight(b);
            if (b)
            {
                alert.GetComponent<ButtonHighlighter>().OtherHighlight(false);//a voir ce que ca donne.. pourquoi laisser ça?
                alert.Notification(false);
                //alert.transform.GetChild(1).GetComponent<UnityEngine.UI.Image>().enabled = false;//desactiver les notifications quand on sélectionne..
            }
            if (!b)
			{
				alert.GetComponent<ButtonAlertDelegateAction>().ResetActionUnselection();
			}
		}
	}
    /*dans un premier temps on teste ici puis on délèguera...*/
	public void AlertContextualUIUpdate(CyberCopEventsInformation e, AlertTest alert, bool b)
    {
        AlertUIUpdateDelegate(e);
    }


    //pour les expés
    public void AlertExpeProgression(CyberCopEventsInformation e)
    {
        if (e.TicketID == -1)
        {
            return;
        }
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        var attack = Scenario.GetAttack(e.TicketID);
        string actions = "";
        bool buttonAction = false;
        if(user == Scenario.Currentuser && e.View==attack.cyberPhysical && e.Asset == attack.assetConcerned)
        {
            if (/*!attack.ActionsPerformed.Contains((RequiredActions)e.Action) && */e.Asset == attack.assetConcerned && e.View == attack.cyberPhysical)//actions sur le bon asset
            {
                attack.AddAction((RequiredActions)e.Action);
            }
            if (attack.ActionsPerformed.Contains((RequiredActions.ASSETSELECTION)))
            {
                actions += "Asset Selectionné \n\n";
            }
            if (attack.ActionsPerformed.Contains((RequiredActions.ASSETINFORMATION)))
            {
                actions += "Informations \n\n";
            }
            if (attack.ActionsPerformed.Contains((RequiredActions.ASSETANALYSIS)))
            {
                actions += "Analyse \n\n";
            }
            if (attack.ActionsPerformed.Contains((RequiredActions.ASSETINFORMATION)) && (!attack.ActionsPerformed.Contains((RequiredActions.ALERTDISCARD))|| !attack.ActionsPerformed.Contains((RequiredActions.ALERTINCIDENT))))
            {
                buttonAction = true;
            }
            if (attack.ActionsPerformed.Contains((RequiredActions.ALERTDISCARD)))
            {
                actions += "Alerte Ecartée";
                buttonAction = false;
            }
            if (attack.ActionsPerformed.Contains((RequiredActions.ALERTINCIDENT)))
            {
                actions += "Alerte Escaladee";
                buttonAction = false;
            }

            CyberCOP3DInteractiveObject assetConcerned = (from item in _alert3DObjects where item.GetAssetInfoID() == e.Asset select item).FirstOrDefault();
            if (assetConcerned != null)
            {
                assetConcerned.UpdateStateAlertText(e.View, actions);
                assetConcerned.AlertActionButtonsActivation(buttonAction);
            }
        }
    }

//prendre en compte le fait que l'action a déjà été effectuée??
	public void AlertProgression(CyberCopEventsInformation e)
	{
        if (e.TicketID == -1)
        {
            return;
        }
		var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        var attack = Scenario.GetAttack(e.TicketID);
        //Debug.Log("event recu" + e.TicketID + " " + e.User+" "+e.Action);
        if (!Scenario.GetAttack(e.TicketID).ActionsPerformed.Contains((RequiredActions)e.Action))
        {
            Scenario.GetAttack(e.TicketID).AddAction((RequiredActions)e.Action);
        }
        //Debug.Log("event apres stockage " + e.TicketID + " " + attack.GetCurrentAction() + " " + e.User + " " + e.Action);
        //envoi à chaque action meme pas dans scenario???
        if (attack.GetCurrentAction()==RequiredActions.ASK_ANALYST_CYBERINVESTIGATION || attack.GetCurrentAction() == RequiredActions.ASK_ANALYST_KINETICINVESTIGATION)
        {
            if (attack.ActionsPerformed.Contains(RequiredActions.ASK_ANALYST_CYBERINVESTIGATION) || attack.ActionsPerformed.Contains(RequiredActions.ASK_ANALYST_KINETICINVESTIGATION))
            {
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide 
                AlertUIUpdateDelegate(e);
                AlertNotification(e);
                TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETPROGRESSION, e.User, e.TicketID));//a voir si on laisse ici...
                return;
            }
        }
        if (attack.GetCurrentAction() == RequiredActions.TICKETACCEPTATION)
        {
            if (attack.ActionsPerformed.Contains(RequiredActions.TICKETACCEPTATION) && e.Asset==attack.assetConcerned)
            {
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide 
                AlertUIUpdateDelegate(e);
                AlertNotification(e);
                TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETPROGRESSION, e.User, e.TicketID));//a voir si on laisse ici...
                return;
            }
        }
        if (attack.GetCurrentAction() == RequiredActions.ASSETINFORMATION)
        {
            /*
            if (attack.ActionsPerformed.Contains(RequiredActions.ASSETANALYSIS) && e.Asset == attack.assetConcerned)//pourquoi controle asset?
            {
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide
                AlertUIUpdateDelegate(e);
                TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETPROGRESSION, e.User, e.TicketID));//a voir si on laisse ici...
                return;
            }
            */
            if (attack.ActionsPerformed.Contains(RequiredActions.REPORTSEND_ANALYST) && e.Asset == attack.assetConcerned)//pourquoi controle asset?
            {
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide 
                AlertUIUpdateDelegate(e);
                AlertNotification(e);
                TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETPROGRESSION, e.User, e.TicketID));//a voir si on laisse ici...
                return;
            }
        }
        if (attack.GetCurrentAction() == RequiredActions.ASSETANALYSIS)
        {

            if (attack.ActionsPerformed.Contains(RequiredActions.REPORTSEND_ANALYST) && e.Asset == attack.assetConcerned)//pourquoi controle asset?
            {
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide 
                AlertUIUpdateDelegate(e);
                AlertNotification(e);
                TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETPROGRESSION, e.User, e.TicketID));//a voir si on laisse ici...

                return;
            }
        }
        if (attack.GetCurrentAction() == RequiredActions.ALERTINCIDENT || attack.GetCurrentAction() == RequiredActions.ALERTDISCARD)
        {

            if (attack.ActionsPerformed.Contains(RequiredActions.ALERTINCIDENT) || attack.ActionsPerformed.Contains(RequiredActions.ALERTDISCARD))//pourquoi controle asset?
            {
                Scenario.GetAttack(e.TicketID).NextScenarioAction();//on passe à la suite si on valide
                AlertUIUpdateDelegate(e);
                AlertNotification(e);
                TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETPROGRESSION, e.User, e.TicketID));//a voir si on laisse ici...

                return;
            }
        }

        if (attack.GetCurrentAction()==(RequiredActions)e.Action && e.Asset==attack.assetConcerned && e.View==/*attack.cyberPhysical*/user.ticketView)//changement pour dire que le user fait progresser sur son ticket..
		{
            //on teste d'ajouter les actions tout le temps?
            Scenario.GetAttack(e.TicketID).NextScenarioAction();
			//Tester en sélection d'asset et tout..
        	//AlertUIUpdateDelegate(e);
			//AssetUIUpdateDelegate(e);//utile??
            AlertNotification(e);
            TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset,e.View,(int)TicketUpdateActions.TICKETPROGRESSION,e.User,e.TicketID));//a voir si on laisse ici...
            //rajouter creation notif si on regarde po
            var alert = (from item in AlertButtonParent.GetComponentsInChildren<ButtonAlertDelegateAction>() where item.ticketID == e.TicketID select item).FirstOrDefault();
            if(alert!=null && !alert.IsSelected())
            {
                alert.GetComponent<ButtonHighlighter>().OtherHighlight(true);
            }
        }
        AlertUIUpdateDelegate(e);
    }

    public void AlertEscalation(CyberCopEventsInformation e)
    {
        AlertEscalationDelegate(e);
    }
    public void AlertDismiss(CyberCopEventsInformation e)
    {
        if (!Scenario.Currentuser.isImmersive)
        {
            var button = GameObject.Instantiate(AlertPrefab, DiscardButtonParent);
            CyberCopEventsInformation discarded = new CyberCopEventsInformation(e.Asset, e.View, e.Action, Scenario.Currentuser.id, e.TicketID);
            button.GetComponent<ButtonAlertDelegateAction>().SetButtonInitState(discarded, false);
            button.transform.SetAsFirstSibling();
            DiscardButtonParent.GetComponent<RectTransform>().sizeDelta += new Vector2(0, button.GetComponent<RectTransform>().sizeDelta.y);
        }
    }

    public void AlertExpeCaracterisation(bool b, CyberCopEventsInformation e)//appeller cette fonction apres dismiss ou escalate..
    {
        if (Scenario.Currentuser.isImmersive)
        {
            if (e.TicketID == -1)
            {
                return;
            }
            var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
            var attack = Scenario.GetAttack(e.TicketID);
            //string actions = "";
            //bool buttonAction = false;
            if (user == Scenario.Currentuser && e.View == attack.cyberPhysical && e.Asset == attack.assetConcerned)
            {
                CyberCOP3DInteractiveObject assetConcerned = (from item in _alert3DObjects where item.GetAssetInfoID() == e.Asset select item).FirstOrDefault();
                if (assetConcerned != null)
                {
                    //maj du nombre d'alertes???
                    _alerts++;
                    if (_alerts == Scenario.Attacks.Length)
                    {
                        Debug.Log("tout est traité");
                        if (GameObject.Find("EndExpeText") != null)
                        {
                            GameObject.Find("EndExpeText").transform.GetChild(0).gameObject.SetActive(true);
                        }
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update () 
	{
		
	}
}
