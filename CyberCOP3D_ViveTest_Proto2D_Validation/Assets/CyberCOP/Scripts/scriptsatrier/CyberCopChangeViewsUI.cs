﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CyberCopChangeViewsUI : MonoBehaviour {
    public Text ViewText;


    private void OnEnable()
    {
        ScenarioActionsListener.CyberPhysicalViewDelegate += DisplayViewsFromDelegate;
    }
    private void OnDisable()
    {
        ScenarioActionsListener.CyberPhysicalViewDelegate -= DisplayViewsFromDelegate;
    }

    // Use this for initialization
    public IEnumerator Start ()
    {
        bool init = true;
        float timer = 0;
        while (init)
        {
            timer += Time.deltaTime;
            if(GameObject.FindGameObjectWithTag("MainCamera")!=null)
            {
                transform.parent = GameObject.FindGameObjectWithTag("MainCamera").transform;
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.identity;
                init = false;
                yield return null;
            }
            if (timer > 5.0f)
            {
                Debug.Log("camera non trouvee");
                init = false;
                yield return null;
            }

            yield return null;
        }
        yield return null;
	}
	

    public void DisplayViewsFromDelegate(int i)
    {
        StartCoroutine(DisplayTextViews(i));
    }

    public IEnumerator DisplayTextViews(int i)//pas de variation d'alpha dans un premier temps
    {
        transform.GetChild(0).gameObject.SetActive(true);
        switch (i)
        {
            case 1:
                ViewText.text = "Vue Cyber";
                break;
            case 0:
                ViewText.text = "Vue IT";
                break;
            case 2:
                ViewText.text = "Vue Alerte";
                break;
            default:
                ViewText.text = "";
                break;
        }
        yield return new WaitForSeconds(1.0f);
        transform.GetChild(0).gameObject.SetActive(false);
        yield return null;
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
