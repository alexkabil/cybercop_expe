﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
//boutons quand on sélectionne une alerte sur la vue coord.
public class AssetActionsButtons : MonoBehaviour, IPointerClickHandler {

    public RequiredActions ButtonAction;
    public int view=0;
    private CyberCopEventsInformation contextualEvent;
    private bool _isActive = true;

    public delegate void OnButtonClickDelegate(CyberCopEventsInformation e);
    public static event OnButtonClickDelegate ButtonDelegate;

    // Use this for initialization
    void Start ()
    {
		
	}
    public void SetButtonLabel()
    {
        string s = "";
        switch (ButtonAction)
        {
            case RequiredActions.ASSETANALYSIS:
                s = "Analyse";
                break;
            case RequiredActions.ASSETINFORMATION:
                s = "Information";
                break;
            case RequiredActions.REPORTSEND_ANALYST:
                s = "Rapport";
                break;
            default:
                s = "Button";
                break;
        }
        transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = s;
    }
    public void SetButtonState(CyberCopEventsInformation e)
    {
        contextualEvent = e;
        SetButtonLabel();
        //pour les assets, les boutons envoient la meme chose, on check juste si ya tiket ou pas...

        GetComponent<UnityEngine.UI.Button>().interactable = e.View == contextualEvent.View && e.TicketID!=-1? true:false;//tester si ca marche..
        GetComponent<UnityEngine.UI.Image>().color = Color.white;
        
    }

    public void ActivateActions(bool b, List<RequiredActions> actionsdone)
    {
        if (b == false)
        {
            GetComponent<UnityEngine.UI.Button>().interactable = false;
            return;
        }
        else
        {
            GetComponent<UnityEngine.UI.Button>().interactable = true;
            if (actionsdone.Contains(ButtonAction))
            {
                GetComponent<UnityEngine.UI.Button>().interactable = false;
            }
            if (ButtonAction == RequiredActions.ASSETANALYSIS || ButtonAction == RequiredActions.ASSETINFORMATION)
            {
                if (actionsdone.Contains(RequiredActions.REPORTSEND_ANALYST))
                {
                    GetComponent<UnityEngine.UI.Button>().interactable = false;
                }
            }
            if (ButtonAction == RequiredActions.REPORTSEND_ANALYST)
            {
                if (actionsdone.Contains(RequiredActions.ASSETANALYSIS) || actionsdone.Contains(RequiredActions.ASSETINFORMATION))
                {
                    GetComponent<UnityEngine.UI.Button>().interactable = true;
                }
                else
                {
                    GetComponent<UnityEngine.UI.Button>().interactable = false;
                }
            }
        }
    }

    public void ActionAlreadyDone(List<RequiredActions> actions)
    {
        foreach(RequiredActions re in actions)
        {
            if (re == ButtonAction)//action deja faite par moi
            {
                GetComponent<UnityEngine.UI.Image>().color = new Color(0.5f, 0.2f, 0.9f, 0.5f);
                GetComponent<UnityEngine.UI.Button>().interactable = false;
                return;
            }
        }
    }

    public void ActionAlreadyDone(int user, CyberCopEventsInformation[] actionsdone)
    {
        foreach(CyberCopEventsInformation item in actionsdone)
        {
            //Debug.Log(System.Enum.GetName(typeof(RequiredActions),ButtonAction)+"  "+ System.Enum.GetName(typeof(RequiredActions), item.Action));
            if (item.Action == (int)ButtonAction && item.View == contextualEvent.View)
            {
                //Debug.Log("action deja faite..");
                GetComponent<UnityEngine.UI.Image>().color = user==item.User ? new Color(0.5f, 0.2f, 0.9f, 0.5f) : new Color(0.6f, 0.2f, 0.4f, 0.5f);
                return;
            }
        }
    }

	// Update is called once per frame
	void Update ()
    {
		
	}

    public void HighlightButtonScenario(CyberCopEventsInformation e)
    {
        if (e.Action == (int)ButtonAction && e.View==view) 
        {
            GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f, 0.8f, 0.2f, 0.8f);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(!GetComponent<UnityEngine.UI.Button>().IsInteractable())
        {
            return;
        }
        //GetComponent<UnityEngine.UI.Image>().color = Color.blue;
        _isActive = !_isActive;//garder en mémoire que l'action a été faite?? faire le lien avec la liste des actions??
        CyberCopEventsInformation click = new CyberCopEventsInformation(contextualEvent.Asset, contextualEvent.View, (int)ButtonAction, contextualEvent.User, contextualEvent.TicketID);
        ButtonDelegate(click); //on ne peut appuyer que si c'est la nextaction? ou alors on passe toujours le buttonAction?
        //throw new System.NotImplementedException();
    }
}
