﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CyberCopLogEvents : MonoBehaviour {
    private AssetInfoList Scenario;
    private StreamWriter _logWriter;
    private string _userName;
    // Use this for initialization
    void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
        _userName = Scenario.Currentuser.name+"_"+System.DateTime.Now.Hour+"_"+ System.DateTime.Now.Minute+"_";
        _logWriter = new StreamWriter(Path.Combine(Application.persistentDataPath, _userName + "log.csv"),true);
        Debug.Log(Path.Combine(Application.persistentDataPath, _userName + "log.csv"));
        WriteLog(System.DateTime.Now.ToString() + ";" + "Init" + ";" + ";" + ";");
	}
	
    public void WriteLog(string s)
    {
        _logWriter.WriteLine(s);
        _logWriter.Flush();
    }
    private void OnDisable()
    {
        _logWriter.Close();
    }
    public void LogUserEvents(CyberCopEventsInformation e, bool b)
    {
        if (!b)
        {
            return;
        }
        string logUser = Time.time.ToString()+";";
        if (Scenario.Currentuser.id == e.User)
        {
            logUser += "Me;"+e.Asset+";" + e.View + ";" + System.Enum.GetName(typeof(RequiredActions),e.Action)+ ";" + e.User + ";" + e.TicketID;
        }
        else
        {
            logUser += "other;" + e.Asset + ";" + e.View + ";" + System.Enum.GetName(typeof(RequiredActions), e.Action) + ";" + e.User + ";" + e.TicketID;
        }
        WriteLog(logUser);
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
