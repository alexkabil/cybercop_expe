﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncidentButtonOk : MonoBehaviour
{
    private AssetInfoList Scenario;
    public Transform Assets;
    public Transform AssetsOnMap;

    public GameObject IncidentButton;
    public UnityEngine.UI.Text CoordText;

    public IncidentButtonCoordEvent IncidentButtonCoordEvent;

    public RequiredActions ScenarioAction = RequiredActions.ALERTINCIDENT;
    // Use this for initialization
	void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }
    public void SetNextStepScenario()
    {
        if (Scenario.GetCurrentAttack().GetCurrentAction() == ScenarioAction)
        {
            Scenario.GetCurrentAttack().NextScenarioAction();
            Debug.Log("Etat de la simu: " + Scenario.GetCurrentAttack().GetCurrentAction());
        }
    }
    public void SetIncidentAssetState(int i, int j, int k, int l)
    {
        if(Scenario.Assets[i-1].State.state== AssetStatesInformation.assetTypeStates.ALERT)
        {
            //Scenario.Assets[i - 1].State.isIncidentAnalyst = true;
        }
    }

    public void SendIncidentState(int i, int j, int k, int l)
    {
        IncidentButtonCoordEvent.IncidentScenarioState(i, j, k, l);
    }

    public void SetIncidentUIInformation()
    {
        CoordText.text = "Analyste a constaté l'alerte et souhaite la qualifier en incident.";
        IncidentButton.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "Incident";
        if (Scenario.hasAssistance)
        {
            IncidentButton.GetComponent<UnityEngine.UI.Image>().color = Color.green;
        }
    }

    public void SetMapInformation(int i, int j, int k, int l)
    {
        AssetsOnMap.GetChild(i - 1).GetChild(0).GetComponent<UnityEngine.UI.Image>().color = Color.red;
    }

    public void ChangeHighlightIncidentAssetButton(int i, int j, int k, int l)
    {
        if (Scenario.hasAssistance)
        {
            if (k == 0)
            {
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightPhysIncidentButton(false);
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().SetInteractableStateIncidentButton(true);
            }
            if (k == 1)
            {
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightCyberIncidentButton(false);
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().SetInteractableStateCyberIncidentButton(true);
            }
        }
        
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
