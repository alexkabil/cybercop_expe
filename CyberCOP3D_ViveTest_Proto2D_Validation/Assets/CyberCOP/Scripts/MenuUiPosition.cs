﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuUiPosition : MonoBehaviour {
    public GameObject CanvasUI;
    private AssetInfoList Scenario;
    //infos en public pour test
    public Text userText1;
    public Text userText2;
    public Button reportButton;
    public Button closeButton;
    public Button tutoButton;
    public Transform mapAssets;
    public bool isTuto = true;

    public delegate void OnUIHandAction(CyberCopEventsInformation e); //test delegate ui
    public static event OnUIHandAction UIHandAction;

    // Use this for initialization
    void Start ()
    {
        Scenario = FindObjectOfType<ScenarioSceneManager>().Scene_Scenario;
    }
    private void OnEnable()
    {
        ScenarioActionsListener.UIUpdateDelegate += SetUIPosition;
    }
    private void OnDisable()
    {
        ScenarioActionsListener.UIUpdateDelegate -= SetUIPosition;
    }
    // Update is called once per frame
    void Update ()
    {
		
	}

    public void SetUIInitTuto()
    {
        userText1.text = "";
        userText2.text = "";
        reportButton.gameObject.SetActive(false);
        closeButton.gameObject.SetActive(false);
        tutoButton.gameObject.SetActive(false);
    }

    public void SetUITuto()
    {
        userText1.text = "Sur cette interface, des informations contextuelles peuvent apparaître";
        userText2.text = "Cliquez sur suivant pour continuer";
        reportButton.gameObject.SetActive(false);
        closeButton.gameObject.SetActive(false);
        tutoButton.gameObject.SetActive(true);
    }

    public void CloseUITuto()
    {
        userText1.text = "";
        userText2.text = "Vous pouvez maintenant fermer cette interface et passer à la suite";
        isTuto = false;//pour l'instant on ne prend pas en compte la hand ui..
        tutoButton.gameObject.SetActive(false);
    }

    public void InfoAsset()
    {
        userText1.text = "Objet de tutoriel sélectionné";
        userText2.text = "Actions potentielles et informations";
    }

    public void SetDataUI(bool b)
    {
        if (Scenario.Currentuser.isImmersive)
        {
            var assetsSelected = Scenario.Currentuser.GetSelectedAssets();
            if (!b)
            {
                userText1.text = "";
                userText2.text = "";
                foreach (UserStereotypeSO.AssetSelected asset in assetsSelected)
                {
                    mapAssets.BroadcastMessage("SelectOnMap", new int[2] { asset.id, 0 });
                }
                return;
            }
            userText1.text = "User " + Scenario.Currentuser.id + "\n";
            userText1.text += "Tickets Résolus : "+Scenario.Currentuser.ticketsDone+ "\n";
            string s = Scenario.Currentuser.CurrentTicketID == -1 ? "NONE" : Scenario.Currentuser.CurrentTicketID.ToString();
            userText2.text = "Ticket en cours : " + s + "\n";
            reportButton.gameObject.SetActive(false);
            closeButton.gameObject.SetActive(false);
            tutoButton.gameObject.SetActive(false);
            foreach (UserStereotypeSO.AssetSelected asset in assetsSelected)
            {
                mapAssets.BroadcastMessage("SelectOnMap", new int[2] { asset.id, 1 });
            }
            if (Scenario.Currentuser.CurrentTicketID != -1)
            {
                if (Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].ActionsPerformed.Contains(RequiredActions.TICKETACCEPTATION))
                {
                    mapAssets.BroadcastMessage("AcceptOnMap", new int[2] { Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].assetConcerned, 1 });
                    userText2.text += "Ticket Accepté \n";
                }
                if (Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].ActionsPerformed.Contains(RequiredActions.ASSETSELECTION))
                {
                    userText2.text += "Asset Selectionne\n";
                    //mapAssets.BroadcastMessage("SelectOnMap", new int[2] { Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].assetConcerned, 1 });
                }
                if (Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].ActionsPerformed.Contains(RequiredActions.ASSETANALYSIS))
                {
                    reportButton.gameObject.SetActive(true);
                    userText2.text += "asset Analyse \n";
                }
                if (Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].ActionsPerformed.Contains(RequiredActions.ASSETINFORMATION))
                {
                    reportButton.gameObject.SetActive(true);
                    userText2.text += "Informations Asset \n";
                }
                if (Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].ActionsPerformed.Contains(RequiredActions.REPORTSEND_ANALYST))
                {
                    reportButton.gameObject.SetActive(false);
                    userText2.text += "rapport Envoyé \n";
                }
                if (Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].ActionsPerformed.Contains(RequiredActions.REPORTREAD_COORD))
                {
                    userText2.text += "Rapport Lu\n";
                }
                if (Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].ActionsPerformed.Contains(RequiredActions.ALERTINCIDENT) || Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].ActionsPerformed.Contains(RequiredActions.ALERTDISCARD))
                {
                    userText2.text += "Incident catalogué \n";
                    closeButton.gameObject.SetActive(true);
                }
                if (Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].ActionsPerformed.Contains(RequiredActions.TICKETCLOSING))
                {
                    mapAssets.BroadcastMessage("AcceptOnMap", new int[2] { Scenario.Attacks[Scenario.Currentuser.CurrentTicketID].assetConcerned, 0 });
                    closeButton.gameObject.SetActive(false);
                    userText2.text += "Incident Clos \n";
                }

            }
            //if (Scenario.Currentuser.GetSelectedAssets().Count > 0)
            //{
             //   mapAssets.BroadcastMessage("SelectOneOnMap", Scenario.Currentuser.LastAssetSelected().id);
            //}
        }
    }

    public void ReportButton()
    {
        CyberCOPAttack attack = Scenario.Attacks[Scenario.Currentuser.CurrentTicketID];
        UIHandAction(new CyberCopEventsInformation(attack.assetConcerned, Scenario.Currentuser.ticketView, (int)RequiredActions.REPORTSEND_ANALYST, Scenario.Currentuser.id, Scenario.Currentuser.CurrentTicketID));
    }

    public void CloseTicketButton()
    {
        CyberCOPAttack attack = Scenario.Attacks[Scenario.Currentuser.CurrentTicketID];
        UIHandAction(new CyberCopEventsInformation(attack.assetConcerned, Scenario.Currentuser.ticketView, (int)RequiredActions.TICKETCLOSING, Scenario.Currentuser.id, Scenario.Currentuser.CurrentTicketID));
    }

    public void SetUIPosition(bool b)
    {
        if (CanvasUI != null)
        {
            CanvasUI.SetActive(b);
            if (!isTuto && b)
            {
                SetDataUI(b);
            }
            
        }
        
        //GetComponent<RectTransform>().anchoredPosition = new Vector2(GetComponent<RectTransform>().anchoredPosition.x, f);
    }
}
