﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualFilters : MonoBehaviour {
    private GameObject[] _PhysicalObjects;
    private GameObject[] _CyberObjects;
    private GameObject[] _AlertObjects;
    // Use this for initialization
	void Start ()
    {
        _PhysicalObjects = GameObject.FindGameObjectsWithTag("3DObj");
        _CyberObjects = GameObject.FindGameObjectsWithTag("TopologyItem");
        _AlertObjects = GameObject.FindGameObjectsWithTag("AlertItem");
    }

    public void AssetActivation(int i)//prendre un autre i pour alerte
    {
        if (i == 0)
        {
            foreach(GameObject g in _PhysicalObjects)
            {
                g.SetActive(true);
            }
            foreach (GameObject g in _CyberObjects)
            {
                g.SetActive(false);
            }
            foreach (GameObject g in _AlertObjects)
            {
                g.SetActive(false);
            }
        }
        else if (i == 1)
        {
            foreach (GameObject g in _PhysicalObjects)
            {
                g.SetActive(false);
            }
            foreach (GameObject g in _CyberObjects)
            {
                g.SetActive(true);
            }
            foreach (GameObject g in _AlertObjects)
            {
                g.SetActive(false);
            }
        }
        else if (i == 2)
        {
            foreach (GameObject g in _PhysicalObjects)
            {
                g.SetActive(false);
            }
            foreach (GameObject g in _CyberObjects)
            {
                g.SetActive(false);
            }
            foreach (GameObject g in _AlertObjects)
            {
                g.SetActive(true);
            }
        }
    }

    public void CyberPhysicalView(int i)//i=2 pour alert
    {
        if (GameObject.FindGameObjectsWithTag("MainCamera") != null)
        {
            switch (i)
            {
                case 0:
                    Camera.main.cullingMask = -105491;
                    break;
                case 1:
                    Camera.main.cullingMask = -121619;//avec assetviz
                    break;
                case 2:
                    Camera.main.cullingMask = -57107;
                    break;
                default:
                    break;
            }
        } 
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            Debug.Log("______________"+Camera.main.cullingMask);
        }
    }
}
