﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberPhysicalListenerActions : MonoBehaviour
{
    public VRTK_CyberCOP_Objects VRTK_ObjectSelecter;
    public VisualFilters VisualFilters;
	// Use this for initialization
	void Start ()
    {
        
	}
    private void OnEnable()
    {
        ScenarioActionsListener.CyberPhysicalViewDelegate += GamepadEvent;
    }
    private void OnDisable()
    {
        ScenarioActionsListener.CyberPhysicalViewDelegate -= GamepadEvent;
    }

    public void GamepadEvent(int i)//i=0 ->physical?
    {
        CyberPhysicalView(i);
        int layer = 0;
        switch (i)
        {
            case 0:
                layer = 14;
                break;
            case 1:
                layer = 10;
                break;
            case 2:
                layer = 16;
                break;
        }
        SetInteractionLayer(layer);
    }

    public void SetInteractionLayer(int i)
    {
        VRTK_ObjectSelecter.SetInteractionLayer(i);
    }

    public void CyberPhysicalView(int i)
    {
        VisualFilters.CyberPhysicalView(i);
        VisualFilters.AssetActivation(i);
    }
	// Update is called once per frame
	void Update ()
    {
		
	}
}
