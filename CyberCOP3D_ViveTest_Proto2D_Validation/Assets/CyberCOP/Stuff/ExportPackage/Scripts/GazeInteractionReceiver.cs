﻿/************************************************************************************

Copyright   :   Copyright 2014 Oculus VR, LLC. All Rights reserved.

Licensed under the Oculus VR Rift SDK License Version 3.2 (the "License");
you may not use the Oculus VR Rift SDK except in compliance with the License,
which is provided at the time of installation or download, or which
otherwise accompanies this software in either electronic or hard copy form.

You may obtain a copy of the License at

http://www.oculusvr.com/licenses/LICENSE-3.2

Unless required by applicable law or agreed to in writing, the Oculus VR SDK
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

************************************************************************************/

using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class GazeInteractionReceiver : MonoBehaviour
{
    public Color InitColor;
    public Color HighlightColor;
    public Color SelectedColor;
    public bool isSelected = false;
    public GameObject player;

    public GameObject GUI;
    private Text text;
    public AssetInformation assetinfo;

    private void Start()
    {
        /*
        if (player == null)
        {
            player = GameObject.Find("Camera");
        }
        */
        if (GUI != null)
        {
            GUI.SetActive(false);
            text = GUI.transform.GetChild(1).GetComponent<Text>();
        }
    }

    public void Highlight()
    {
        if (!isSelected)
        {
            InitColor = GetComponent<Renderer>().material.color;
            GetComponent<Renderer>().material.color = HighlightColor;
        }
        
    }

    public void Select()
    {
        if (!isSelected)
        {
            GetComponent<Renderer>().material.color = SelectedColor;
            isSelected = true;
            if (GUI != null)
            {
                GUI.SetActive(true);
                if (player != null)
                {
                    //GUI.transform.LookAt(2 * transform.position - player.transform.position);
                }
                
            }
        }
        
    }

    public void UnSelect()
    {
        if (isSelected)
        {
            isSelected = false;
            GetComponent<Renderer>().material.color = InitColor;
            if (GUI != null)
            {
                GUI.SetActive(false);
            }
        }
        
    }

    public void Return()
    {
        if(!isSelected)
        {
            GetComponent<Renderer>().material.color = InitColor;
            if (GUI != null)
            {
                GUI.SetActive(false);
            }
        }
    }
    IEnumerator AssetInfo()
    {
        float timer = 0;
        bool test = false;
        text.text = "Scan Information\n";
        while (!test)
        {
            timer += Time.deltaTime;
            text.text +=".";
            if (timer >= 2.5f)
            {
                test = true;
            }
            yield return null;
        }
        text.text = assetinfo.DataInfo();
        yield return null;
    }

    public void GetAssetInformation()
    {
        StartCoroutine(AssetInfo());
    }
   
}
