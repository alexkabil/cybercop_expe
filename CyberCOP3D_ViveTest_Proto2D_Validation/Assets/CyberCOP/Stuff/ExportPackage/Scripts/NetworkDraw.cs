﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkDraw : MonoBehaviour {
    public Transform next;
	// Use this for initialization
	void Start () {
        DrawLine();
	}
	
    public void DrawLine()
    {
        GetComponent<LineRenderer>().SetPosition(0, transform.position);
        GetComponent<LineRenderer>().SetPosition(1, next.position);
    }

	// Update is called once per frame
	void Update () {
		
	}
}
