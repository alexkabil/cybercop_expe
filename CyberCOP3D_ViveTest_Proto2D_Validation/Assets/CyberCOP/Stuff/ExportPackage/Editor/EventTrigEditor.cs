﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(GameEventSO))]
public class EventTrigEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GameEventSO myScript = (GameEventSO)target;
        if (GUILayout.Button("Raise Events"))
        {
            myScript.Raise();
        }
        if (GUILayout.Button("Raise Event Float"))
        {
            myScript.Raise(myScript.f_value);
        }
        if (GUILayout.Button("Raise Event String"))
        {
            myScript.Raise(myScript.s_value);
        }
    }
}