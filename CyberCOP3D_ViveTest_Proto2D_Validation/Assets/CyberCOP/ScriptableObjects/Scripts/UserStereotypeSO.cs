﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public enum Roles
{
    ANALYST,
    COORDINATOR,
    TESTER, //role qui peut tout faire???
    OTHER,
}


[CreateAssetMenu]
public class UserStereotypeSO : ScriptableObject
{
    public RequiredActions[] PossibleActions;
    public Roles userRole;
    public string Name;
    public int id;
    public int CurrentTicketID;//mettre un index comme sur le scenar?
    public bool isImmersive;
    public int ticketsDone;

    public bool isInvestigatingTicket; //pour séparer ticket d'alerte.... pas fan
    public int ticketView;
    public bool hasAuth;

    private List<CyberCopEventsInformation> _actionList;
    private List<AssetSelected> _assetselecteds; 

    private int _currentTicketID;
    private int _ticketsDone;
    private bool _isImmersive;
    private Roles _userRole;

    private bool _isInvestigatingTicket;
    private int _ticketView;//pour les cas d'erreurs sur les tickets..
    private bool _hasAuth;

    public void InitState()
    {
        _currentTicketID = CurrentTicketID;
        _ticketsDone = ticketsDone;
        _isImmersive = isImmersive;
        _userRole = userRole;
        _actionList = new List<CyberCopEventsInformation>();
        _assetselecteds = new List<AssetSelected>();
        _isInvestigatingTicket = isInvestigatingTicket;
        _ticketView = ticketView;
        _hasAuth = hasAuth;
    }
    public void EndState()
    {
        CurrentTicketID = _currentTicketID;
        ticketsDone = _ticketsDone;
        isImmersive = _isImmersive;
        userRole = _userRole;
        isInvestigatingTicket = _isInvestigatingTicket;
        ticketView = _ticketView;
        _actionList.Clear();
        hasAuth = _hasAuth;
    }
    //rajouter methodes d'initialisation?

    public void AddActionUser(CyberCopEventsInformation e)
    {
        //controle sur l'existence d'une meme action?
        _actionList.Add(e);
    }
    public CyberCopEventsInformation GetLastAction()
    {
        //check si liste nulle?
        return _actionList.LastOrDefault();
    }

    public List<CyberCopEventsInformation> GetUserActions()
    {
        return _actionList;
    }

    public void AddAssetSelected(CyberCopEventsInformation e)
    {
        AssetSelected asset = new AssetSelected();
        asset.Init(e.Asset, e.View/*, e.TicketID*/);
        if (!_assetselecteds.Contains(asset))
        {
            _assetselecteds.Add(asset);
        }
    }

    public void TicketsDone()
    {
        ticketsDone++;
    }

    public void RemoveAssetSelected(CyberCopEventsInformation e)
    {
        AssetSelected asset = new AssetSelected();
        asset.Init(e.Asset, e.View/*, e.TicketID*/);
        if (_assetselecteds.Contains(asset))
        {
            _assetselecteds.Remove(asset);
        }
    }

    public AssetSelected LastAssetSelected()
    {
        return _assetselecteds.LastOrDefault();
    }

    public List<AssetSelected> GetSelectedAssets()
    {
        return _assetselecteds;
    }

    public struct AssetSelected
    {
        public int id;
        public int view;
        //public int ticket;//utile??
        public void Init(int id, int view/*, int ticket*/)
        {
            this.id = id;
            this.view = view;
            //this.ticket = ticket;
        }
    };
}
