﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TestUIEvents : ScriptableObject
{
    public float f_value = 0;
    public string s_value = "";

    private List<TestEventsListener> listeners = new List<TestEventsListener>();
    public TestUIEvents LogTest;

    public void Raise(float f)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(f);
        }
    }
   
    public void Raise(string s)
    {
        {
            for (int i = listeners.Count - 1; i >= 0; i--)
            {
                listeners[i].OnEventRaised(s);
            }
        }
    }

    public void Raise(int i, int j, int k, int l)
    {
        for (int n = listeners.Count - 1; n >= 0; n--)
        {
            listeners[n].OnEventRaised(i,j,k,l);
        }
        string log = this.name + " " + i + " " + j + " " + k + " " + l;
        LogTest.Raise(log);
    }
    public void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(TestEventsListener list)
    {
        if (!listeners.Contains(list))
        {
            listeners.Add(list);
        }
    }

    public void UnregisterListener(TestEventsListener list)
    {
        if (listeners.Contains(list))
        {
            listeners.Remove(list);
        }
    }

}
