﻿using UnityEngine;
using System;

[Serializable]
public class FloatReference
{
    public bool isCst = true;
    public float CstVal;
    public FloatAsset Var;
    public float Value
    {
        get { return isCst ? CstVal : Var.Value; }
    }

}
