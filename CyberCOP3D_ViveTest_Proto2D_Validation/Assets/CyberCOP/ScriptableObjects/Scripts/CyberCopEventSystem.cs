﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CyberCopEventSystem : CyberCopEvent
{
    [SerializeField]
    private SystemActions sysAction = SystemActions.NONE;

    private SystemActions _sysAction;

    public override void InitValues(bool b)
    {
        if (b)
        {
            _sysAction = sysAction;
        }
        if (!b)
        {
            sysAction = _sysAction;
        }
        base.InitValues(b);
    }

    public override void Raise()
    {
        action = (int)sysAction;
        base.Raise();
    }

}
