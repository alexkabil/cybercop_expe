﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
[CreateAssetMenu]
public class AssetInfoList : ScriptableObject
{
    public int scenarioNumber;
    public bool hasAssistance;
    public int alertNumber = 0;
    public int incidentNumber = 0;

    private int _incidentNumber=0;
    private int _alertnumber = 0;
    private int _currentAttack = 0;
    private bool _isTuto;
    public List<AssetInformation> Assets;
    public List<AssetPositionsFilter> AssetsPositions;

    public CyberCOPAttack[] Attacks;
    public int currentAttack = -1;

    public TestUIEvents SetWorldState;
    public TestUIEvents ResetWorldState;
    public UserStereotypeSO[] Users;
    /* pour l'instant je fais ca..*/
    public UserStereotypeSO Currentuser;

    public void InitState()
    {
        _incidentNumber = incidentNumber;
        _alertnumber = alertNumber;
        _currentAttack = currentAttack;
        foreach(CyberCOPAttack cy in Attacks)
        {
            cy.InitState();
        }
        foreach(AssetInformation AI in Assets)
        {
            AI.InitState();
        }
        foreach(UserStereotypeSO so in Users)
        {
            so.InitState();
        }
    }

    public void EndState()
    {
        incidentNumber = _incidentNumber;
        alertNumber = _alertnumber;
        currentAttack = _currentAttack;
        foreach (CyberCOPAttack cy in Attacks)
        {
            cy.EndState();
        }
        foreach (AssetInformation AI in Assets)
        {
            AI.EndState();
        }
        foreach(UserStereotypeSO so in Users)
        {
            so.EndState();
        }

    }
     public void SetCurrentAttack(int i)
    {
        //condition sur current attack autre qu'initiale
        /*
        if (currentAttack != -1)
        {
            ResetWorldState.Raise(GetCurrentAttack().assetConcerned, 0, 0, 0);
        }
        */
        currentAttack = i;
        //SetWorldState.Raise(GetCurrentAttack().assetConcerned, 0, 0, 0);
    }

    public void SetNextAction()
    {
        GetCurrentAttack().NextScenarioAction();
    }

    public CyberCOPAttack GetCurrentAttack()
    {
        var atk = (from element in Attacks where element.attackID == currentAttack select element).First();
        return atk;
        //return Attacks[currentAttack];
    }

    public CyberCOPAttack GetAttack(int i)
    {
        return Attacks[i];
    }

    public void SetNextActionAtk(int i)
    {
        GetAttack(i).NextScenarioAction();
    }

    public void CheckScenarioAction(RequiredActions action, int i)
    {
        if (GetAttack(i).GetCurrentAction() == action)
        {
            GetAttack(i).NextScenarioAction();
            Debug.Log("Alerte " + i + " :Etape suivante: " + action.ToString());
        }
        else
        {
            Debug.Log("PA LA BONNE ACTION!!!");
        }
    }
}
