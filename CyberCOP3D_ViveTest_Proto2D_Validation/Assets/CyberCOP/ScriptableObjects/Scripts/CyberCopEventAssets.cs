﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CyberCopEventAssets : CyberCopEvent
{
    [SerializeField]
    private AssetUpdateActions assetAction = AssetUpdateActions.NONE;

    private AssetUpdateActions _assetAction;

    public override void InitValues(bool b)
    {
        if (b)
        {
            _assetAction = assetAction;
        }
        if (!b)
        {
            assetAction = _assetAction;
        }
        base.InitValues(b);
    }

    public override void Raise()
    {
        action = (int)assetAction;
        base.Raise();
    }

}
