﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CyberCOPAttack : ScriptableObject
{
    public bool isFalsePositive;
    public int attackState=0;
    public  int cyberPhysical = 0;
    public  int assetConcerned = 0;
    public  int attackID=0;
    public RequiredActions[] RequiredActions;
    public TestUIEvents[] AttackEvent;
    private bool _isFalsePositive;
    private int _attackState;
    public List<RequiredActions> ActionsPerformed;
    private List<ActionsDone> ActionsEvaluation;

    public struct ActionsDone
    {
        public int userId;
        public RequiredActions actionsDone;
        public bool isGoodAction;
        //public int ticket;//utile??
        public void Init(int userId, RequiredActions actionsDone, bool isGoodAction)
        {
            this.userId = userId;
            this.actionsDone = actionsDone;
            this.isGoodAction = isGoodAction;
        }
    };


    public void InitState()
    {
        _attackState = attackState;
        _isFalsePositive = isFalsePositive;
        ActionsPerformed = new List<RequiredActions>();
        ActionsEvaluation = new List<ActionsDone>();
    }

    public void EndState()
    {
        attackState = _attackState;
        isFalsePositive = _isFalsePositive;
        ActionsPerformed.Clear();
        ActionsEvaluation.Clear();
    }

    public RequiredActions GetCurrentAction()
    {
        return RequiredActions[attackState];
    }
    /*On passe en parametre le numéro de l'alerte..*/
    public void BeginAttack(int i)
    {
        foreach (TestUIEvents Atk in AttackEvent)
        {
            Atk.Raise(assetConcerned, attackState, cyberPhysical, i);
        }
    }

    //dès qu'on progresse, on change aussi les actionsperformed

    public void NextScenarioAction()
    {
        if (attackState < RequiredActions.Length-1)
        {
            //ActionsPerformed.Add(GetCurrentAction());
            attackState++;
            return;
        }
        if (attackState == RequiredActions.Length - 1)
        {
            Debug.Log("on a tout fait");
        }
    }

    public void AddEvalAction(int user, RequiredActions action, bool b)
    {
        ActionsDone act = new ActionsDone();
        act.Init(user, action, b);
        if (!ActionsEvaluation.Contains(act))
        {
            ActionsEvaluation.Add(act);
        }
    }

    public int Scoring()
    {
        int i = 0;
        foreach(ActionsDone d in ActionsEvaluation)
        {
            if (d.isGoodAction)
            {
                i++;
            }
            else
            {
                i--;
            }
        }
        return i;

    }
    public void AddAction(RequiredActions action)
    {
        ActionsPerformed.Add(action);
    }


}
