﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MyEvent: UnityEvent<float>
{

}
[System.Serializable]
public class MyStringEvent : UnityEvent<string>
{

}
[System.Serializable]
public class MyCompositeEvent : UnityEvent<string,int,float>
{

}


public class GameEventListener : MonoBehaviour {
    public GameEventSO GE;
    public MyEvent Response;
    public MyStringEvent ResponseSource;
    public MyCompositeEvent ResponseComplex;

    public string specificMessage;
    public float specificValue;
	// Use this for initialization
	void Start () {
		
	}

    private void OnEnable()
    {
        GE.RegisterListener(this);   
    }
    private void OnDisable()
    {
        GE.UnregisterListener(this);
    }
    public void OnEventRaised(float f)
    {
        Response.Invoke(f);
    }
    public void OnEventRaised(string s, int i, float f)
    {
        ResponseComplex.Invoke(s, i, f);
    }

    public void OnEventRaised(string s)
    {
        ResponseSource.Invoke(s);
    }
    public void OnEventRaised()
    {
        Response.Invoke(specificValue);
        ResponseSource.Invoke(specificMessage);
    }

    // Update is called once per frame
    void Update ()
    {

	}
}
