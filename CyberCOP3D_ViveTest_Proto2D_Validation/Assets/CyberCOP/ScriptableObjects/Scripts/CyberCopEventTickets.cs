﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CyberCopEventTickets : CyberCopEvent
{
    [SerializeField]
    private TicketUpdateActions ticketAction = TicketUpdateActions.NONE;

    private TicketUpdateActions _ticketAction;

    public override void InitValues(bool b)
    {
        if (b)
        {
            _ticketAction = ticketAction;
        }
        if (!b)
        {
            ticketAction = _ticketAction;
        }
        base.InitValues(b);
    }

    public override void Raise()
    {
        action = (int)ticketAction;
        base.Raise();
    }

}
