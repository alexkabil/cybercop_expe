﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class EventsList : ScriptableObject
{
    public List<GameEventSO> Events;
    public void RaiseEvent(int i)
    {
        if (i < Events.Count)
        {
            Events[i].Raise();
        }
    }
    public void RaiseEvent(int i, float f)
    {
        if (i < Events.Count)
        {
            Events[i].Raise(f);
        }
    }
    public void RaiseEvent(int i, string s)
    {
        if (i < Events.Count)
        {
            Events[i].Raise(s);
        }
    }
}
