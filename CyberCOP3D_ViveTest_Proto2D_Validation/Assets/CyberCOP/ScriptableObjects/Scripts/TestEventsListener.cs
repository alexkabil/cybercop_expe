﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TestEventFloat : UnityEvent<float>
{

}

[System.Serializable]
public class TestEventString : UnityEvent<string>
{

}

[System.Serializable]
public class TestCompositeEvent : UnityEvent<int, int, int, int>
{

}


public class TestEventsListener : MonoBehaviour
{
    public TestUIEvents GE;
    public TestCompositeEvent ResponseComplex;
    public TestEventFloat ResponseFloat;
    public TestEventString ResponseString;
    public UnityEvent Response;

    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {
        GE.RegisterListener(this);
    }
    private void OnDisable()
    {
        GE.UnregisterListener(this);
    }
    public void OnEventRaised(float f)
    {
        ResponseFloat.Invoke(f);
    }
    public void OnEventRaised(string s)
    {
        ResponseString.Invoke(s);
    }
    public void OnEventRaised(int i, int j, int k, int l)
    {
        ResponseComplex.Invoke(i,j,k,l);
    }

    public void OnEventRaised()
    {
        Response.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
