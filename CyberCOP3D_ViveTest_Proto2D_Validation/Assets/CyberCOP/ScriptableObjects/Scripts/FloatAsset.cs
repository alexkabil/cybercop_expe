﻿using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu]
public class FloatAsset : ScriptableObject
{
    public FloatAssetList list;
    public float Value;
    public float Treshold;
    public int index;

    public Dictionary<int, float> prevValues;

    private float _value;

    public void InitState()
    {
        index = 2;
        _value = Value;
        prevValues = new Dictionary<int, float>();
        prevValues.Add(0, -100.0f);
        prevValues.Add(1, 0.0f);
        prevValues.Add(2, 100.0f);
    }
    public void EndState()
    {
        Value = _value;
        prevValues.Clear();
        index = 0;
    }

    public void SetValue(float f)
    {
        Value = f;
        index++;
        prevValues.Add(index, f);
        if (prevValues.Count > 21)
        {
            prevValues.Remove(0);
        }
    }


}
