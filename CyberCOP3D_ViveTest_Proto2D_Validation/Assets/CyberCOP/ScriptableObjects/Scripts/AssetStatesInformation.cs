﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu]
public class AssetStatesInformation : ScriptableObject
{
    public int id;
    public assetTypeStates kineticState = assetTypeStates.INIT;
    public assetTypeStates cyberState = assetTypeStates.INIT;
    public assetTypeStates state = assetTypeStates.INIT; //à virer parce que ca sert plus
    public bool isAlert = false;
    public bool isCompromised = false;
    public bool isNormal = false;

    public List<CyberCopEventsInformation> ActionsDoneOnAsset; 

    private int _id;
    private assetTypeStates _kineticState;
    private assetTypeStates _cyberState;
    private assetTypeStates _state;
    private bool _isAlert;
    public bool _isCompromised;
    public bool _isNormal;

    public void InitState()
    {
        _id = id;
        _state = state;
        _kineticState = kineticState;
        _cyberState = cyberState;

        _isAlert = isAlert;
        _isCompromised = isCompromised;
        _isNormal = isNormal;
        ActionsDoneOnAsset = new List<CyberCopEventsInformation>();
    }

    public void EndState()
    {
        id = _id;
        state = _state;
        kineticState = _kineticState;
        cyberState = _cyberState;

        isAlert = _isAlert;
        isCompromised = _isCompromised;
        isNormal = _isNormal;
        ActionsDoneOnAsset.Clear();
    }

    public CyberCopEventsInformation GetLastActionDoneOnAsset()
    {
        return ActionsDoneOnAsset.LastOrDefault();
    }

    public assetTypeStates currentState(int view)
    {
        if (view == 1)
        {
            return kineticState;
        }
        else
        {
            return cyberState;
        }
    }

    public enum assetTypeStates
    {
        INIT,
        NORMAL,
        ALERT,
        UNDERANALYSIS,
        ANALYZED,
        UNDERINVESTIGATION,
        INVESTIGATED,
        COMPROMIZED
    }

}
