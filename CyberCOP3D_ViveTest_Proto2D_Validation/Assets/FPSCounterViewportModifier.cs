﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.UI;
public class FPSCounterViewportModifier : MonoBehaviour {
    public bool printing = false;
	// Use this for initialization
	void Start ()
    {
        StartCoroutine(PrintFpsAndViewport());
	}
	
    public IEnumerator PrintFpsAndViewport()
    {
        while (true)
        {
            if (!printing)
            {
                yield return null;
            }
            GetComponent<Text>().text = "FPS : " + ((int)(1f / Time.unscaledDeltaTime)).ToString()+"\n RatioViewport : "+XRSettings.eyeTextureResolutionScale.ToString();
            yield return new WaitForSeconds(0.5f);
        }
        yield return null;
    }
    private void OnDisable()
    {
        StopCoroutine(PrintFpsAndViewport());
    }
    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            printing = !printing;
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            XRSettings.eyeTextureResolutionScale -= 0.1f;
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            XRSettings.eyeTextureResolutionScale += 0.1f;
        }
    }
}
